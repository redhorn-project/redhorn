#include <vector>
#include <string>

#include <gtest/gtest.h>

#include <redhorn/p2p/protocol.hpp>

#include "../transport.hpp"

TEST(P2PTest, ProtocolBasic)
{
    struct traits
    {
        typedef transport::cfgs::msg<transport::cfgs::all> payload;
    };
    typedef redhorn::p2p::protocol<traits> protocol;
    typedef protocol::msg msg;

    redhorn::p2p::id::router rid1 = static_cast<redhorn::p2p::id::router>(redhorn::p2p::id::router::generate());
    redhorn::p2p::id::router rid2 = static_cast<redhorn::p2p::id::router>(redhorn::p2p::id::router::generate());
    redhorn::p2p::id::peer pid1 = static_cast<redhorn::p2p::id::peer>(redhorn::p2p::id::peer::generate());
    redhorn::p2p::id::peer pid2 = static_cast<redhorn::p2p::id::peer>(redhorn::p2p::id::peer::generate());
    std::stringstream ss;
    {
        std::ostream &out(ss);
        {
            protocol::msg handshake;
            auto data = handshake.alloc<protocol::HANDSHAKE>();
            data->rid = rid1;
            data->beat = 500;
            redhorn::json::write(out, handshake);
        }
        {
            msg route;
            auto data = route.alloc<protocol::ROUTE>();
            data->dst.insert(pid2);
            traits::payload payload;
            auto cfg = payload.alloc<transport::cfgs::COORD>();
            cfg->endpoint = "endpoint";
            data->payload = redhorn::make_locked<traits::payload>(std::move(payload));
            redhorn::json::write(out, route);
        }
    }
    {
        std::istream &in(ss);
        {
            msg handshake;
            redhorn::json::read(in, handshake);

            constexpr static int handshake_id = protocol::HANDSHAKE::msg_id;
            ASSERT_EQ(handshake_id, handshake.msg_id());

            auto data = handshake.get<protocol::HANDSHAKE>();
            EXPECT_EQ(rid1, data->rid);
            EXPECT_EQ(500, data->beat);
        }
        {
            msg route;
            redhorn::json::read(in, route);

            constexpr static int route_id = protocol::ROUTE::msg_id;
            ASSERT_EQ(route_id, route.msg_id());
            
            auto data = route.get<protocol::ROUTE>();
            EXPECT_EQ(1, data->dst.size());
            EXPECT_EQ(1, data->dst.count(pid2));

            constexpr static int coord_id = transport::cfgs::COORD::msg_id;
            ASSERT_EQ(coord_id, data->payload->msg_id());

            auto cfg = data->payload->get<transport::cfgs::COORD>();
            EXPECT_EQ("endpoint", cfg->endpoint);
        }
    }
}
