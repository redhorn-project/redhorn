#include <gtest/gtest.h>

#include <redhorn/p2p/checksum.hpp>
#include <redhorn/rsa.hpp>

TEST(P2PTest, Checksum)
{
    static const redhorn::p2p::id::router rid1(redhorn::p2p::id::router::generate());
    static const redhorn::p2p::id::router rid2(redhorn::p2p::id::router::generate());
    static const redhorn::p2p::id::router rid3(redhorn::p2p::id::router::generate());
    static const redhorn::p2p::id::router rid4(redhorn::p2p::id::router::generate());
    static const redhorn::rsa::key key1 = redhorn::rsa::key::generate(1024);
    static const redhorn::rsa::key key2 = redhorn::rsa::key::generate(1024);
    static const redhorn::rsa::key key3 = redhorn::rsa::key::generate(1024);
    static const redhorn::rsa::key key4 = redhorn::rsa::key::generate(1024);
    static const redhorn::p2p::id::peer pid1(key1.fingerprint());
    static const redhorn::p2p::id::peer pid2(key2.fingerprint());
    static const redhorn::p2p::id::peer pid3(key3.fingerprint());
    static const redhorn::p2p::id::peer pid4(key4.fingerprint());

    std::map<redhorn::p2p::id::peer, redhorn::p2p::route> routes1;
    std::map<redhorn::p2p::id::peer, redhorn::p2p::route> routes2;

    std::unordered_map<redhorn::p2p::id::peer, redhorn::rsa::certificate> certificates;
    certificates[pid1] = key1.certificate();
    certificates[pid2] = key2.certificate();
    certificates[pid3] = key3.certificate();
    certificates[pid4] = key4.certificate();
    
    routes1[pid1].emplace_back(redhorn::p2p::make_route_point(rid1, {redhorn::p2p::make_endpoint("localhost1", 34561)}));
    routes1[pid2].emplace_back(redhorn::p2p::make_route_point(rid2, {redhorn::p2p::make_endpoint("localhost2", 34562)}));
    routes1[pid3].emplace_back(redhorn::p2p::make_route_point(rid3, {redhorn::p2p::make_endpoint("localhost3", 34563)}));
    routes1[pid4].emplace_back(redhorn::p2p::make_route_point(rid4, {redhorn::p2p::make_endpoint("localhost4", 34564)}));

    routes2[pid4].emplace_back(redhorn::p2p::make_route_point(rid4, {redhorn::p2p::make_endpoint("localhost4", 34564)}));
    routes2[pid2].emplace_back(redhorn::p2p::make_route_point(rid2, {redhorn::p2p::make_endpoint("localhost2", 34562)}));
    routes2[pid1].emplace_back(redhorn::p2p::make_route_point(rid1, {redhorn::p2p::make_endpoint("localhost1", 34561)}));
    routes2[pid3].emplace_back(redhorn::p2p::make_route_point(rid3, {redhorn::p2p::make_endpoint("localhost3", 34563)}));
    
    EXPECT_EQ(redhorn::p2p::router_checksum(routes1, certificates), redhorn::p2p::router_checksum(routes2, certificates));
}

