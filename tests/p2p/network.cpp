#include <gtest/gtest.h>

#include <redhorn/p2p/network.decl>
#include <redhorn/logging.hpp>
#include <redhorn/butcher.hpp>

#include "../transport.hpp"


class P2PNetworkTest : public ::testing::Test 
{
};

namespace redhorn
{
    template <>
    class butcher<P2PNetworkTest>
    {
    public:
        butcher(network &net)
            : _network(net)
        {
        }

    public:
        std::map<redhorn::p2p::id::peer, redhorn::p2p::route> routes()
        {
            return _network._blueprint.routes();
        }

        std::map<redhorn::p2p::id::peer, redhorn::p2p::route> router_routes(redhorn::p2p::id::router rid)
        {
            return _network._blueprint.routers().at(rid)->routes;
        }

        size_t routers_size()
        {
            return _network._blueprint.routers().size();            
        }
    private:
        network &_network;
    };
}

class network_callback : public network::callback
{
public:
    network_callback()
    {}

    virtual void insert(const redhorn::p2p::id::router &) override
    {
        _cv.notify_one();
    }

    virtual void remove(const redhorn::p2p::id::router &) override
    {
        _cv.notify_one();
    }

    virtual void update(const std::vector<redhorn::p2p::peer_route> &,
                        const std::vector<redhorn::p2p::id::peer> &) override
    {
        _cv.notify_one();
    }

    bool wait_until(const std::function<bool()> &pred)
    {
        std::unique_lock<std::mutex> guard(_mutex);
        return _cv.wait_for(guard, std::chrono::seconds(5), pred);
    }
private:
    std::mutex _mutex;
    std::condition_variable _cv;
};

TEST_F(P2PNetworkTest, NetworkBasic)
{
    redhorn::p2p::id::router rid(redhorn::p2p::id::router::generate());
    redhorn::rsa::key key = redhorn::rsa::key::generate(1024);
    redhorn::p2p::id::peer pid(key.fingerprint());
    network network(redhorn::logger("p2p::network"),
                    rid,
                    {312546},
                    {redhorn::p2p::make_endpoint("127.0.0.1", 312546)},
                    std::chrono::milliseconds(2000),
                    std::chrono::milliseconds(2000));
    network::gate gate = network.join(key);
    
    redhorn::butcher<P2PNetworkTest> butcher(network);
    auto routes =  butcher.routes();
    ASSERT_EQ(1, routes.size());
    ASSERT_EQ(1, routes.at(pid).size());
    ASSERT_EQ(0, butcher.routers_size());
}


#define WAIT_ASSERT_EQ(expected, actual)                    \
    {                                                       \
        cb.wait_until([&](){return expected == actual;});   \
        ASSERT_EQ(expected, actual);                        \
    }


TEST_F(P2PNetworkTest, NetworkBasicConnect)
{
    static const redhorn::p2p::id::router rid1(redhorn::p2p::id::router::generate());
    static const redhorn::p2p::id::router rid2(redhorn::p2p::id::router::generate());
    static const redhorn::p2p::id::router rid3(redhorn::p2p::id::router::generate());
    static const redhorn::p2p::id::router rid4(redhorn::p2p::id::router::generate());
    static const redhorn::rsa::key key1 = redhorn::rsa::key::generate(1024);
    static const redhorn::rsa::key key2 = redhorn::rsa::key::generate(1024);
    static const redhorn::rsa::key key3 = redhorn::rsa::key::generate(1024);
    static const redhorn::rsa::key key4 = redhorn::rsa::key::generate(1024);
    static const redhorn::p2p::id::peer pid1(key1.fingerprint());
    static const redhorn::p2p::id::peer pid2(key2.fingerprint());
    static const redhorn::p2p::id::peer pid3(key3.fingerprint());
    static const redhorn::p2p::id::peer pid4(key4.fingerprint());
    static const unsigned int port1 = 312547;
    static const unsigned int port2 = 312548;
    static const unsigned int port3 = 312549;
    static const unsigned int port4 = 312550;

    std::chrono::milliseconds beat = std::chrono::milliseconds(20000);
    network_callback cb;

    network network1(redhorn::logger("p2p::network1"),
                     rid1,
                     {port1},
                     {redhorn::p2p::make_endpoint("127.0.0.1", port1)},
                     beat,
                     beat);
    network network2(redhorn::logger("p2p::network2"),
                     rid2,
                     {port2},
                     {redhorn::p2p::make_endpoint("127.0.0.1", port2)},
                     beat,
                     beat);
    network network3(redhorn::logger("p2p::network3"),
                     rid3,
                     {port3},
                     {redhorn::p2p::make_endpoint("127.0.0.1", port3)},
                     beat,
                     beat);
    network network4(redhorn::logger("p2p::network4"),
                     rid4,
                     {port4},
                     {redhorn::p2p::make_endpoint("127.0.0.1", port4)},
                     beat,
                     beat);

    network::gate gate1 = network1.join(key1);
    network::gate gate2 = network2.join(key2);
    network::gate gate3 = network3.join(key3);
    network::gate gate4 = network4.join(key4);

    redhorn::butcher<P2PNetworkTest> butcher1(network1);
    redhorn::butcher<P2PNetworkTest> butcher2(network2);
    redhorn::butcher<P2PNetworkTest> butcher3(network3);
    redhorn::butcher<P2PNetworkTest> butcher4(network4);

    network1.subscribe(&cb);
    network2.subscribe(&cb);
    network3.subscribe(&cb);
    network4.subscribe(&cb);

    network1.start(3);
    network2.start(3);
    network3.start(3);
    network4.start(3);
    
    network2.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port1)});
// First peer
    {
// Own routes
        WAIT_ASSERT_EQ(2, butcher1.routes().size());
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid2));
        
        WAIT_ASSERT_EQ(1, butcher1.routes().at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher1.routes().at(pid2).size());

// Connected peers
        WAIT_ASSERT_EQ(1, butcher1.routers_size());

// 2nd peer routes
        WAIT_ASSERT_EQ(2, butcher1.router_routes(rid2).size());
        WAIT_ASSERT_EQ(1, butcher1.router_routes(rid2).count(pid1));
        WAIT_ASSERT_EQ(1, butcher1.router_routes(rid2).count(pid2));
        
        WAIT_ASSERT_EQ(2, butcher1.router_routes(rid2).at(pid1).size());
        WAIT_ASSERT_EQ(1, butcher1.router_routes(rid2).at(pid2).size());
    }

// Second peer
    {
// Own routes
        WAIT_ASSERT_EQ(2, butcher2.routes().size());
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid2));

        WAIT_ASSERT_EQ(2, butcher2.routes().at(pid1).size());
        WAIT_ASSERT_EQ(1, butcher2.routes().at(pid2).size());

// Connected peers
        WAIT_ASSERT_EQ(1, butcher2.routers_size());

// 1st peer routes
        WAIT_ASSERT_EQ(2, butcher2.router_routes(rid1).size());
        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid1).count(pid1));
        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid1).count(pid2));

        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid1).at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher2.router_routes(rid1).at(pid2).size());
    }

    network3.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port2)});

// First peer
    {
// Own routes
        WAIT_ASSERT_EQ(3, butcher1.routes().size());
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid3));

        WAIT_ASSERT_EQ(1, butcher1.routes().at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher1.routes().at(pid2).size());
        WAIT_ASSERT_EQ(3, butcher1.routes().at(pid3).size());

// Connected peers
        WAIT_ASSERT_EQ(1, butcher1.routers_size());

// 2nd peer routes
        WAIT_ASSERT_EQ(3, butcher1.router_routes(rid2).size());

        WAIT_ASSERT_EQ(1, butcher1.router_routes(rid2).count(pid1));
        WAIT_ASSERT_EQ(1, butcher1.router_routes(rid2).count(pid2));
        WAIT_ASSERT_EQ(1, butcher1.router_routes(rid2).count(pid3));

        WAIT_ASSERT_EQ(2, butcher1.router_routes(rid2).at(pid1).size());
        WAIT_ASSERT_EQ(1, butcher1.router_routes(rid2).at(pid2).size());
        WAIT_ASSERT_EQ(2, butcher1.router_routes(rid2).at(pid3).size());
    }

// Second peer
    {
// Own routes
        WAIT_ASSERT_EQ(3, butcher2.routes().size());

        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid3));
        
        WAIT_ASSERT_EQ(2, butcher2.routes().at(pid1).size());
        WAIT_ASSERT_EQ(1, butcher2.routes().at(pid2).size());
        WAIT_ASSERT_EQ(2, butcher2.routes().at(pid3).size());

// Connected peers
        WAIT_ASSERT_EQ(2, butcher2.routers_size());

// 1st peer routes
        WAIT_ASSERT_EQ(3, butcher2.router_routes(rid1).size());

        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid1).count(pid1));
        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid1).count(pid2));
        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid1).count(pid3));

        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid1).at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher2.router_routes(rid1).at(pid2).size());
        WAIT_ASSERT_EQ(3, butcher2.router_routes(rid1).at(pid3).size());

// 3rd peer routes
        WAIT_ASSERT_EQ(3, butcher2.router_routes(rid3).size());

        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid3).count(pid1));
        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid3).count(pid2));
        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid3).count(pid3));

        WAIT_ASSERT_EQ(3, butcher2.router_routes(rid3).at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher2.router_routes(rid3).at(pid2).size());
        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid3).at(pid3).size());
    }

// Third peer
    {
// Own routes
        auto routes =  butcher3.routes();
        WAIT_ASSERT_EQ(3, butcher3.routes().size());

        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid3));

        WAIT_ASSERT_EQ(3, butcher3.routes().at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher3.routes().at(pid2).size());
        WAIT_ASSERT_EQ(1, butcher3.routes().at(pid3).size());

// Connected peers
        WAIT_ASSERT_EQ(1, butcher3.routers_size());

// 2nd peer routes
        routes = butcher1.router_routes(rid2);
        WAIT_ASSERT_EQ(3, butcher3.router_routes(rid2).size());

        WAIT_ASSERT_EQ(1, butcher3.router_routes(rid2).count(pid1));
        WAIT_ASSERT_EQ(1, butcher3.router_routes(rid2).count(pid2));
        WAIT_ASSERT_EQ(1, butcher3.router_routes(rid2).count(pid3));

        WAIT_ASSERT_EQ(2, butcher3.router_routes(rid2).at(pid1).size());
        WAIT_ASSERT_EQ(1, butcher3.router_routes(rid2).at(pid2).size());
        WAIT_ASSERT_EQ(2, butcher3.router_routes(rid2).at(pid3).size());
    }


    network1.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port3)});

// First peer
    {
// Own routes
        auto routes =  butcher1.routes();
        WAIT_ASSERT_EQ(3, butcher1.routes().size());

        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid3));

        WAIT_ASSERT_EQ(1, butcher1.routes().at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher1.routes().at(pid2).size());
        WAIT_ASSERT_EQ(2, butcher1.routes().at(pid3).size());

// Connected peers
        WAIT_ASSERT_EQ(2, butcher1.routers_size());

// 2nd peer routes
        WAIT_ASSERT_EQ(3, butcher1.router_routes(rid2).size());

        WAIT_ASSERT_EQ(1, butcher1.router_routes(rid2).count(pid1));
        WAIT_ASSERT_EQ(1, butcher1.router_routes(rid2).count(pid2));
        WAIT_ASSERT_EQ(1, butcher1.router_routes(rid2).count(pid3));

        WAIT_ASSERT_EQ(2, butcher1.router_routes(rid2).at(pid1).size());
        WAIT_ASSERT_EQ(1, butcher1.router_routes(rid2).at(pid2).size());
        WAIT_ASSERT_EQ(2, butcher1.router_routes(rid2).at(pid3).size());

// 3rd peer routes
        WAIT_ASSERT_EQ(3, butcher1.router_routes(rid3).size());

        WAIT_ASSERT_EQ(1, butcher1.router_routes(rid3).count(pid1));
        WAIT_ASSERT_EQ(1, butcher1.router_routes(rid3).count(pid2));
        WAIT_ASSERT_EQ(1, butcher1.router_routes(rid3).count(pid3));

        WAIT_ASSERT_EQ(2, butcher1.router_routes(rid3).at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher1.router_routes(rid3).at(pid2).size());
        WAIT_ASSERT_EQ(1, butcher1.router_routes(rid3).at(pid3).size());
    }

// Second peer
    {
// Own routes
        WAIT_ASSERT_EQ(3, butcher2.routes().size());
        
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid3));

        WAIT_ASSERT_EQ(2, butcher2.routes().at(pid1).size());
        WAIT_ASSERT_EQ(1, butcher2.routes().at(pid2).size());
        WAIT_ASSERT_EQ(2, butcher2.routes().at(pid3).size());

// Connected peers
        WAIT_ASSERT_EQ(2, butcher2.routers_size());

// 1st peer routes
        WAIT_ASSERT_EQ(3, butcher2.router_routes(rid1).size());
        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid1).count(pid1));
        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid1).count(pid2));
        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid1).count(pid3));

        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid1).at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher2.router_routes(rid1).at(pid2).size());
        WAIT_ASSERT_EQ(2, butcher2.router_routes(rid1).at(pid3).size());

// 3rd peer routes
        WAIT_ASSERT_EQ(3, butcher2.router_routes(rid3).size());
        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid3).count(pid1));
        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid3).count(pid2));
        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid3).count(pid3));

        WAIT_ASSERT_EQ(2, butcher2.router_routes(rid3).at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher2.router_routes(rid3).at(pid2).size());
        WAIT_ASSERT_EQ(1, butcher2.router_routes(rid3).at(pid3).size());

    }

// Third peer
    {
// Own routes
        WAIT_ASSERT_EQ(3, butcher3.routes().size());
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid3));

        WAIT_ASSERT_EQ(2, butcher3.routes().at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher3.routes().at(pid2).size());
        WAIT_ASSERT_EQ(1, butcher3.routes().at(pid3).size());

// Connected peers
        WAIT_ASSERT_EQ(2, butcher3.routers_size());

// 1st peer routes
        WAIT_ASSERT_EQ(3, butcher3.router_routes(rid1).size());

        WAIT_ASSERT_EQ(1, butcher3.router_routes(rid1).count(pid1));
        WAIT_ASSERT_EQ(1, butcher3.router_routes(rid1).count(pid2));
        WAIT_ASSERT_EQ(1, butcher3.router_routes(rid1).count(pid3));

        WAIT_ASSERT_EQ(1, butcher3.router_routes(rid1).at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher3.router_routes(rid1).at(pid2).size());
        WAIT_ASSERT_EQ(2, butcher3.router_routes(rid1).at(pid3).size());

// 2nd peer routes
        WAIT_ASSERT_EQ(3, butcher3.router_routes(rid2).size());

        WAIT_ASSERT_EQ(1, butcher3.router_routes(rid2).count(pid1));
        WAIT_ASSERT_EQ(1, butcher3.router_routes(rid2).count(pid2));
        WAIT_ASSERT_EQ(1, butcher3.router_routes(rid2).count(pid3));

        WAIT_ASSERT_EQ(2, butcher3.router_routes(rid2).at(pid1).size());
        WAIT_ASSERT_EQ(1, butcher3.router_routes(rid2).at(pid2).size());
        WAIT_ASSERT_EQ(2, butcher3.router_routes(rid2).at(pid3).size());
    }


    network4.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port1)});

// Own routes only
    {
        WAIT_ASSERT_EQ(4, butcher1.routes().size());

        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid3));
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid4));

        WAIT_ASSERT_EQ(1, butcher1.routes().at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher1.routes().at(pid2).size());
        WAIT_ASSERT_EQ(2, butcher1.routes().at(pid3).size());
        WAIT_ASSERT_EQ(2, butcher1.routes().at(pid4).size());
        
    }

    {
        WAIT_ASSERT_EQ(4, butcher2.routes().size());

        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid3));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid4));

        WAIT_ASSERT_EQ(2, butcher2.routes().at(pid1).size());
        WAIT_ASSERT_EQ(1, butcher2.routes().at(pid2).size());
        WAIT_ASSERT_EQ(2, butcher2.routes().at(pid3).size());
        WAIT_ASSERT_EQ(3, butcher2.routes().at(pid4).size());

    }

    {
        WAIT_ASSERT_EQ(4, butcher3.routes().size());

        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid3));
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid4));
        
        WAIT_ASSERT_EQ(2, butcher3.routes().at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher3.routes().at(pid2).size());
        WAIT_ASSERT_EQ(1, butcher3.routes().at(pid3).size());
        WAIT_ASSERT_EQ(3, butcher3.routes().at(pid4).size());
    }

    {
        WAIT_ASSERT_EQ(4, butcher4.routes().size());

        WAIT_ASSERT_EQ(1, butcher4.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher4.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher4.routes().count(pid3));
        WAIT_ASSERT_EQ(1, butcher4.routes().count(pid4));
        
        WAIT_ASSERT_EQ(2, butcher4.routes().at(pid1).size());
        WAIT_ASSERT_EQ(3, butcher4.routes().at(pid2).size());
        WAIT_ASSERT_EQ(3, butcher4.routes().at(pid3).size());
        WAIT_ASSERT_EQ(1, butcher4.routes().at(pid4).size());
    }
    
    
    network1.stop();
    network2.stop();
    network3.stop();
    network4.stop();
    
    network1.wait();
    network2.wait();
    network3.wait();
    network4.wait();
}


TEST_F(P2PNetworkTest, NetworkBasicSend)
{
    redhorn::logger log = redhorn::logger("basic-send");
    static const redhorn::p2p::id::router rid1(redhorn::p2p::id::router::generate());
    static const redhorn::p2p::id::router rid2(redhorn::p2p::id::router::generate());
    static const redhorn::p2p::id::router rid3(redhorn::p2p::id::router::generate());
    static const redhorn::p2p::id::router rid4(redhorn::p2p::id::router::generate());
    static const redhorn::rsa::key key1 = redhorn::rsa::key::generate(1024);
    static const redhorn::rsa::key key2 = redhorn::rsa::key::generate(1024);
    static const redhorn::rsa::key key3 = redhorn::rsa::key::generate(1024);
    static const redhorn::rsa::key key4 = redhorn::rsa::key::generate(1024);
    static const redhorn::p2p::id::peer pid1(key1.fingerprint());
    static const redhorn::p2p::id::peer pid2(key2.fingerprint());
    static const redhorn::p2p::id::peer pid3(key3.fingerprint());
    static const redhorn::p2p::id::peer pid4(key4.fingerprint());
    static const unsigned int port1 = 313547;
    static const unsigned int port2 = 313548;
    static const unsigned int port3 = 313549;
    static const unsigned int port4 = 313550;
    std::chrono::milliseconds beat = std::chrono::milliseconds(20000);

    network network1(redhorn::logger("p2p::network1"),
                     rid1,
                     {port1},
                     {redhorn::p2p::make_endpoint("127.0.0.1", port1)},
                     beat,
                     beat);
    network network2(redhorn::logger("p2p::network2"),
                     rid2,
                     {port2},
                     {redhorn::p2p::make_endpoint("127.0.0.1", port2)},
                     beat,
                     beat);
    network network3(redhorn::logger("p2p::network3"),
                     rid3,
                     {port3},
                     {redhorn::p2p::make_endpoint("127.0.0.1", port3)},
                     beat,
                     beat);
    network network4(redhorn::logger("p2p::network4"),
                     rid4,
                     {port4},
                     {redhorn::p2p::make_endpoint("127.0.0.1", port4)},
                     beat,
                     beat);

    redhorn::butcher<P2PNetworkTest> butcher1(network1);
    redhorn::butcher<P2PNetworkTest> butcher2(network2);
    redhorn::butcher<P2PNetworkTest> butcher3(network3);
    redhorn::butcher<P2PNetworkTest> butcher4(network4);
    
    network_callback cb;

    network1.subscribe(&cb);
    network2.subscribe(&cb);
    network3.subscribe(&cb);
    network4.subscribe(&cb);

    
    network::gate gate1 = network1.join(key1);
    network::gate gate2 = network2.join(key2);
    network::gate gate3 = network3.join(key3);
    network::gate gate4 = network4.join(key4);

    network1.start(3);
    network2.start(3);
    network3.start(3);
    network4.start(3);
    
    redhorn::p2p::id::peer src;

    network2.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port1)});
    {
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid2));
    }
    gate1.send(pid2, 5);
    ASSERT_EQ(5, *gate2.recv(src));
    ASSERT_EQ(pid1, src);
    
    network3.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port2)});
    {
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid3));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid3));
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid3));
    }
    gate3.send(pid1, 6);
    ASSERT_EQ(6, *gate1.recv(src));
    ASSERT_EQ(pid3, src);

    network3.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port1)});
    network4.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port1)});
    {
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid3));
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid4));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid3));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid4));
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid4));
        WAIT_ASSERT_EQ(1, butcher4.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher4.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher4.routes().count(pid3));
    }
    gate4.send(pid2, 7);
    ASSERT_EQ(7, *gate2.recv(src));
    ASSERT_EQ(pid4, src);

    gate4.send({pid1, pid3}, 8);
    ASSERT_EQ(8, *gate1.recv(src));
    ASSERT_EQ(pid4, src);
    ASSERT_EQ(8, *gate3.recv());
    
    network1.stop();
    network2.stop();
    network3.stop();
    network4.stop();
    
    network1.wait();
    network2.wait();
    network3.wait();
    network4.wait();
}



TEST_F(P2PNetworkTest, NetworkBasicDisconnect)
{
    static const redhorn::p2p::id::router rid1(redhorn::p2p::id::router::generate());
    static const redhorn::p2p::id::router rid2(redhorn::p2p::id::router::generate());
    static const redhorn::p2p::id::router rid3(redhorn::p2p::id::router::generate());
    static const redhorn::p2p::id::router rid4(redhorn::p2p::id::router::generate());
    static const redhorn::rsa::key key1 = redhorn::rsa::key::generate(1024);
    static const redhorn::rsa::key key2 = redhorn::rsa::key::generate(1024);
    static const redhorn::rsa::key key3 = redhorn::rsa::key::generate(1024);
    static const redhorn::rsa::key key4 = redhorn::rsa::key::generate(1024);
    static const redhorn::p2p::id::peer pid1(key1.fingerprint());
    static const redhorn::p2p::id::peer pid2(key2.fingerprint());
    static const redhorn::p2p::id::peer pid3(key3.fingerprint());
    static const redhorn::p2p::id::peer pid4(key4.fingerprint());

    static const unsigned int port1 = 314547;
    static const unsigned int port2 = 314548;
    static const unsigned int port3 = 314549;
    static const unsigned int port4 = 314550;

    std::chrono::milliseconds beat = std::chrono::milliseconds(20);
    network_callback cb;

    network network1(redhorn::logger("p2p::network1"),
                     rid1,
                     {port1},
                     {redhorn::p2p::make_endpoint("127.0.0.1", port1)},
                     beat,
                     beat);
    network network2(redhorn::logger("p2p::network2"),
                     rid2,
                     {port2},
                     {redhorn::p2p::make_endpoint("127.0.0.1", port2)},
                     beat,
                     beat);
    network network3(redhorn::logger("p2p::network3"),
                     rid3,
                     {port3},
                     {redhorn::p2p::make_endpoint("127.0.0.1", port3)},
                     beat,
                     beat);
    network network4(redhorn::logger("p2p::network4"),
                     rid4,
                     {port4},
                     {redhorn::p2p::make_endpoint("127.0.0.1", port4)},
                     beat,
                     beat);

    network::gate gate1 = network1.join(key1);
    network::gate gate2 = network2.join(key2);
    network::gate gate3 = network3.join(key3);
    network::gate gate4 = network4.join(key4);
    
    redhorn::butcher<P2PNetworkTest> butcher1(network1);
    redhorn::butcher<P2PNetworkTest> butcher2(network2);
    redhorn::butcher<P2PNetworkTest> butcher3(network3);
    redhorn::butcher<P2PNetworkTest> butcher4(network4);

    network1.subscribe(&cb);
    network2.subscribe(&cb);
    network3.subscribe(&cb);
    network4.subscribe(&cb);

    network1.start(3);
    network2.start(3);
    network3.start(3);
    network4.start(3);

// Starting network    
    {
        network2.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port1)});
        network3.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port2)});
        network1.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port3)});
        network4.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port1)});

// Own routes only
        {
            WAIT_ASSERT_EQ(4, butcher1.routes().size());

            WAIT_ASSERT_EQ(1, butcher1.routes().count(pid1));
            WAIT_ASSERT_EQ(1, butcher1.routes().count(pid2));
            WAIT_ASSERT_EQ(1, butcher1.routes().count(pid3));
            WAIT_ASSERT_EQ(1, butcher1.routes().count(pid4));

            WAIT_ASSERT_EQ(1, butcher1.routes().at(pid1).size());
            WAIT_ASSERT_EQ(2, butcher1.routes().at(pid2).size());
            WAIT_ASSERT_EQ(2, butcher1.routes().at(pid3).size());
            WAIT_ASSERT_EQ(2, butcher1.routes().at(pid4).size());
        
        }

        {
            WAIT_ASSERT_EQ(4, butcher2.routes().size());

            WAIT_ASSERT_EQ(1, butcher2.routes().count(pid1));
            WAIT_ASSERT_EQ(1, butcher2.routes().count(pid2));
            WAIT_ASSERT_EQ(1, butcher2.routes().count(pid3));
            WAIT_ASSERT_EQ(1, butcher2.routes().count(pid4));

            WAIT_ASSERT_EQ(2, butcher2.routes().at(pid1).size());
            WAIT_ASSERT_EQ(1, butcher2.routes().at(pid2).size());
            WAIT_ASSERT_EQ(2, butcher2.routes().at(pid3).size());
            WAIT_ASSERT_EQ(3, butcher2.routes().at(pid4).size());

        }

        {
            WAIT_ASSERT_EQ(4, butcher3.routes().size());

            WAIT_ASSERT_EQ(1, butcher3.routes().count(pid1));
            WAIT_ASSERT_EQ(1, butcher3.routes().count(pid2));
            WAIT_ASSERT_EQ(1, butcher3.routes().count(pid3));
            WAIT_ASSERT_EQ(1, butcher3.routes().count(pid4));
        
            WAIT_ASSERT_EQ(2, butcher3.routes().at(pid1).size());
            WAIT_ASSERT_EQ(2, butcher3.routes().at(pid2).size());
            WAIT_ASSERT_EQ(1, butcher3.routes().at(pid3).size());
            WAIT_ASSERT_EQ(3, butcher3.routes().at(pid4).size());
        }

        {
            WAIT_ASSERT_EQ(4, butcher4.routes().size());

            WAIT_ASSERT_EQ(1, butcher4.routes().count(pid1));
            WAIT_ASSERT_EQ(1, butcher4.routes().count(pid2));
            WAIT_ASSERT_EQ(1, butcher4.routes().count(pid3));
            WAIT_ASSERT_EQ(1, butcher4.routes().count(pid4));
        
            WAIT_ASSERT_EQ(2, butcher4.routes().at(pid1).size());
            WAIT_ASSERT_EQ(3, butcher4.routes().at(pid2).size());
            WAIT_ASSERT_EQ(3, butcher4.routes().at(pid3).size());
            WAIT_ASSERT_EQ(1, butcher4.routes().at(pid4).size());
        }
        
    }

// Stopping 4th peer
    {
        network4.stop();
        network4.wait();

// First peer
        auto routes =  butcher1.routes();
        WAIT_ASSERT_EQ(3, butcher1.routes().size());

        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid3));

        WAIT_ASSERT_EQ(1, butcher1.routes().at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher1.routes().at(pid2).size());
        WAIT_ASSERT_EQ(2, butcher1.routes().at(pid3).size());

// Second peer
        WAIT_ASSERT_EQ(3, butcher2.routes().size());
        
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid3));

        WAIT_ASSERT_EQ(2, butcher2.routes().at(pid1).size());
        WAIT_ASSERT_EQ(1, butcher2.routes().at(pid2).size());
        WAIT_ASSERT_EQ(2, butcher2.routes().at(pid3).size());

// Third peer
        WAIT_ASSERT_EQ(3, butcher3.routes().size());
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid2));
        WAIT_ASSERT_EQ(1, butcher3.routes().count(pid3));

        WAIT_ASSERT_EQ(2, butcher3.routes().at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher3.routes().at(pid2).size());
        WAIT_ASSERT_EQ(1, butcher3.routes().at(pid3).size());
    }


    {
        network3.stop();
        network3.wait();

// First peer
        WAIT_ASSERT_EQ(2, butcher1.routes().size());
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid2));
        
        WAIT_ASSERT_EQ(1, butcher1.routes().at(pid1).size());
        WAIT_ASSERT_EQ(2, butcher1.routes().at(pid2).size());

// Second peer
        WAIT_ASSERT_EQ(2, butcher2.routes().size());
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher2.routes().count(pid2));

        WAIT_ASSERT_EQ(2, butcher2.routes().at(pid1).size());
        WAIT_ASSERT_EQ(1, butcher2.routes().at(pid2).size());
    }

    {
        network2.stop();
        network2.wait();
// First peer
        WAIT_ASSERT_EQ(1, butcher1.routes().size());
        WAIT_ASSERT_EQ(1, butcher1.routes().count(pid1));
        WAIT_ASSERT_EQ(1, butcher1.routes().at(pid1).size());
    }

    {
        network1.stop();
        network1.wait();
    }
}


TEST_F(P2PNetworkTest, NetworkBasicAnchoring)
{
    static const redhorn::p2p::id::router rid1(redhorn::p2p::id::router::generate());
    static const redhorn::p2p::id::router rid2(redhorn::p2p::id::router::generate());
    static const redhorn::p2p::id::router rid3(redhorn::p2p::id::router::generate());
    static const redhorn::p2p::id::router rid4(redhorn::p2p::id::router::generate());
    static const redhorn::rsa::key key1 = redhorn::rsa::key::generate(1024);
    static const redhorn::rsa::key key2 = redhorn::rsa::key::generate(1024);
    static const redhorn::rsa::key key3 = redhorn::rsa::key::generate(1024);
    static const redhorn::rsa::key key4 = redhorn::rsa::key::generate(1024);
    static const redhorn::p2p::id::peer pid1(key1.fingerprint());
    static const redhorn::p2p::id::peer pid2(key2.fingerprint());
    static const redhorn::p2p::id::peer pid3(key3.fingerprint());
    static const redhorn::p2p::id::peer pid4(key4.fingerprint());

    static const unsigned int port1 = 324547;
    static const unsigned int port2 = 324548;
    static const unsigned int port3 = 324549;
    static const unsigned int port4 = 324550;

    std::chrono::milliseconds beat = std::chrono::milliseconds(2000);
    std::chrono::milliseconds navigation_beat = std::chrono::milliseconds(20);
    size_t connection_count = 10;
    network_callback cb;


    network network1(redhorn::logger("p2p::network1"),
                     rid1,
                     {port1},
                     {redhorn::p2p::make_endpoint("127.0.0.1", port1)},
                     beat,
                     beat,
                     navigation_beat,
                     connection_count);                                           
    network network2(redhorn::logger("p2p::network2"),
                     rid2,
                     {port2},
                     {redhorn::p2p::make_endpoint("127.0.0.1", port2)},
                     beat,
                     beat,
                     navigation_beat,
                     connection_count);                                           
    network network3(redhorn::logger("p2p::network3"),
                     rid3,
                     {port3},
                     {redhorn::p2p::make_endpoint("non-existing-dns-name", port3), 
                      redhorn::p2p::make_endpoint("127.0.0.1", port3)},
                     beat,
                     beat,
                     navigation_beat,
                     connection_count);                                           
    network network4(redhorn::logger("p2p::network4"),
                     rid4,
                     {port4},
                     {redhorn::p2p::make_endpoint("127.0.0.1", port4)},
                     beat,
                     beat,
                     navigation_beat,
                     connection_count);                                           
                     
    network::gate gate1 = network1.join(key1);
    network::gate gate2 = network2.join(key2);
    network::gate gate3 = network3.join(key3);
    network::gate gate4 = network4.join(key4);


    redhorn::butcher<P2PNetworkTest> butcher1(network1);
    redhorn::butcher<P2PNetworkTest> butcher2(network2);
    redhorn::butcher<P2PNetworkTest> butcher3(network3);
    redhorn::butcher<P2PNetworkTest> butcher4(network4);

    network1.subscribe(&cb);
    network2.subscribe(&cb);
    network3.subscribe(&cb);
    network4.subscribe(&cb);

    network1.start(3);
    network2.start(3);
    network3.start(3);
    network4.start(3);

// Starting network    
    {

        network2.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port1)});
        network3.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port2)});
        network1.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port3)});
        network4.bootstrap({redhorn::p2p::make_endpoint("127.0.0.1", port1)});

// Own routes only
        {
            WAIT_ASSERT_EQ(4, butcher1.routes().size());

            WAIT_ASSERT_EQ(1, butcher1.routes().count(pid1));
            WAIT_ASSERT_EQ(1, butcher1.routes().count(pid2));
            WAIT_ASSERT_EQ(1, butcher1.routes().count(pid3));
            WAIT_ASSERT_EQ(1, butcher1.routes().count(pid4));
        }

        {
            WAIT_ASSERT_EQ(4, butcher2.routes().size());

            WAIT_ASSERT_EQ(1, butcher2.routes().count(pid1));
            WAIT_ASSERT_EQ(1, butcher2.routes().count(pid2));
            WAIT_ASSERT_EQ(1, butcher2.routes().count(pid3));
            WAIT_ASSERT_EQ(1, butcher2.routes().count(pid4));
        }

        {
            WAIT_ASSERT_EQ(4, butcher3.routes().size());

            WAIT_ASSERT_EQ(1, butcher3.routes().count(pid1));
            WAIT_ASSERT_EQ(1, butcher3.routes().count(pid2));
            WAIT_ASSERT_EQ(1, butcher3.routes().count(pid3));
            WAIT_ASSERT_EQ(1, butcher3.routes().count(pid4));

            WAIT_ASSERT_EQ(3, butcher3.routes().at(pid4).size());
        }

        {
            WAIT_ASSERT_EQ(4, butcher4.routes().size());

            WAIT_ASSERT_EQ(1, butcher4.routes().count(pid1));
            WAIT_ASSERT_EQ(1, butcher4.routes().count(pid2));
            WAIT_ASSERT_EQ(1, butcher4.routes().count(pid3));
            WAIT_ASSERT_EQ(1, butcher4.routes().count(pid4));

            WAIT_ASSERT_EQ(3, butcher4.routes().at(pid3).size());
        }
        
    }
    

    {
        network4.anchor(pid3);

        {
            WAIT_ASSERT_EQ(4, butcher1.routes().size());

            WAIT_ASSERT_EQ(1, butcher1.routes().count(pid1));
            WAIT_ASSERT_EQ(1, butcher1.routes().count(pid2));
            WAIT_ASSERT_EQ(1, butcher1.routes().count(pid3));
            WAIT_ASSERT_EQ(1, butcher1.routes().count(pid4));
        }

        {
            WAIT_ASSERT_EQ(4, butcher2.routes().size());

            WAIT_ASSERT_EQ(1, butcher2.routes().count(pid1));
            WAIT_ASSERT_EQ(1, butcher2.routes().count(pid2));
            WAIT_ASSERT_EQ(1, butcher2.routes().count(pid3));
            WAIT_ASSERT_EQ(1, butcher2.routes().count(pid4));
        }

        {
            WAIT_ASSERT_EQ(4, butcher3.routes().size());

            WAIT_ASSERT_EQ(1, butcher3.routes().count(pid1));
            WAIT_ASSERT_EQ(1, butcher3.routes().count(pid2));
            WAIT_ASSERT_EQ(1, butcher3.routes().count(pid3));
            WAIT_ASSERT_EQ(1, butcher3.routes().count(pid4));
        
            WAIT_ASSERT_EQ(2, butcher3.routes().at(pid4).size());
        }

        {
            WAIT_ASSERT_EQ(4, butcher4.routes().size());

            WAIT_ASSERT_EQ(1, butcher4.routes().count(pid1));
            WAIT_ASSERT_EQ(1, butcher4.routes().count(pid2));
            WAIT_ASSERT_EQ(1, butcher4.routes().count(pid3));
            WAIT_ASSERT_EQ(1, butcher4.routes().count(pid4));
        
            WAIT_ASSERT_EQ(2, butcher4.routes().at(pid3).size());
        }
        
    }    

    network1.stop();
    network2.stop();
    network3.stop();
    network4.stop();

    network1.wait();
    network2.wait();
    network3.wait();
    network4.wait();
}
