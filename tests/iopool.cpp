#include <vector>
#include <mutex>
#include <condition_variable>


#include <gtest/gtest.h>

#include <redhorn/iopool.hpp>
#include <redhorn/event.hpp>
#include <redhorn/socket/binary.impl>

#include "transport.hpp"

static const unsigned int port = 45676;


template <typename socket>
class IOPoolTest : public ::testing::Test 
{
};

TYPED_TEST_SUITE_P(IOPoolTest);


template <typename socket>
void connect_thread(redhorn::logger log)
{
    log.info("connecting");
    auto sock = socket::connect("127.0.0.1", port);
    log.info("connected");
    message m;
    auto cfg_ptr = m.alloc<transport::cfgs::WORKER>();
    cfg_ptr->value = 33.45;
    cfg_ptr->second_value = 34.45;
    log.info("sending");
    sock.send(std::move(m));
    sock.flush();
    log.info("receiving");
    m = sock.recv();
    log.info("exiting");
}
    

template <typename socket>
class recv_callback : public socket::callback
{
public:
    virtual void fetch(socket sock) override
    {
        auto log = redhorn::logger("recv_callback");
        log.info("fetch callback, fd %d", sock.handler());
        _event.notify();
    }

public:
    recv_callback(redhorn::event &e)
        : _event(e)
    {
    }

private:
    redhorn::event &_event;
};

template <typename socket>
class accept_callback : public redhorn::listener::callback
{
public:
    virtual void poll(redhorn::listener listener) override
    {
        socket sock;
        do
        {
            sock = listener.accept<socket>(false);
            if (sock)
            {
                sock.subscribe(&_recv_cb);
                _iop.bind(sock);
                {
                    std::lock_guard<std::mutex> guard(_sockets_mutex);
                    _sockets.push_back(sock);
                }
                _recv_event.notify();
            }
        } while (sock);
    }

public:
    accept_callback(redhorn::iopool &iop,
                    recv_callback<socket> &recv_cb,
                    std::vector<socket> &sockets,
                    std::mutex &sockets_mutex,
                    redhorn::event &recv_event)
        : _iop(iop)
        , _recv_cb(recv_cb)
        , _sockets(sockets)
        , _sockets_mutex(sockets_mutex)
        , _log(redhorn::logger("accept_callback"))
        , _recv_event(recv_event)
          
    {
    }

private:
    redhorn::iopool &_iop;
    recv_callback<socket> &_recv_cb;
    std::vector<socket> &_sockets;
    std::mutex &_sockets_mutex;
    redhorn::logger _log;
    redhorn::event &_recv_event;
};

TYPED_TEST_P(IOPoolTest, Basic) 
{
    typedef TypeParam socket;
    
    auto log = redhorn::logger("main");
    log.info("Starting IOPoolTest Basic");
    std::vector<socket> sockets;
    std::mutex sockets_mutex;
    redhorn::event recv_event;

    redhorn::iopool pool(redhorn::logger("iopool"));
    pool.start(2);
    
    recv_callback<socket> recv_cb(recv_event);
    accept_callback<socket> accept_cb(pool, 
                                      recv_cb, 
                                      sockets,
                                      sockets_mutex,
                                      recv_event);

    redhorn::listener listener(port);
    listener.subscribe(&accept_cb);
    pool.bind(listener);

    constexpr const size_t threads_count = 3;
    std::vector<std::thread> threads;
    for (size_t i = 0; i < threads_count; ++i)
        threads.emplace_back([i]()
                             {
                                 redhorn::logging::name_this_thread("connect." + std::to_string(i));
                                 auto log = redhorn::logger("clients");
                                 connect_thread<socket>(log);
                             });
    
    size_t recv_count = 0;
    while (recv_count < threads_count)
    {
        log.info("waiting for event, %d", recv_count);
        recv_event.wait();
        {
            std::unique_lock<std::mutex> guard(sockets_mutex);
            for (auto &sock : sockets)
            {
                log.info("try recv from, %d", sock.handler());
                message m = sock.recv(false);
                if (m)
                {
                    log.info("received from, %d", sock.handler());
                    ++recv_count;
                    log.info("recv count %zd", recv_count);
                    sock.send(std::move(m));
                }
            }
        }
    }

    log.info("joining treads");
    for (auto &thread : threads)
        thread.join();

    log.info("unbind listener");
    pool.unbind(listener);

    log.info("unbind sockets");
    for (auto &sock : sockets)
    {
        pool.unbind(sock);
    }

    log.info("stop pool");
    pool.stop();
    pool.wait();
    pool.flush();

    listener.close();
    for (auto &sock : sockets)
        sock.close();
    
}


REGISTER_TYPED_TEST_SUITE_P(IOPoolTest,
                           Basic);

typedef ::testing::Types<redhorn::json::socket<message>, redhorn::binary::socket<message>> SocketTypes;

INSTANTIATE_TYPED_TEST_SUITE_P(IOPoolTests, IOPoolTest, SocketTypes);
