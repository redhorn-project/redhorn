#include <gtest/gtest.h>

#include "transport.hpp"

#include <redhorn/sequence/dtype.hpp>
#include <redhorn/set/dtype.hpp>
#include <redhorn/map/dtype.hpp>
#include <redhorn/string/dtype.hpp>
#include <redhorn/packet/dtype.hpp>
#include <redhorn/message/dtype.hpp>
#include <redhorn/padding/dtype.hpp>
#include <redhorn/locked_data/dtype.hpp>
#include <redhorn/builtin/dtype.hpp>

TEST(DTypeTest, Basic) 
{
    EXPECT_EQ("(packet, ('packet1', ( ('d1', (float, 8)), ('f1', (float, 4)), ('i1', (int, 4)), ('u1', (uint, 4)), ('b1', (bool, 1)), ('padding', (padding, 4)), ('b2', (bool, 1)), ('s1', (string,)), ('v1', (vector, (int, 4))),)))",
              redhorn::dtype<transport::packet1>::str);
    EXPECT_EQ("(packet, ('packet2', ( ('d1', (float, 8)), ('p1', (packet, ('packet1', ( ('d1', (float, 8)), ('f1', (float, 4)), ('i1', (int, 4)), ('u1', (uint, 4)), ('b1', (bool, 1)), ('padding', (padding, 4)), ('b2', (bool, 1)), ('s1', (string,)), ('v1', (vector, (int, 4))),)))), ('b1', (bool, 1)), ('p2', (packet, ('packet1', ( ('d1', (float, 8)), ('f1', (float, 4)), ('i1', (int, 4)), ('u1', (uint, 4)), ('b1', (bool, 1)), ('padding', (padding, 4)), ('b2', (bool, 1)), ('s1', (string,)), ('v1', (vector, (int, 4))),)))), ('padding', (padding, 4)), ('s1', (string,)),)))",
              redhorn::dtype<transport::packet2>::str);
    EXPECT_EQ("(packet, ('status', ( ('id', (int, 4)), ('worker', (packet, ('worker_cfg', ( ('value', (float, 8)), ('second_value', (float, 4)),)))), ('coord', (packet, ('coord_cfg', ( ('endpoint', (string,)),)))), ('name', (string,)), ('padding', (padding, 4)), ('sex', (bool, 1)), ('funds', (vector, (int, 4))), ('last', (char, 1)),)))",
              redhorn::dtype<transport::status>::str);
    EXPECT_EQ("(message, ('request', 1488, ( (('START', 0), (packet, ('start', ( ('id', (int, 4)), ('cfg', (message, ('cfgs', 1489, ( (('WORKER', 0), (packet, ('worker_cfg', ( ('value', (float, 8)), ('second_value', (float, 4)),)))), (('COORD', 1), (packet, ('coord_cfg', ( ('endpoint', (string,)),)))), (('BEAT', 2), (packet, ('empty_packet', ()))),)))),)))), (('STATUS', 1), (packet, ('status', ( ('id', (int, 4)), ('worker', (packet, ('worker_cfg', ( ('value', (float, 8)), ('second_value', (float, 4)),)))), ('coord', (packet, ('coord_cfg', ( ('endpoint', (string,)),)))), ('name', (string,)), ('padding', (padding, 4)), ('sex', (bool, 1)), ('funds', (vector, (int, 4))), ('last', (char, 1)),)))),)))",
              redhorn::dtype<transport::request::msg<transport::request::all>>::str);
}
