#include "transport.hpp"

#include <redhorn/flavored.impl>
#include <redhorn/sequence.impl>
#include <redhorn/set.impl>
#include <redhorn/map.impl>
#include <redhorn/digest.impl>
#include <redhorn/string.impl>
#include <redhorn/packet.impl>
#include <redhorn/message.impl>
#include <redhorn/padding.impl>
#include <redhorn/locked_data.impl>
#include <redhorn/builtin.impl>

#include <redhorn/socket/json.impl>
#include <redhorn/socket/binary.impl>

#include <redhorn/p2p/network.impl>

template class redhorn::socket<typename redhorn::p2p::protocol<traits>::msg, redhorn::json::socket_metadata>;

template class redhorn::p2p::network<traits>;
template class redhorn::p2p::blueprint<traits>;
template class redhorn::p2p::gate<traits>;

template class redhorn::socket<message, redhorn::json::socket_metadata>;
template class redhorn::socket<message, redhorn::binary::socket_metadata>;

template struct redhorn::_message::message<transport::request::super_header,
                                           typename redhorn::type_list::as_list<transport::request::all>::type>;
template struct redhorn::_message::message<transport::request::super_header,
                                           typename redhorn::type_list::as_list<transport::request::STATUS>::type>;
template struct redhorn::_message::message<transport::request::super_header,
                                           typename redhorn::type_list::as_list<transport::request::START>::type>;

template struct redhorn::_message::message<transport::cfgs::super_header,
                                           typename redhorn::type_list::as_list<transport::cfgs::all>::type>;
template struct redhorn::_message::message<transport::cfgs::super_header,
                                           typename redhorn::type_list::as_list<transport::cfgs::WORKER>::type>;
template struct redhorn::_message::message<transport::cfgs::super_header,
                                           typename redhorn::type_list::as_list<transport::cfgs::COORD>::type>;
template struct redhorn::_message::message<transport::cfgs::super_header,
                                           typename redhorn::type_list::as_list<transport::cfgs::BEAT>::type>;

template void redhorn::binary::write(int, const transport::packet1&);
template void redhorn::binary::write(int, const transport::packet2&);
template void redhorn::binary::write(int, const transport::worker_cfg&);
template void redhorn::binary::write(int, const transport::coord_cfg&);
template void redhorn::binary::write(int, const transport::status&);
template void redhorn::binary::write(int, const transport::start&);

template void redhorn::binary::write(int, const transport::request::msg<transport::request::all>&);
template void redhorn::binary::write(int, const transport::request::msg<transport::request::STATUS>&);
template void redhorn::binary::write(int, const transport::request::msg<transport::request::START>&);
template void redhorn::binary::write(int, const transport::cfgs::msg<transport::cfgs::all>&);


template void redhorn::binary::read(int, transport::packet1&);
template void redhorn::binary::read(int, transport::packet2&);
template void redhorn::binary::read(int, transport::worker_cfg&);
template void redhorn::binary::read(int, transport::coord_cfg&);
template void redhorn::binary::read(int, transport::status&);
template void redhorn::binary::read(int, transport::start&);


template void redhorn::binary::read(int, transport::request::msg<transport::request::all>&);
template void redhorn::binary::read(int, transport::request::msg<transport::request::STATUS>&);
template void redhorn::binary::read(int, transport::request::msg<transport::request::START>&);
template void redhorn::binary::read(int, transport::cfgs::msg<transport::cfgs::COORD>&);


template void redhorn::json::write(std::ostream&, const transport::packet1&);
template void redhorn::json::write(std::ostream&, const transport::packet2&);
template void redhorn::json::write(std::ostream&, const transport::worker_cfg&);
template void redhorn::json::write(std::ostream&, const transport::coord_cfg&);
template void redhorn::json::write(std::ostream&, const transport::status&);
template void redhorn::json::write(std::ostream&, const transport::start&);

template void redhorn::json::write(std::ostream&, const transport::request::msg<transport::request::all>&);
template void redhorn::json::write(std::ostream&, const transport::request::msg<transport::request::STATUS>&);
template void redhorn::json::write(std::ostream&, const transport::request::msg<transport::request::START>&);
template void redhorn::json::write(std::ostream&, const transport::cfgs::msg<transport::cfgs::all>&);


template void redhorn::json::read(std::istream&, transport::packet1&);
template void redhorn::json::read(std::istream&, transport::packet2&);
template void redhorn::json::read(std::istream&, transport::worker_cfg&);
template void redhorn::json::read(std::istream&, transport::coord_cfg&);
template void redhorn::json::read(std::istream&, transport::status&);
template void redhorn::json::read(std::istream&, transport::start&);


template void redhorn::json::read(std::istream&, transport::request::msg<transport::request::all>&);
template void redhorn::json::read(std::istream&, transport::request::msg<transport::request::STATUS>&);
template void redhorn::json::read(std::istream&, transport::request::msg<transport::request::START>&);
template void redhorn::json::read(std::istream&, transport::cfgs::msg<transport::cfgs::COORD>&);

template void redhorn::json::reads(const std::string&, transport::cfgs::msg<transport::cfgs::WORKER>&);

template void redhorn::json::reads(const std::string&, transport::packet1&);
template void redhorn::json::reads(const std::string&, transport::packet2&);
