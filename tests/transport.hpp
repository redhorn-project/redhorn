#ifndef REDHORN_TESTS_TRANSPORT_HPP
#define REDHORN_TESTS_TRANSPORT_HPP

#include <redhorn/flavored.decl>
#include <redhorn/sequence.decl>
#include <redhorn/set.decl>
#include <redhorn/map.decl>
#include <redhorn/string.decl>
#include <redhorn/packet.decl>
#include <redhorn/message.decl>
#include <redhorn/padding.decl>
#include <redhorn/locked_data.decl>
#include <redhorn/builtin.decl>

#include <redhorn/socket/json.decl>
#include <redhorn/socket/binary.decl>

#include <redhorn/p2p/network.decl>

namespace transport
{
    PACKET(packet1);
    FIELD(double, d1);
    FIELD(float, f1);
    FIELD(int, i1);
    FIELD(unsigned, u1);
    FIELD(bool, b1);
    FIELD(redhorn::padding<19>, padding);
    FIELD(bool, b2);
    FIELD(std::string, s1);
    FIELD(std::vector<int>, v1);
    TEKCAP(packet1);

    PACKET(packet2);
    FIELD(double, d1);
    FIELD(packet1, p1);
    FIELD(bool, b1);
    FIELD(packet1, p2);
    FIELD(redhorn::padding<7>, padding);
    FIELD(std::string, s1);
    TEKCAP(packet2);
    
    PACKET(worker_cfg);
    FIELD(double, value);
    FIELD(float, second_value);
    TEKCAP(worker_cfg);

    PACKET(coord_cfg);
    FIELD(std::string, endpoint);
    TEKCAP(coord_cfg);

    MESSAGES(cfgs, 1489);
    MESSAGE(WORKER, worker_cfg);
    MESSAGE(COORD, coord_cfg);
    MESSAGE(BEAT, redhorn::empty_packet);
    SEGASSEM(cfgs);

    PACKET(status);
    FIELD(int, id);
    FIELD(worker_cfg, worker);
    FIELD(coord_cfg, coord);
    FIELD(redhorn::locked_data<std::string>, name);
    FIELD(redhorn::padding<19>, padding);
    FIELD(bool, sex);
    FIELD(std::vector<int>, funds);
    FIELD(char, last);
    TEKCAP(status);



    PACKET(start);
    FIELD(int, id);
    FIELD(redhorn::locked_data<cfgs::msg<cfgs::all>>, cfg);
    TEKCAP(start);


    MESSAGES(request, 1488);
    MESSAGE(START, start);
    MESSAGE(STATUS, status);
    SEGASSEM(request);

    
};

typedef transport::cfgs::msg<transport::cfgs::all> message;

struct traits
{
    typedef int payload;
    template <typename _datatype> using socket = redhorn::json::socket<_datatype>;
};

typedef redhorn::p2p::network<traits> network;

#endif // REDHORN_TESTS_TRANSPORT_HPP
