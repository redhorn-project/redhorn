#include <thread>

#include <gtest/gtest.h>

#include <redhorn/listener.hpp>

#include "transport.hpp"

template <typename socket>
class ListenerTest : public ::testing::Test 
{
};

TYPED_TEST_SUITE_P(ListenerTest);

TYPED_TEST_P(ListenerTest, Basic) 
{
    typedef TypeParam socket;

    class callback : public redhorn::listener::callback
    {
    public:
        callback()
            : poll_count(0)
            , accept_count(0)
        {
        }
    public:
        virtual void poll(redhorn::listener) override
        {
            ++poll_count;
        }
        virtual void accept(redhorn::listener) override
        {
            ++accept_count;
        }
    public:
        int poll_count;
        int accept_count;
    };

    const unsigned int port = 312546;
    callback cb;
    redhorn::listener listener(port);
    listener.subscribe(&cb);
    std::thread accept_thread([&listener, &cb](){
            EXPECT_EQ(0, cb.poll_count);
            ASSERT_TRUE(listener.poll());
            EXPECT_EQ(1, cb.poll_count);
            EXPECT_EQ(0, cb.accept_count);
            socket out = listener.accept<socket>(false);
            EXPECT_EQ(1, cb.accept_count);
            ASSERT_TRUE(static_cast<bool>(out));
            message m;
            auto cfg_ptr = m.alloc<transport::cfgs::WORKER>();
            cfg_ptr->value = 33.45;
            cfg_ptr->second_value = 34.45;
            out.send(std::move(m));
            out.flush();
        });

    std::thread connect_thread([](){
            socket in = socket::connect("localhost", port);
            message m = in.recv();
            constexpr static int worker_id = transport::cfgs::WORKER::msg_id;
            ASSERT_EQ(worker_id, m.msg_id());
            auto cfg = m.get<transport::cfgs::WORKER>();
            EXPECT_DOUBLE_EQ(33.45, cfg->value);
            EXPECT_FLOAT_EQ(34.45, cfg->second_value);
        });
    accept_thread.join();
    connect_thread.join();
}

REGISTER_TYPED_TEST_SUITE_P(ListenerTest,
                           Basic);

typedef ::testing::Types<redhorn::json::socket<message>, redhorn::binary::socket<message>> SocketTypes;

INSTANTIATE_TYPED_TEST_SUITE_P(ListenerTests, ListenerTest, SocketTypes);
