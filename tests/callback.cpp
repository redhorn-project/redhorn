#include <gtest/gtest.h>

#include <redhorn/callback.hpp>
#include <redhorn/logging.hpp>
#include <redhorn/errors.hpp>

class abstract_callback
{
public:
    virtual void something_happened(){};
    virtual ~abstract_callback(){};
};


class callback : public abstract_callback
{
public:
    callback(redhorn::callback_registry<abstract_callback> &registry)
        : _registry(registry)
    {
    }

    virtual void something_happened() override
    {
        ASSERT_THROW(_registry.unsubscribe(this),
                     redhorn::deadlock_error);
    }   

private:
    redhorn::callback_registry<abstract_callback> &_registry;    
};


TEST(CallBackRegistry, DeadLockDetection) 
{

    redhorn::callback_registry<abstract_callback> registry(redhorn::logger("callbacks"));
    callback cb(registry);
    registry.subscribe(&cb);
    registry.call([](abstract_callback &cb){cb.something_happened();});
}
