#include <gtest/gtest.h>

#include "transport.hpp"

#include <redhorn/sequence/sha3.hpp>
#include <redhorn/set/sha3.hpp>
#include <redhorn/map/sha3.hpp>
#include <redhorn/string/sha3.hpp>
#include <redhorn/packet/sha3.hpp>
#include <redhorn/message/sha3.hpp>
#include <redhorn/padding/sha3.hpp>
#include <redhorn/locked_data/sha3.hpp>
#include <redhorn/builtin/sha3.hpp>

TEST(Sha3Test, Basic) 
{
    transport::request::msg<transport::request::STATUS> status;
    auto status_ptr = status.alloc<transport::request::STATUS>();
    status_ptr->id = 5;
    status_ptr->worker.value = 1.5;
    status_ptr->worker.second_value = 4.5;
    status_ptr->coord.endpoint = "trapapap";
    status_ptr->name = redhorn::make_locked<std::string>("status_name");
    status_ptr->sex = true;
    status_ptr->last = 'A';
    status_ptr->funds.resize(10);
    for (size_t i = 0; i < status_ptr->funds.size(); ++i)
        status_ptr->funds[i] = i;
    EXPECT_EQ("fb06593f49d47ebf2c16dd4e676b70049dc0cdfc5c2646540461a8de997211eb",
              redhorn::sha3::hash(status).str());

    status_ptr->last = 'B';
    EXPECT_NE("fb06593f49d47ebf2c16dd4e676b70049dc0cdfc5c2646540461a8de997211eb",
              redhorn::sha3::hash(status).str());

    EXPECT_EQ("4a71f22c060f3bd81c88c1c1d1370b5b6613f42f632024135ba29113b178b43d",
              redhorn::sha3::hash(status_ptr->worker).str());
}
 
