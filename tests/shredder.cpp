#include <gtest/gtest.h>

#include <redhorn/shredder.hpp>
#include <redhorn/logging.hpp>

class garbage
{
public:
    garbage()
    {
        ++count;
    }

    ~garbage()
    {
        --count;
    }

public:
    static size_t count;
};

size_t garbage::count = 0;

TEST(Shredder, Basic) 
{
    ASSERT_EQ(0, garbage::count);
    {
        redhorn::shredder<std::shared_ptr<garbage>> shredder(redhorn::logger("shredder"));
        std::vector<std::shared_ptr<garbage>> vec;
        static const size_t count = 10;
        for (size_t i = 0; i < count; ++i)
            vec.emplace_back(std::make_shared<garbage>());
        ASSERT_EQ(count, garbage::count);
        shredder.start();
        for (auto &it : vec)
            shredder.schedule(std::move(it));
        shredder.stop();
        shredder.wait();
    }
    ASSERT_EQ(0, garbage::count);
}
