#include <cstring>

#include <gtest/gtest.h>

#include <redhorn/logging.hpp>
#include <redhorn/signals.hpp>

int main(int argc, char **argv) 
{
    redhorn::logging::config cfg;
    if ((argc > 1) && (std::strcmp(argv[1], "-v") == 0))
        cfg[""] = redhorn::logging::level::debug;
    else
        cfg[""] = redhorn::logging::level::critical;

    redhorn::logging::init("redhorn-tests", cfg);

    redhorn::signals::block({SIGPIPE});

    testing::InitGoogleTest(&argc, argv);
    auto exit_code = RUN_ALL_TESTS();

    return exit_code;
    
}
