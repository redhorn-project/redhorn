#include <gtest/gtest.h>

#include "transport.hpp"

#include <redhorn/sequence/describe.hpp>
#include <redhorn/set/describe.hpp>
#include <redhorn/map/describe.hpp>
#include <redhorn/string/describe.hpp>
#include <redhorn/packet/describe.hpp>
#include <redhorn/message/describe.hpp>
#include <redhorn/padding/describe.hpp>
#include <redhorn/locked_data/describe.hpp>
#include <redhorn/builtin/describe.hpp>

TEST(DescribeTest, Basic) 
{
    transport::request::msg<transport::request::STATUS> status;
    auto status_ptr = status.alloc<transport::request::STATUS>();
    status_ptr->id = 5;
    status_ptr->worker.value = 1.5;
    status_ptr->worker.second_value = 4.5;
    status_ptr->coord.endpoint = "trapapap";
    status_ptr->name = redhorn::make_locked<std::string>("status_name");
    status_ptr->sex = true;
    status_ptr->last = 'A';
    status_ptr->funds.resize(10);
    for (size_t i = 0; i < status_ptr->funds.size(); ++i)
        status_ptr->funds[i] = i;
    EXPECT_EQ("('STATUS', {'id':5, 'worker':{'value':1.5, 'second_value':4.5}, 'coord':{'endpoint':\"trapapap\"}, 'name':\"status_name\", 'padding':None, 'sex':True, 'funds':[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 'last':'A'})",
              redhorn::str(status));
    EXPECT_EQ("('STATUS', {'id':5, 'worker':{'value':1.5, 'second_value':4.5}, 'coord':{'endpoint':\"trapapap\"}, 'name':\"status_name\", 'padding':None, 'sex':True, 'funds':[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 'last':'A'}), dtype=(message, ('request', 1488, ( (('STATUS', 1), (packet, ('status', ( ('id', (int, 4)), ('worker', (packet, ('worker_cfg', ( ('value', (float, 8)), ('second_value', (float, 4)),)))), ('coord', (packet, ('coord_cfg', ( ('endpoint', (string,)),)))), ('name', (string,)), ('padding', (padding, 4)), ('sex', (bool, 1)), ('funds', (vector, (int, 4))), ('last', (char, 1)),)))),)))",
              redhorn::repr(status));
}
