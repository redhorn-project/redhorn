#include <vector>
#include <mutex>
#include <condition_variable>


#include <gtest/gtest.h>

#include <redhorn/heart_beat.hpp>
#include <redhorn/iopool.hpp>
#include <redhorn/event.hpp>

#include "transport.hpp"

static const unsigned int port = 45676;


template <typename socket>
class HeartBeatTest : public ::testing::Test 
{
};

TYPED_TEST_SUITE_P(HeartBeatTest);


template <typename socket>
void connect_ping_thread(redhorn::logger log)
{
    log.info("connecting");
    auto sock = socket::connect("127.0.0.1", port);
    log.info("connected");
    for (size_t i = 0; i < 5; ++i)
    {
        log.info("recevinig");
        auto m = sock.recv();
        log.info("received");
        int beat_id = transport::cfgs::BEAT::msg_id;
        ASSERT_EQ(beat_id, m.msg_id());
    }
    log.info("exiting");
}

template <typename socket>
void connect_pong_thread(redhorn::logger log, socket &sock)
{
    log.info("connecting");
    sock = socket::connect("127.0.0.1", port);
    log.info("exiting");
}


redhorn::locked_data<message> beat_data()
{
    message msg;
    msg.alloc<transport::cfgs::BEAT>();
    return redhorn::make_locked<message>(std::move(msg));
}

TYPED_TEST_P(HeartBeatTest, Ping) 
{
    typedef TypeParam socket;
    auto log = redhorn::logger("main");

    redhorn::iopool pool(redhorn::logger("iopool"));
    redhorn::heart_beat heart(redhorn::logger("heart"),
                              std::chrono::milliseconds(20),
                              std::chrono::milliseconds(20));
    
    pool.start(2);
    heart.start();

    redhorn::listener listener(port);

    std::thread connect_thread(connect_ping_thread<socket>, redhorn::logger("connect_thread"));

    log.info("accepting");
    socket sock = listener.accept<socket>();
    ASSERT_TRUE(static_cast<bool>(sock));

    log.info("accepted");

    pool.bind(sock);
    heart.bind(sock, 
               std::chrono::milliseconds::zero(),
               beat_data());

    log.info("joining");
    connect_thread.join();
    log.info("joined");
    heart.unbind(sock);

    heart.stop();
    pool.stop();
    heart.wait();
    pool.wait();
    
}

TYPED_TEST_P(HeartBeatTest, Pong) 
{
    typedef TypeParam socket;
    auto log = redhorn::logger("main");

    redhorn::iopool pool(redhorn::logger("iopool"));
    redhorn::heart_beat heart(redhorn::logger("heart"),
                              std::chrono::milliseconds(20),
                              std::chrono::milliseconds(20));
    
    pool.start(2);
    heart.start();

    redhorn::listener listener(port);

    socket conn_sock;
    std::thread connect_thread(connect_pong_thread<socket>,
                               redhorn::logger("connect_thread"),
                               std::ref(conn_sock));

    log.info("accepting");
    socket sock = listener.accept<socket>();
    connect_thread.join();
    
    ASSERT_TRUE(static_cast<bool>(sock));

    log.info("accepted");

    pool.bind(sock);
    heart.bind(sock, 
               std::chrono::milliseconds(20),
               beat_data());

    EXPECT_THROW(sock.recv(), std::exception);
    log.info("before assert");
    ASSERT_FALSE(static_cast<bool>(sock));
    log.info("after assert");


    heart.unbind(sock);

    
    heart.stop();
    pool.stop();
    heart.wait();
    pool.wait();
    
}

TYPED_TEST_P(HeartBeatTest, BrokenConnection) 
{
    typedef TypeParam socket;
    auto log = redhorn::logger("main");

    redhorn::iopool pool(redhorn::logger("iopool"));
    redhorn::heart_beat heart(redhorn::logger("heart"),
                              std::chrono::milliseconds(20),
                              std::chrono::milliseconds(20));
    
    pool.start(2);
    heart.start();

    redhorn::listener listener(port);

    socket conn_sock;
    std::thread connect_thread(connect_pong_thread<socket>,
                               redhorn::logger("connect_thread"),
                               std::ref(conn_sock));

    log.info("accepting");
    socket sock = listener.accept<socket>();
    connect_thread.join();
    
    ASSERT_TRUE(static_cast<bool>(sock));

    log.info("accepted");

    pool.bind(sock);
    heart.bind(sock, 
               std::chrono::milliseconds(20),
               beat_data());

    conn_sock.close();
    EXPECT_THROW(sock.recv(), std::exception);
    log.info("before assert");
    ASSERT_FALSE(static_cast<bool>(sock));
    log.info("after assert");


    heart.unbind(sock);
    pool.unbind(sock);
    
    heart.stop();
    pool.stop();
    heart.wait();
    pool.wait();
}


REGISTER_TYPED_TEST_SUITE_P(HeartBeatTest,
                           Ping,
                           Pong,
                           BrokenConnection);

typedef ::testing::Types<redhorn::json::socket<message>, redhorn::binary::socket<message>> SocketTypes;

INSTANTIATE_TYPED_TEST_SUITE_P(HeartBeatTests, HeartBeatTest, SocketTypes);
