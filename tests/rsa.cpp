#include <gtest/gtest.h>

#include "transport.hpp"

#include <redhorn/sequence/sha3.hpp>
#include <redhorn/set/sha3.hpp>
#include <redhorn/map/sha3.hpp>
#include <redhorn/string/sha3.hpp>
#include <redhorn/packet/sha3.hpp>
#include <redhorn/message/sha3.hpp>
#include <redhorn/padding/sha3.hpp>
#include <redhorn/locked_data/sha3.hpp>
#include <redhorn/builtin/sha3.hpp>

#include <redhorn/rsa.hpp>

TEST(RsaTest, Basic) 
{
    transport::request::msg<transport::request::STATUS> status;
    auto status_ptr = status.alloc<transport::request::STATUS>();
    status_ptr->id = 5;
    status_ptr->worker.value = 1.5;
    status_ptr->worker.second_value = 4.5;
    status_ptr->coord.endpoint = "trapapap";
    status_ptr->name = redhorn::make_locked<std::string>("status_name");
    status_ptr->sex = true;
    status_ptr->last = 'A';
    status_ptr->funds.resize(10);
    for (size_t i = 0; i < status_ptr->funds.size(); ++i)
        status_ptr->funds[i] = i;

    
    redhorn::rsa::key key1 = redhorn::rsa::key::generate(1024);
    redhorn::rsa::key key2 = redhorn::rsa::key::generate(1024);
    ASSERT_NE(key1.str(), key2.str());
    ASSERT_NE(key1.certificate().str(), key2.certificate().str());

    std::string sign = redhorn::rsa::sign(key1, status);
    ASSERT_TRUE(redhorn::rsa::verify(key1.certificate(), sign, status));
    ASSERT_FALSE(redhorn::rsa::verify(key2.certificate(), sign, status));

    redhorn::rsa::key key3(key1.str());
    ASSERT_EQ(key1.str(), key3.str());

    redhorn::rsa::certificate cert3(key1.certificate().str());
    ASSERT_EQ(key3.certificate().str(), cert3.str());
    ASSERT_TRUE(redhorn::rsa::verify(key3.certificate(), sign, status));
    ASSERT_TRUE(redhorn::rsa::verify(cert3, sign, status));

    status_ptr->sex = false;
     
    ASSERT_FALSE(redhorn::rsa::verify(key1.certificate(), sign, status));
    ASSERT_FALSE(redhorn::rsa::verify(key2.certificate(), sign, status));
}
        
 
TEST(RsaTest, EmptyCertificate) 
{
    redhorn::rsa::certificate cert1;
    redhorn::rsa::certificate cert2 = redhorn::rsa::key::generate(1024).certificate();
    ASSERT_FALSE(cert1);
    ASSERT_TRUE(cert2);
}
