#include <thread>
#include <chrono>
#include <atomic>

#include <gtest/gtest.h>

#include <redhorn/threading.hpp>


TEST(Threading, BasicSignaling) 
{
    EXPECT_NO_THROW(redhorn::threading::check());
    std::thread t([](){
            EXPECT_NO_THROW(redhorn::threading::check());
            redhorn::threading::signal(std::this_thread::get_id());
            EXPECT_THROW(redhorn::threading::check(), redhorn::threading::stop_thread);
        });
    t.join();
}

TEST(Threading, SingleThread) 
{
    std::atomic<bool> exception_caught(false);
    auto f = [&exception_caught]()
        {
            try
            {
                while (true)
                {
                    redhorn::threading::check();
                    std::this_thread::sleep_for(std::chrono::milliseconds(5));
                }
            }
            catch (const redhorn::threading::stop_thread &e)
            {
                exception_caught.store(true);
            }
        };
    std::thread t(f);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    EXPECT_FALSE(exception_caught.load());
    redhorn::threading::signal(t.get_id());
    t.join();
    EXPECT_TRUE(exception_caught.load());
}

TEST(Threading, MultipleThreads) 
{
    static const int thread_count = 10;
    std::atomic<int> exception_caught(0);
    auto f = [&exception_caught]()
        {
            try
            {
                while (true)
                {
                    redhorn::threading::check();
                    std::this_thread::sleep_for(std::chrono::milliseconds(5));
                }
            }
            catch (const redhorn::threading::stop_thread &e)
            {
                exception_caught.fetch_add(1);
            }
        };
    std::vector<std::thread> threads;
    for (size_t i = 0; i < thread_count; ++i)
        threads.emplace_back(f);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    EXPECT_EQ(0, exception_caught.load());
    for (auto &t : threads)
        redhorn::threading::signal(t.get_id());
    for (auto &t : threads)
        t.join();
    EXPECT_EQ(thread_count, exception_caught.load());
}

