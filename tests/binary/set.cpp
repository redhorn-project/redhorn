#include <set>
#include <unordered_set>

#include <gtest/gtest.h>

#include <redhorn/set.hpp>
#include <redhorn/string.hpp>

#include "binary_test.hpp"

template <typename set>
class SetBinaryTest : public BinaryTest
{
};

TYPED_TEST_SUITE_P(SetBinaryTest);

namespace
{
    template <typename value_type>
    value_type create_value(size_t i)
    {
        return i * 10;
    }

    template <>
    std::string create_value<std::string>(size_t i)
    {
        return std::to_string(i * 10);
    }
};

TYPED_TEST_P(SetBinaryTest, Basic)
{
    ASSERT_TRUE(this->inited);
    TypeParam sent;
    auto inserter = std::inserter(sent, sent.end());
    for (size_t i = 0; i < 25; ++i)
        inserter = create_value<typename TypeParam::value_type>(i * 10);
    TypeParam recv;
    redhorn::binary::write(this->out, sent);
    redhorn::binary::read(this->in, recv);
    std::multiset<typename TypeParam::value_type> ordered_sent, ordered_recv;
    ordered_sent.insert(sent.begin(), sent.end());
    ordered_recv.insert(recv.begin(), recv.end());
    auto sent_iter = ordered_sent.begin();
    auto recv_iter = ordered_recv.begin();
    while ((sent_iter != ordered_sent.end()) && (recv_iter != ordered_recv.end()))
    {
        EXPECT_EQ(*sent_iter, *recv_iter);
        ++sent_iter;
        ++recv_iter;
    }
    EXPECT_TRUE(sent_iter == ordered_sent.end());
    EXPECT_TRUE(recv_iter == ordered_recv.end());
}

REGISTER_TYPED_TEST_SUITE_P(SetBinaryTest,
                           Basic);


typedef ::testing::Types<std::set<int>,
                         std::multiset<int>,
                         std::unordered_set<int>,
                         std::unordered_multiset<int>,
                         std::set<std::string>,
                         std::multiset<std::string>,
                         std::unordered_set<std::string>,
                         std::unordered_multiset<std::string>> SetTypes;

INSTANTIATE_TYPED_TEST_SUITE_P(SetBinaryTests, SetBinaryTest, SetTypes);

