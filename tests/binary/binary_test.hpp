#include <cstdlib>
#include <climits>
#include <cstring>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <gtest/gtest.h>

static const int five = 5;
static const float pi = 3.14;
static const double e = 2.7182818;
static const std::string hello = "Hello guys!!";


class BinaryTest : public ::testing::Test 
{
public:
    
public:
    BinaryTest()
        : inited(false)
        , in(-1)
        , out(-1)
    {
        char templ[255];
        std::strcpy(templ, "/tmp/XXXXXX");
        int fd = mkstemp(templ);
        if (fd == -1)
            return;
        close(fd);
        in = open(templ, O_RDONLY);
        out = open(templ, O_WRONLY);
        unlink(templ);

        if ((in != -1) && (out != -1))
            inited = true;
    }

    virtual ~BinaryTest()
    {
        if (in != -1)
            close(in);
        if (out != -1)
            close(out);
    }

    bool inited;
    int in;
    int out;
};
