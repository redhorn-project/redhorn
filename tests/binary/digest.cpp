#include <gtest/gtest.h>

#include <redhorn/digest.hpp>

#include "binary_test.hpp"

TEST_F(BinaryTest, Digest)
{
    ASSERT_TRUE(inited);
    redhorn::digest<8> sent;
    for (size_t i = 0; i < sent.size; ++i)
        sent[i] = i * 98;
    redhorn::digest<8> recv;
    EXPECT_NE(sent, recv);
    redhorn::binary::write(out, sent);
    redhorn::binary::read(in, recv);
    EXPECT_EQ(sent, recv);
}
