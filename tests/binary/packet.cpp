#include <vector>
#include <string>

#include <gtest/gtest.h>


#include "binary_test.hpp"

#include "../transport.hpp"

TEST_F(BinaryTest, SimplePacket)
{
    ASSERT_TRUE(inited);
    transport::packet1 sent;
    sent.d1 = e;
    sent.d1 = e;
    sent.f1 = pi;
    sent.i1 = five;
    sent.u1 = five;
    sent.b1 = true;
    sent.b2 = false;
    sent.s1 = hello;
    for (size_t i = 0; i < 25; ++i)
        sent.v1.push_back(i * 10);

    redhorn::binary::write(out, sent);

    transport::packet1 recv;
    redhorn::binary::read(in, recv);
    
    EXPECT_EQ(e, recv.d1);
    EXPECT_EQ(pi, recv.f1);
    EXPECT_EQ(five, recv.i1);
    EXPECT_EQ(five, recv.u1);
    EXPECT_EQ(true, recv.b1);
    EXPECT_EQ(false, recv.b2);
    EXPECT_EQ(hello, recv.s1);
    ASSERT_EQ(sent.v1.size(), recv.v1.size());
    for (size_t i = 0; i < sent.v1.size(); ++i)
        EXPECT_EQ(sent.v1[i], recv.v1[i]);
}

TEST_F(BinaryTest, NestedPackets)
{
    ASSERT_TRUE(inited);
    transport::packet2 sent;
    sent.d1 = e;
    sent.p1.d1 = e;
    sent.p1.f1 = pi;
    sent.p1.i1 = five;
    sent.p1.u1 = five;
    sent.p1.b1 = true;
    sent.p1.b2 = false;
    sent.p1.s1 = hello;
    for (size_t i = 0; i < 25; ++i)
        sent.p1.v1.push_back(i * 10);
    sent.b1 = true;
    sent.p2.d1 = e;
    sent.p2.f1 = pi;
    sent.p2.i1 = five;
    sent.p2.u1 = 4;
    sent.p2.b1 = true;
    sent.p2.b2 = false;
    sent.p2.s1 = hello;
    for (size_t i = 0; i < 5; ++i)
        sent.p2.v1.push_back(i * 2);
    sent.s1 = hello;

    redhorn::binary::write(out, sent);

    transport::packet2 recv;
    redhorn::binary::read(in, recv);
    
    EXPECT_EQ(e, recv.d1);
    EXPECT_EQ(e, recv.p1.d1);
    EXPECT_EQ(pi, recv.p1.f1);
    EXPECT_EQ(five, recv.p1.i1);
    EXPECT_EQ(five, recv.p1.u1);
    EXPECT_EQ(true, recv.p1.b1);
    EXPECT_EQ(false, recv.p1.b2);
    EXPECT_EQ(hello, recv.p1.s1);
    ASSERT_EQ(sent.p1.v1.size(), recv.p1.v1.size());
    for (size_t i = 0; i < sent.p1.v1.size(); ++i)
        EXPECT_EQ(sent.p1.v1[i], recv.p1.v1[i]);
    EXPECT_EQ(true, recv.b1);
    EXPECT_EQ(e, recv.d1);
    EXPECT_EQ(e, recv.p2.d1);
    EXPECT_EQ(pi, recv.p2.f1);
    EXPECT_EQ(five, recv.p2.i1);
    EXPECT_EQ(4, recv.p2.u1);
    EXPECT_EQ(true, recv.p2.b1);
    EXPECT_EQ(false, recv.p2.b2);
    EXPECT_EQ(hello, recv.p2.s1);
    ASSERT_EQ(sent.p2.v1.size(), recv.p2.v1.size());
    for (size_t i = 0; i < sent.p2.v1.size(); ++i)
        EXPECT_EQ(sent.p2.v1[i], recv.p2.v1[i]);
    EXPECT_EQ(hello, recv.s1);
}
