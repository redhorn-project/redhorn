#include <gtest/gtest.h>

#include <redhorn/builtin.hpp>

#include "binary_test.hpp"

TEST_F(BinaryTest, Int)
{
    ASSERT_TRUE(inited);
    int sent = five;
    int recv = 0;
    redhorn::binary::write(out, sent);
    redhorn::binary::read(in, recv);
    EXPECT_EQ(five, recv);
}

TEST_F(BinaryTest, Boolean)
{
    ASSERT_TRUE(inited);
    bool sent = true;
    bool recv = false;
    redhorn::binary::write(out, sent);
    redhorn::binary::read(in, recv);
    EXPECT_EQ(true, recv);
}

TEST_F(BinaryTest, UnignedInt)
{
    ASSERT_TRUE(inited);
    unsigned int sent = five;
    unsigned int recv = 0;
    redhorn::binary::write(out, sent);
    redhorn::binary::read(in, recv);
    EXPECT_EQ(five, recv);
}

TEST_F(BinaryTest, Float)
{
    ASSERT_TRUE(inited);
    float sent = pi;
    float recv = 0;
    redhorn::binary::write(out, sent);
    redhorn::binary::read(in, recv);
    EXPECT_FLOAT_EQ(pi, recv);
}

TEST_F(BinaryTest, Double)
{
    ASSERT_TRUE(inited);
    double sent = e;
    double recv = 0;
    redhorn::binary::write(out, sent);
    redhorn::binary::read(in, recv);
    EXPECT_DOUBLE_EQ(e, recv);
}

TEST_F(BinaryTest, Basic)
{
    ASSERT_TRUE(inited);
    struct data
    {
        int i;
        float x;
        double a;
        unsigned long int l;
    };
    
    data sent = {five, pi, e, INT_MAX};
    data recv = {0, 0.0, 0.0, 0};
    redhorn::binary::write(out, sent.i);
    redhorn::binary::write(out, sent.x);
    redhorn::binary::write(out, sent.a);
    redhorn::binary::write(out, sent.l);
    redhorn::binary::read(in, recv.i);
    redhorn::binary::read(in, recv.x);
    redhorn::binary::read(in, recv.a);
    redhorn::binary::read(in, recv.l);
    EXPECT_EQ(five, recv.i);
    EXPECT_FLOAT_EQ(pi, recv.x);
    EXPECT_DOUBLE_EQ(e, recv.a);
    EXPECT_EQ(INT_MAX, recv.l);
}

