#include <vector>
#include <list>
#include <deque>

#include <gtest/gtest.h>

#include <redhorn/sequence.hpp>
#include <redhorn/string.hpp>

#include "binary_test.hpp"

template <typename sequence>
class SequenceBinaryTest : public BinaryTest
{
};

TYPED_TEST_SUITE_P(SequenceBinaryTest);

namespace
{
    template <typename value_type>
    value_type create_value(size_t i)
    {
        return i * 10;
    }

    template <>
    std::string create_value<std::string>(size_t i)
    {
        return std::to_string(i * 10);
    }
};

TYPED_TEST_P(SequenceBinaryTest, Basic)
{
    ASSERT_TRUE(this->inited);
    TypeParam sent;
    auto inserter = std::inserter(sent, sent.end());
    for (size_t i = 0; i < 25; ++i)
        inserter = create_value<typename TypeParam::value_type>(i * 10);
    TypeParam recv;
    redhorn::binary::write(this->out, sent);
    redhorn::binary::read(this->in, recv);
    auto sent_iter = sent.begin();
    auto recv_iter = recv.begin();
    while ((sent_iter != sent.end()) && (recv_iter != recv.end()))
    {
        EXPECT_EQ(*sent_iter, *recv_iter);
        ++sent_iter;
        ++recv_iter;
    }
    EXPECT_TRUE(sent_iter == sent.end());
    EXPECT_TRUE(recv_iter == recv.end());
}

REGISTER_TYPED_TEST_SUITE_P(SequenceBinaryTest,
                           Basic);

typedef ::testing::Types<std::vector<int>, 
                         std::list<int>,
                         std::deque<int>,
                         std::vector<std::string>, 
                         std::list<std::string>,
                         std::deque<std::string>> SequenceTypes;

INSTANTIATE_TYPED_TEST_SUITE_P(SequenceBinaryTests, SequenceBinaryTest, SequenceTypes);
