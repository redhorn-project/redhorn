#include <gtest/gtest.h>

#include <redhorn/string.hpp>

#include "binary_test.hpp"

TEST_F(BinaryTest, String)
{
    ASSERT_TRUE(inited);
    std::string sent = hello;
    std::string recv;
    redhorn::binary::write(out, sent);
    redhorn::binary::read(in, recv);
    EXPECT_EQ(hello, recv);
}
