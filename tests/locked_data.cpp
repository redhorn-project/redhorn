#include <gtest/gtest.h>

#include <redhorn/locked_data.hpp>

#include "transport.hpp"

TEST(LockedData, Basic)
{
    typedef transport::request::msg<transport::request::STATUS> message;
    message status;
    {
        auto status_ptr = status.alloc<transport::request::STATUS>();
        status_ptr->id = 5;
        status_ptr->worker.value = 1.5;
        status_ptr->worker.second_value = 4.5;
        status_ptr->coord.endpoint = "trapapap";
        status_ptr->name = redhorn::make_locked<std::string>("status_name");
        status_ptr->sex = true;
        status_ptr->last = 'A';
        status_ptr->funds.resize(10);
        for (size_t i = 0; i < status_ptr->funds.size(); ++i)
            status_ptr->funds[i] = i;
    }

    redhorn::locked_data<message> locked1;
    redhorn::locked_data<message> locked2;

    ASSERT_TRUE(status);
    ASSERT_FALSE(locked1);
    ASSERT_FALSE(locked2);
    
    locked1 = redhorn::make_locked<message>(std::move(status));
    ASSERT_FALSE(status);
    ASSERT_TRUE(locked1);
    ASSERT_FALSE(locked2);

    locked2 = locked1;
    ASSERT_FALSE(status);
    ASSERT_TRUE(locked1);
    ASSERT_TRUE(locked2);

    {
        constexpr static int status_id = transport::request::STATUS::msg_id;
        ASSERT_EQ(status_id, locked1->msg_id());
        ASSERT_EQ("STATUS", locked1->msg_name());

        auto status_ptr = locked1->get<transport::request::STATUS>();
        EXPECT_EQ(5, status_ptr->id);
        EXPECT_DOUBLE_EQ(1.5, status_ptr->worker.value);
        EXPECT_FLOAT_EQ(4.5, status_ptr->worker.second_value);
        EXPECT_EQ("trapapap", status_ptr->coord.endpoint);
        EXPECT_EQ("status_name", *(status_ptr->name));
        EXPECT_EQ(true, status_ptr->sex);
        EXPECT_EQ('A', status_ptr->last);
        ASSERT_EQ(10, status_ptr->funds.size());
        for (size_t i = 0; i < status_ptr->funds.size(); ++i)
            EXPECT_EQ(i, status_ptr->funds[i]);
        
    }

    {
        constexpr static int status_id = transport::request::STATUS::msg_id;
        ASSERT_EQ(status_id, locked2->msg_id());
        ASSERT_EQ("STATUS", locked2->msg_name());

        auto status_ptr = locked2->get<transport::request::STATUS>();
        EXPECT_EQ(5, status_ptr->id);
        EXPECT_DOUBLE_EQ(1.5, status_ptr->worker.value);
        EXPECT_FLOAT_EQ(4.5, status_ptr->worker.second_value);
        EXPECT_EQ("trapapap", status_ptr->coord.endpoint);
        EXPECT_EQ("status_name", *(status_ptr->name));
        EXPECT_EQ(true, status_ptr->sex);
        EXPECT_EQ('A', status_ptr->last);
        ASSERT_EQ(10, status_ptr->funds.size());
        for (size_t i = 0; i < status_ptr->funds.size(); ++i)
            EXPECT_EQ(i, status_ptr->funds[i]);
        
    }    
}

