#include <sys/types.h>
#include <sys/socket.h>

#include <gtest/gtest.h>

#include <redhorn/socket/filters.hpp>

#include "transport.hpp"

template <typename socket>
class SocketTest : public ::testing::Test 
{
public:
    typedef socket test_socket;
public:
    SocketTest()
        : inited(false)
        , in(-1)
        , out(-1)
    {
        int socks[2];
        if (socketpair(AF_UNIX, SOCK_STREAM, 0, socks))
            return;
        in = socks[0];
        out = socks[1];
        if ((in != -1) && (out != -1))
            inited = true;
    }

    virtual ~SocketTest()
    {
        if (in != -1)
        {
            shutdown(in, SHUT_RDWR);
            close(in);
        }
        if (out != -1)
        {
            shutdown(out, SHUT_RDWR);
            close(out);
        }
    }

    bool inited;
    int in;
    int out;
};

TYPED_TEST_SUITE_P(SocketTest);

TYPED_TEST_P(SocketTest, Basic) 
{
    typedef TypeParam socket;
    ASSERT_TRUE(this->inited);
    {
        socket out(this->out);
        message m;
        auto cfg_ptr = m.alloc<transport::cfgs::WORKER>();
        cfg_ptr->value = 33.45;
        cfg_ptr->second_value = 34.45;
        out.send(std::move(m));
        out.flush();
    }
    {
        socket in(this->in);
        message m = in.recv();
        constexpr static int worker_id = transport::cfgs::WORKER::msg_id;
        ASSERT_EQ(worker_id, m.msg_id());
        auto cfg = m.get<transport::cfgs::WORKER>();
        EXPECT_DOUBLE_EQ(33.45, cfg->value);
        EXPECT_FLOAT_EQ(34.45, cfg->second_value);
        
    }
}

TYPED_TEST_P(SocketTest, BasicLockedData) 
{
    typedef TypeParam socket;
    ASSERT_TRUE(this->inited);
    {
        socket out(this->out);
        message m;
        {
            auto cfg_ptr = m.alloc<transport::cfgs::WORKER>();
            cfg_ptr->value = 33.45;
            cfg_ptr->second_value = 34.45;
        }
        redhorn::locked_data<message> locked = redhorn::make_locked<message>(std::move(m));
        out.send(locked);
        out.send(locked);
        out.flush();
        out.send(locked);
        out.send(locked);
        locked.reset();
        out.flush();
    }
    {
        socket in(this->in);
        for (int i = 0; i < 4; ++i)
        {
            message m = in.recv();
            constexpr static int worker_id = transport::cfgs::WORKER::msg_id;
            ASSERT_EQ(worker_id, m.msg_id());
            auto cfg = m.get<transport::cfgs::WORKER>();
            EXPECT_DOUBLE_EQ(33.45, cfg->value);
            EXPECT_FLOAT_EQ(34.45, cfg->second_value);
        }
    }
}

TYPED_TEST_P(SocketTest, FilterOldLady) 
{
    typedef TypeParam socket;
    ASSERT_TRUE(this->inited);
    {
        socket out(this->out);
        {
            message m;
            auto cfg_ptr = m.alloc<transport::cfgs::COORD>();
            cfg_ptr->endpoint = "hello";
            out.send(std::move(m));
        }
        {
            message m;
            auto cfg_ptr = m.alloc<transport::cfgs::WORKER>();
            cfg_ptr->value = 33.45;
            cfg_ptr->second_value = 34.45;
            out.send(std::move(m));
        }
        out.flush();
    }
    {
        socket in(this->in);
        redhorn::filters::old_lady<message, transport::cfgs::WORKER> filter;
        message m;
        EXPECT_THROW(m = in.recv(true,
                                 std::chrono::milliseconds::zero(),
                                 filter),
                     std::runtime_error);
        puts(m.msg_name().c_str());
    }
}

TYPED_TEST_P(SocketTest, Callback) 
{
    typedef TypeParam socket;
    class callback : public socket::callback
    {
    public:
        callback()
            : flush_counter(0)
            , fetch_counter(0)
            , send_counter(0)
            , recv_counter(0)
            , shutdown_counter(0)
            , close_counter(0)
        {
        }

    public:
        virtual void flush(socket) override
        {
            ++flush_counter;
        }
        virtual void fetch(socket) override
        {
            ++fetch_counter;
        }
        virtual void recv(socket) override
        {
            ++recv_counter;
        }
        virtual void send(socket) override
        {
            ++send_counter;
        }
        virtual void shutdown(socket) override
        {
            ++shutdown_counter;
        }

        virtual void close(socket) override
        {
            ++close_counter;
        }

    public:
        int flush_counter;
        int fetch_counter;
        int send_counter;
        int recv_counter;
        int shutdown_counter;
        int close_counter;
    };
    
    callback cb;
    ASSERT_TRUE(this->inited);
    {
        socket out(this->out);
        out.subscribe(&cb);
        message m;
        auto cfg_ptr = m.alloc<transport::cfgs::WORKER>();
        cfg_ptr->value = 33.45;
        cfg_ptr->second_value = 34.45;
        EXPECT_EQ(0, cb.send_counter);
        out.send(std::move(m));
        EXPECT_EQ(1, cb.send_counter);
        EXPECT_EQ(0, cb.flush_counter);
        out.flush();
        EXPECT_EQ(1, cb.flush_counter);
    }
    {
        socket in(this->in);
        in.subscribe(&cb);
        EXPECT_EQ(0, cb.recv_counter);
        message m = in.recv();
        EXPECT_EQ(1, cb.recv_counter);
        constexpr static int worker_id = transport::cfgs::WORKER::msg_id;
        ASSERT_EQ(worker_id, m.msg_id());
        auto cfg = m.get<transport::cfgs::WORKER>();
        EXPECT_DOUBLE_EQ(33.45, cfg->value);
        EXPECT_FLOAT_EQ(34.45, cfg->second_value);
        
    }
}


REGISTER_TYPED_TEST_SUITE_P(SocketTest,
                           Basic, 
                           BasicLockedData,
                           FilterOldLady,
                           Callback);


typedef ::testing::Types<redhorn::json::socket<message>, redhorn::binary::socket<message>> SocketTypes;

INSTANTIATE_TYPED_TEST_SUITE_P(SocketTests, SocketTest, SocketTypes);
