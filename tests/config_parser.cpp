#include <initializer_list>
#include <cstddef>
#include <cstring>

#include <gtest/gtest.h>

#include <redhorn/config_parser.hpp>

void parse(redhorn::config_parser &parser, std::vector<std::string> l, const std::vector<std::string> &cfg_fname = {})
{
    class scoped_args
    {
    public:
        scoped_args(std::vector<std::string> l)
            : _ptr(nullptr)
            , _size(l.size())
        {
            _ptr = new char*[_size];
            std::memset(_ptr, 0, _size);
            auto it = l.begin();
            for (size_t i = 0; i < _size; ++i)
            {
                _ptr[i] = new char[it->size() + 1];
                std::strcpy(_ptr[i], it->c_str());
                ++it;
            }
        }
            
        ~scoped_args()
        {
            if (_ptr)
            {
                for (size_t i = 0; i < _size; ++i)
                    delete [] _ptr[i];
                delete [] _ptr;
            }
        }

        char** ptr()
        {
            return _ptr;
        }

        size_t size()
        {
            return _size;
        }
    private:
        char **_ptr;
        size_t _size;
    };
    scoped_args args(std::move(l));
    parser.parse(args.size(), args.ptr(), cfg_fname);
}

TEST(ConfigParser, BasicTypes) 
{
    redhorn::config_parser parser;

    int i;
    bool b;
    std::string s;
    float f;
    double d;

    parser.add_option(i)
        .long_option("int")
        .short_option('i')
        .required(false);

    parser.add_option(b)
        .long_option("bool")
        .short_option('b')
        .required(false);

    parser.add_option(s)
        .long_option("str")
        .short_option('s')
        .required(false);

    parser.add_option(f)
        .long_option("float")
        .short_option('f')
        .required(false);

    parser.add_option(d)
        .long_option("double")
        .short_option('d')
        .required(false);

    
    parse(parser, {"test", 
                "-i", "4556",
                "-b",
                "-s", "foo bar", 
                "-d", "1.75",
                "-f", "-1.75",
                });
    EXPECT_EQ(4556, i);
    EXPECT_TRUE(b);
    EXPECT_EQ("foo bar", s);
    EXPECT_FLOAT_EQ(-1.75, f);
    EXPECT_DOUBLE_EQ(1.75, d);

}

TEST(ConfigParser, DefaultValue) 
{
    redhorn::config_parser parser;

    int i;
    std::string s;
    parser.add_option(i)
        .long_option("int")
        .short_option('i')
        .default_value(34);

    parser.add_option(s)
        .long_option("str")
        .short_option('s')
        .default_value("default");

    parse(parser, {"test",
                "-s", "not default"
                });
    EXPECT_EQ(34, i);
    EXPECT_EQ("not default", s);
}
 

TEST(ConfigParser, Required) 
{
    redhorn::config_parser parser;

    int i;

    parser.add_option(i)
        .long_option("int")
        .short_option('i')
        .required(true);

    EXPECT_THROW(parse(parser, {"test"}), std::invalid_argument);
}

TEST(ConfigParser, InvalidOption) 
{
    redhorn::config_parser parser;

    int i;

    parser.add_option(i)
        .long_option("int")
        .short_option('i')
        .required(true);

    ASSERT_DEATH(parse(parser, {"test", "-k", "5"}), "");
}


TEST(ConfigParser, LongOptions) 
{
    redhorn::config_parser parser;

    int i;
    bool b;
    std::string s;
    float f;
    double d;

    parser.add_option(i)
        .long_option("int")
        .short_option('i')
        .required(false);

    parser.add_option(b)
        .long_option("bool")
        .short_option('b')
        .required(false);

    parser.add_option(s)
        .long_option("str")
        .short_option('s')
        .required(false);

    parser.add_option(f)
        .long_option("float")
        .short_option('f')
        .required(false);

    parser.add_option(d)
        .long_option("double")
        .short_option('d')
        .required(false);

    
    parse(parser, {"test", 
                "--int=4556",
                "--bool", 
                "--str", "trapapa", 
                "--double", "1.75",
                "--float", "-1.75",
                });
    EXPECT_EQ(4556, i);
    EXPECT_TRUE(b);
    EXPECT_EQ("trapapa", s);
    EXPECT_FLOAT_EQ(-1.75, f);
    EXPECT_DOUBLE_EQ(1.75, d);

}


TEST(ConfigParser, OptionOverwrite) 
{

    class cfg_file
    {
    public:
        cfg_file()
        {
            char templ[255];
            std::strcpy(templ, "/tmp/XXXXXX");
            int fd = mkstemp(templ);
            close(fd);
            _fname = templ;
        }

        ~cfg_file()
        {
            std::remove(_fname.c_str());
        }

        const std::string &fname() const
        {
            return _fname;
        }
        
    protected:
        std::string _fname;
        
    };

    class cfg_file1 : public cfg_file
    {
    public:
        cfg_file1()
        {
            std::ofstream f(_fname);
            f << "fifth=2" << std::endl;
            f << "fourth=2" << std::endl;
            f << "third=2" << std::endl;
            f.close();
        }
    };

    class cfg_file2 : public cfg_file
    {
    public:
        cfg_file2()
        {
            std::ofstream f(_fname);
            f << "fifth=3" << std::endl;
            f << "fourth=3" << std::endl;
            f.close();
        }
    };
    
    redhorn::config_parser parser;
    int i1, i2, i3, i4, i5;
    i1 = i2 = i3 = i4 = i5 = 0;
    std::string s;
    parser.add_option(i1)
        .long_option("first")
        .short_option('a')
        .required(false);

    parser.add_option(i2)
        .long_option("second")
        .short_option('b')
        .required(false)
        .default_value(1);

    parser.add_option(i3)
        .long_option("third")
        .short_option('c')
        .required(false)
        .default_value(1);

    parser.add_option(i4)
        .long_option("fourth")
        .short_option('d')
        .required(false)
        .default_value(1);

    parser.add_option(i5)
        .long_option("fifth")
        .short_option('e')
        .required(false)
        .default_value(1);

    {
        cfg_file1 cfgf1;
        cfg_file2 cfgf2;
        parse(parser, 
              {"test", "-e", "4"},
              {cfgf1.fname(), cfgf2.fname()});
    
    }
    EXPECT_EQ(0, i1);
    EXPECT_EQ(1, i2);
    EXPECT_EQ(2, i3);
    EXPECT_EQ(3, i4);
    EXPECT_EQ(4, i5);
}
