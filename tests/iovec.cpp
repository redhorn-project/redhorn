#include <memory>

#include <gtest/gtest.h>

#include <redhorn/iovec.hpp>

TEST(IOVECTest, Basic)
{
    redhorn::iovec iov;
    EXPECT_TRUE(iov.empty());
    int i = 5;
    double d = 5.6;
    iov.push(&i);
    iov.push(&d);
    std::shared_ptr<int> i2 = std::make_shared<int>();
    iov.push_deferred([i2]
                      (redhorn::iovec &iov)
                      {
                          iov.push(i2.get());
                      });
    iov.push(&d);
    EXPECT_FALSE(iov.empty());
    redhorn::iovec::iov data = iov.top();
    ASSERT_EQ(2, data.count);
    EXPECT_EQ(&i, data.data[0].iov_base);
    EXPECT_EQ(sizeof(i), data.data[0].iov_len);
    EXPECT_EQ(&d, data.data[1].iov_base);
    EXPECT_EQ(sizeof(d), data.data[1].iov_len);
    iov.pop(sizeof(i) + sizeof(d));

    EXPECT_FALSE(iov.empty());
    data = iov.top();
    ASSERT_EQ(1, data.count);
    EXPECT_EQ(i2.get(), data.data[0].iov_base);
    EXPECT_EQ(sizeof(*i2), data.data[0].iov_len);
    iov.pop(sizeof(*i2));

    EXPECT_FALSE(iov.empty());
    data = iov.top();
    ASSERT_EQ(1, data.count);
    EXPECT_EQ(&d, data.data[0].iov_base);
    EXPECT_EQ(sizeof(d), data.data[0].iov_len);
    iov.pop(sizeof(d));

    EXPECT_TRUE(iov.empty());

}
