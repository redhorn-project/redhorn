#include <gtest/gtest.h>

#include <redhorn/flavored/flavored.hpp>
#include <redhorn/digest/digest.hpp>

TEST(Flavored, Basic) 
{
    class flavor {};
    typedef redhorn::flavored<redhorn::digest<8>, flavor> flavored_t;
    flavored_t flavored1 = static_cast<flavored_t>(flavored_t::generate());
    flavored_t flavored2 = std::move(flavored1);
    flavored_t flavored3;
    flavored3 = flavored2;
    ASSERT_EQ(flavored2, flavored3);
}

