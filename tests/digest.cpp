#include <gtest/gtest.h>

#include <redhorn/digest/digest.hpp>

TEST(Digest, Basic) 
{
    redhorn::digest<8> digest1;
    for (size_t i = 0; i < digest1.size; ++i)
        ASSERT_EQ(0, digest1[i]);
    for (size_t i = 0; i < digest1.size; ++i)
        digest1[i] = i * 98;
    ASSERT_EQ("0062c42688ea4cae", digest1.str());
    
    redhorn::digest<8> digest2(digest1.str());
    for (size_t i = 0; i < digest1.size; ++i)
        ASSERT_EQ(digest1[i], digest2[i]);
    
    ASSERT_EQ(digest1.str(), digest2.str());
    ASSERT_EQ(digest1, digest2);
    redhorn::digest<8> digest3;
    ASSERT_NE(digest1, digest3);
}

TEST(Digest, Generate)
{
    redhorn::digest<10> digest1 = redhorn::digest<10>::generate();
    redhorn::digest<10> digest2 = redhorn::digest<10>::generate();
    redhorn::digest<10> digest3 = digest1;
    redhorn::digest<10> zero = redhorn::digest<10>::zero();
    ASSERT_NE(digest1, digest2);
    ASSERT_NE(digest1, zero);
    ASSERT_NE(zero, digest2);
    ASSERT_EQ(digest1, digest3);
}
