#include <map>
#include <unordered_map>

#include <gtest/gtest.h>

#include <redhorn/map.hpp>
#include <redhorn/string.hpp>

#include "json_test.hpp"

template <typename map>
class MapJSONTest : public JSONTest
{
};

TYPED_TEST_SUITE_P(MapJSONTest);

namespace
{
    template <typename value_type>
    value_type create_value(size_t i)
    {
        return i * 10;
    }

    template <>
    std::string create_value<std::string>(size_t i)
    {
        return std::to_string(i * 10);
    }
};

TYPED_TEST_P(MapJSONTest, Basic)
{
    TypeParam sent;
    auto inserter = std::inserter(sent, sent.end());
    for (size_t i = 0; i < 25; ++i)
        inserter = std::make_pair<std::string, int>(std::to_string(i), i);
    TypeParam recv;
    redhorn::json::write(this->out, sent);
    redhorn::json::read(this->in, recv);
    std::multimap<std::string, int> ordered_sent, ordered_recv;
    ordered_sent.insert(sent.begin(), sent.end());
    ordered_recv.insert(recv.begin(), recv.end());
    auto sent_iter = ordered_sent.begin();
    auto recv_iter = ordered_recv.begin();
    while ((sent_iter != ordered_sent.end()) && (recv_iter != ordered_recv.end()))
    {
        EXPECT_EQ(sent_iter->first, recv_iter->first);
        EXPECT_EQ(sent_iter->second, recv_iter->second);
        ++sent_iter;
        ++recv_iter;
    }
    EXPECT_TRUE(sent_iter == ordered_sent.end());
    EXPECT_TRUE(recv_iter == ordered_recv.end());
}

REGISTER_TYPED_TEST_SUITE_P(MapJSONTest,
                           Basic);

typedef ::testing::Types<std::map<std::string, int>,
                         std::multimap<std::string, int>,
                         std::unordered_map<std::string, int>,
                         std::unordered_multimap<std::string, int>> MapTypes;

INSTANTIATE_TYPED_TEST_SUITE_P(MapJSONTests, MapJSONTest, MapTypes);

