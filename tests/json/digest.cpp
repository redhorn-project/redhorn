#include <gtest/gtest.h>

#include <redhorn/digest.hpp>

#include "json_test.hpp"

TEST_F(JSONTest, Digest)
{
    {
        redhorn::digest<8> recv;
        redhorn::json::reads("\"0062c42688ea4cae\"", recv);
        for (size_t i = 0; i < recv.size; ++i)
            EXPECT_EQ(redhorn::digest<8>::byte(i * 98), recv[i]);
    }
    {
        redhorn::digest<8> sent;
        for (size_t i = 0; i < sent.size; ++i)
            sent[i] = i * 98;
        redhorn::digest<8> recv;
        EXPECT_NE(sent, recv);
        redhorn::json::write(out, sent);
        redhorn::json::read(in, recv);
        EXPECT_EQ(sent, recv);
    }
}
