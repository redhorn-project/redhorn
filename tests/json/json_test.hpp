#include <cstdlib>
#include <climits>
#include <fstream>
#include <sstream>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <gtest/gtest.h>

static const int five = 5;
static const float pi = 3.14;
static const double e = 2.718;
static const std::string hello = "Hello guys!!";


class JSONTest : public ::testing::Test 
{
public:
    
public:
    JSONTest()
        : in(ss)
        , out(ss)
    {
    }

    virtual ~JSONTest()
    {
    }

    std::stringstream ss;
    std::istream &in;
    std::ostream &out;
};
