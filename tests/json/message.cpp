#include <vector>
#include <string>

#include <gtest/gtest.h>

#include <redhorn/errors.hpp>

#include "json_test.hpp"
#include "../transport.hpp"

TEST_F(JSONTest, MessageBasic)
{
    {
        transport::request::msg<transport::request::STATUS> status;
        auto status_ptr = status.alloc<transport::request::STATUS>();
        status_ptr->id = 5;
        status_ptr->worker.value = 1.5;
        status_ptr->worker.second_value = 4.5;
        status_ptr->coord.endpoint = "trapapap";
        status_ptr->name = redhorn::make_locked<std::string>("status_name");
        status_ptr->sex = true;
        status_ptr->last = 'A';
        status_ptr->funds.resize(10);
        for (size_t i = 0; i < status_ptr->funds.size(); ++i)
            status_ptr->funds[i] = i;

        redhorn::json::write(out, status);

        transport::request::msg<transport::request::START> start;
        auto start_ptr = start.alloc<transport::request::START>();
        start_ptr->id = 6;

        typedef transport::cfgs::msg<transport::cfgs::all>  cfg_t;
        cfg_t cfg;
        cfg.alloc<transport::cfgs::WORKER>();
        cfg.get<transport::cfgs::WORKER>()->value = 4.2;
        cfg.get<transport::cfgs::WORKER>()->second_value = 5.5;
        start_ptr->cfg = redhorn::make_locked<cfg_t>(std::move(cfg));
        redhorn::json::write(out, start);

        cfg.alloc<transport::cfgs::COORD>();
        cfg.get<transport::cfgs::COORD>()->endpoint = "cc endpoint";
        start_ptr->cfg = redhorn::make_locked<cfg_t>(std::move(cfg));
        redhorn::json::write(out, start);
    }
    {
        {
            transport::request::msg<transport::request::all> m;
            redhorn::json::read(in, m);

            constexpr static int status_id = transport::request::STATUS::msg_id;
            ASSERT_EQ(status_id, m.msg_id());
            ASSERT_EQ("STATUS", m.msg_name());

            auto status_ptr = m.get<transport::request::STATUS>();
            EXPECT_EQ(5, status_ptr->id);
            EXPECT_DOUBLE_EQ(1.5, status_ptr->worker.value);
            EXPECT_FLOAT_EQ(4.5, status_ptr->worker.second_value);
            EXPECT_EQ("trapapap", status_ptr->coord.endpoint);
            EXPECT_EQ("status_name", *(status_ptr->name));
            EXPECT_EQ(true, status_ptr->sex);
            EXPECT_EQ('A', status_ptr->last);
            ASSERT_EQ(10, status_ptr->funds.size());
            for (size_t i = 0; i < status_ptr->funds.size(); ++i)
                EXPECT_EQ(i, status_ptr->funds[i]);
        }
        {
            transport::request::msg<transport::request::all> m;
            redhorn::json::read(in, m);

            constexpr static int start_id = transport::request::START::msg_id;
            ASSERT_EQ(start_id, m.msg_id());
            ASSERT_EQ("START", m.msg_name());
            
            auto start_ptr = m.get<transport::request::START>();
            EXPECT_EQ(6, start_ptr->id);

            constexpr static int worker_id = transport::cfgs::WORKER::msg_id;
            ASSERT_EQ(worker_id, start_ptr->cfg->msg_id());
            ASSERT_EQ("WORKER", start_ptr->cfg->msg_name());

            EXPECT_DOUBLE_EQ(4.2, start_ptr->cfg->get<transport::cfgs::WORKER>()->value);
            EXPECT_FLOAT_EQ(5.5, start_ptr->cfg->get<transport::cfgs::WORKER>()->second_value);
        }
        {
            transport::request::msg<transport::request::all> m;
            redhorn::json::read(in, m);

            constexpr static int start_id = transport::request::START::msg_id;
            ASSERT_EQ(start_id, m.msg_id());
            ASSERT_EQ("START", m.msg_name());
            auto start_ptr = m.get<transport::request::START>();
            EXPECT_EQ(6, start_ptr->id);

            constexpr static int coord_id = transport::cfgs::COORD::msg_id;
            ASSERT_EQ(coord_id, start_ptr->cfg->msg_id());
            ASSERT_EQ("COORD", start_ptr->cfg->msg_name());
            EXPECT_EQ("cc endpoint", start_ptr->cfg->get<transport::cfgs::COORD>()->endpoint);

        }
    }
}

TEST_F(JSONTest, WrongMessage)
{
    {
        transport::cfgs::msg<transport::cfgs::all> m;
        m.alloc<transport::cfgs::WORKER>();
        redhorn::json::write(out, m);
    }

    {
        transport::cfgs::msg<transport::cfgs::COORD> m;
        EXPECT_THROW(redhorn::json::read(in, m), redhorn::invalid_json);
    }
}


TEST_F(JSONTest, ReadsMessage)
{
    {
        static const std::string msg =
            "["
            "\"WORKER\""
            ","
            "{"
            "  \"value\" : 33.45,"
            "  \"second_value\" : 34.45"
            "}"
            "]";
        transport::cfgs::msg<transport::cfgs::WORKER> m;
        redhorn::json::reads(msg, m);
        constexpr static int worker_id = transport::cfgs::WORKER::msg_id;
        ASSERT_EQ(worker_id, m.msg_id());
        ASSERT_EQ("WORKER", m.msg_name());
        auto cfg = m.get<transport::cfgs::WORKER>();
        EXPECT_DOUBLE_EQ(33.45, cfg->value);
        EXPECT_FLOAT_EQ(34.45, cfg->second_value);
    }
}

