#include <gtest/gtest.h>

#include <redhorn/builtin.hpp>

#include "json_test.hpp"

TEST_F(JSONTest, Int)
{
    {
        int recv = 0;
        redhorn::json::reads("5", recv);
        EXPECT_EQ(5, recv);
    }
    {
        int sent = five;
        int recv = 0;
        redhorn::json::write(out, sent);
        redhorn::json::read(in, recv);
        EXPECT_EQ(five, recv);
    }
}

TEST_F(JSONTest, Boolean)
{
    {
        bool recv;
        redhorn::json::reads("true", recv);
        EXPECT_EQ(true, recv);
        redhorn::json::reads("false", recv);
        EXPECT_EQ(false, recv);
    }
    {
        bool sent = true;
        bool recv = false;
        redhorn::json::write(out, sent);
        redhorn::json::read(in, recv);
        EXPECT_EQ(true, recv);
    }
}

TEST_F(JSONTest, UnignedInt)
{
    {
        unsigned int recv = 0;
        redhorn::json::reads("5", recv);
        EXPECT_EQ(5, recv);
    }
    {
        unsigned int sent = five;
        unsigned int recv = 0;
        redhorn::json::write(out, sent);
        redhorn::json::read(in, recv);
        EXPECT_EQ(five, recv);
    }
}

TEST_F(JSONTest, Float)
{
    {
        float recv = 0;
        redhorn::json::reads("3.14", recv);
        EXPECT_FLOAT_EQ(3.14f, recv);
    }
    {
        float sent = pi;
        float recv = 0;
        redhorn::json::write(out, sent);
        redhorn::json::read(in, recv);
        EXPECT_FLOAT_EQ(pi, recv);
    }
}

TEST_F(JSONTest, Double)
{
    {
        float recv = 0;
        redhorn::json::reads("2.75", recv);
        EXPECT_FLOAT_EQ(2.75, recv);
    }
    {
        double sent = e;
        double recv = 0;
        redhorn::json::write(out, sent);
        redhorn::json::read(in, recv);
        EXPECT_DOUBLE_EQ(e, recv);
    }
}

