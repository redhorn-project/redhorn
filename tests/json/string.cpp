#include <gtest/gtest.h>

#include <redhorn/string.hpp>

#include "json_test.hpp"

TEST_F(JSONTest, String)
{
    {
        std::string recv;
        redhorn::json::reads("\"hello\"", recv);
        EXPECT_EQ("hello", recv);
    }
    {
        std::string sent = hello;
        std::string recv;
        redhorn::json::write(out, sent);
        redhorn::json::read(in, recv);
        EXPECT_EQ(hello, recv);
    }
}
