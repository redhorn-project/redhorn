#ifndef REDHORN_P2P_RECENT_ENDPOINTS_HPP
#define REDHORN_P2P_RECENT_ENDPOINTS_HPP

#include <unordered_map>
#include <list>
#include <string>

#include "protocol.decl"

namespace redhorn::p2p
{
    class recent_endpoints
    {
    public:
        recent_endpoints() = default;

    public:
        void cache(endpoints endpoints);

        void cache(const endpoints &endpoints,
                   const id::peer &anchor);

        void remove(const id::peer &anchor);

        endpoints find(const id::peer &anchor) const;

        std::list<endpoints> bootstrap() const;

    private:
        std::unordered_map<id::peer, endpoints> _anchors;
        mutable std::mutex _anchors_lock;
        std::list<endpoints> _bootstrap;
        mutable std::mutex _bootstrap_lock;
    };
}
#endif // REDHORN_P2P_RECENT_ENDPOINTS_HPP
