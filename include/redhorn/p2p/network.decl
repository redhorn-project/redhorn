#ifndef REDHORN_P2P_NETWORK_DECL
#define REDHORN_P2P_NETWORK_DECL

#include <map>
#include <unordered_map>
#include <unordered_set>
#include <mutex>
#include <chrono>

#include <redhorn/logging.hpp>
#include <redhorn/event.hpp>
#include <redhorn/butcher.hpp>
#include <redhorn/listener.hpp>
#include <redhorn/iopool.hpp>
#include <redhorn/heart_beat.hpp>
#include <redhorn/queue.decl>
#include <redhorn/callback.decl>

#include "protocol.decl"
#include "checksum.hpp"
#include "directions.decl"
#include "gate.decl"
#include "callbacks.decl"
#include "blueprint.decl"
#include "navigator.decl"
#include "butler.decl"

namespace redhorn::p2p
{
    template <typename _traits>
    class network
    {
        template <typename _t>
        friend class redhorn::butcher;

        friend class gate<_traits>;
        friend class navigator<_traits>;

    public:
        typedef redhorn::p2p::protocol<_traits> protocol;
        typedef redhorn::p2p::gate<_traits> gate;
        typedef redhorn::p2p::blueprint<_traits> blueprint;
        typedef redhorn::p2p::navigator<_traits> navigator;
        typedef redhorn::p2p::butler<_traits> butler;
        typedef typename _traits::template socket<typename protocol::msg> socket;

    public:
        typedef typename blueprint::callback callback;

    private:
        typedef redhorn::p2p::direction<_traits> direction;
        typedef std::shared_ptr<direction> direction_ptr;
        typedef redhorn::p2p::router<_traits> router;
        typedef std::shared_ptr<router> router_ptr;
        typedef redhorn::p2p::peer<_traits> peer;
        typedef std::shared_ptr<peer> peer_ptr;
        typedef redhorn::rsa::certificate certificate;

    public:
        network(redhorn::logger log,
                id::router rid,
                std::vector<unsigned int> ports,
                endpoints endpoints,
                std::chrono::milliseconds ping_rate,
                std::chrono::milliseconds pong_rate,
                std::chrono::milliseconds navigation_rate = std::chrono::seconds(5),
                size_t connection_limit = 20);
        ~network();

        void start(size_t pool_size);
        void stop();
        void wait();

    public:
        void bootstrap(const endpoints &endpoints);
        gate join(redhorn::rsa::key key);

    public:
        void subscribe(callback *cb);
        void unsubscribe(callback *cb);

    public:
        void anchor(id::peer pid);
        void unanchor(const id::peer &pid);

    private:
        void process_msg(router_ptr router, typename protocol::msg &&msg);

    private:
        void accept_loop();
        void recv_loop();

    private:
        mutable redhorn::logger _log;
        redhorn::iopool _iopool;
        redhorn::heart_beat _heart;
        std::vector<redhorn::listener> _listeners;
        redhorn::event _accept_event;
        listener_callback _listener_cb;
        redhorn::event _recv_event;

        blueprint _blueprint;
        butler _butler;
        navigator _navigator;
        std::thread _accept_thread;
        std::thread _recv_thread;

    };
}

#endif // REDHORN_P2P_NETWORK_DECL
