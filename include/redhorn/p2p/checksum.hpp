#ifndef REDHORN_P2P_CHECKSUM_HPP
#define REDHORN_P2P_CHECKSUM_HPP

#include <map>
#include <unordered_map>
#include <memory>

#include <redhorn/rsa.decl>

#include "protocol.decl"

namespace redhorn::p2p
{
    redhorn::p2p::checksum router_checksum(const std::map<id::peer, redhorn::p2p::route> &routes,
                                           const std::unordered_map<id::peer, redhorn::rsa::certificate> &certificates);
    redhorn::p2p::checksum router_checksum(const std::map<id::peer, redhorn::p2p::route> &routes);
}

#endif // REDHORN_P2P_CHECKSUM_HPP

