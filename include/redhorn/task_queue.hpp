#ifndef REDHORN_TASK_QUEUE_HPP
#define REDHORN_TASK_QUEUE_HPP

#include <functional>

#include <redhorn/queue.decl>

namespace redhorn
{
    typedef queue<std::function<void()>> task_queue;
}

#endif // REDHORN_TASK_QUEUE_HPP
