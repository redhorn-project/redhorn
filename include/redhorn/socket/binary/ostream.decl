#ifndef REDHORN_SOCKET_BINARY_OSTREAM_DECL
#define REDHORN_SOCKET_BINARY_OSTREAM_DECL

#include <cstddef>
#include <memory>
#include <mutex>
#include <chrono>

#include <redhorn/logging.hpp>

namespace redhorn
{
    namespace binary
    {

        class send_error : public std::runtime_error
        {
        public:
            send_error(const std::string &what)
                :std::runtime_error(what)
            {
            }
        };


        template <typename _datatype>
        class ostream
        {
        public:
            typedef _datatype datatype;
        public:
        
            struct node 
            {
                redhorn::locked_data<datatype> data;
                redhorn::iovec iovec;
                std::unique_ptr<node> next;

                node() = default;

                node(const node&) = delete;
                node& operator=(const node&) = delete;

                node(node&&) = delete;
                node& operator=(node&&) = delete;

            };
        
            struct head
            {
                std::unique_ptr<node> base;

                head()
                    : base(std::make_unique<node>())
                {
                }

                head(const head&) = delete;
                head& operator=(const head&) = delete;

                head(head&&) = default;
                head& operator=(head&&) = default;
            };

            struct tail
            {
                node *base;

                tail(const head &h)
                    : base(h.base.get())
                {
                }

                tail(const tail&) = default;
                tail& operator=(const tail&) = default;
            };

        public:
            explicit ostream(int fd)
                : _fd(fd)
                , _head()
                , _tail(_head)
                , _old_tail(_head)
                , _log("ostream." + std::to_string(fd))
            {
            };
        
            ostream(const ostream &) = delete;
            ostream& operator=(const ostream &) = delete;
            ostream(ostream &&) = delete;
            ostream& operator=(ostream &&) = delete;

        public:
            size_t flush(bool block = true,
                         std::chrono::milliseconds timeout = std::chrono::milliseconds::zero());
            void send(redhorn::locked_data<datatype> msg);
        
            bool empty() const
            {
                std::lock_guard<std::mutex> guard(_head_mutex);
                return _head.base.get() == get_old_tail().base;
            }
        
        protected:
            size_t _flush(bool block, const tail &old_tail);

            void set_old_tail(const tail &t)
            {
                std::lock_guard<std::mutex> guard(_old_tail_mutex);
                _old_tail = t;
            }

            tail get_old_tail() const
            {
                std::lock_guard<std::mutex> guard(_old_tail_mutex);
                return _old_tail;
            }
    
        private:
            int _fd;
            head _head;
            tail _tail;
            tail _old_tail;
            redhorn::logger _log;
            mutable std::mutex _head_mutex;
            mutable std::mutex _tail_mutex;
            mutable std::mutex _old_tail_mutex;
        };
    }
}
#endif // REDHORN_SOCKET_BINARY_OSTREAM_DECL
