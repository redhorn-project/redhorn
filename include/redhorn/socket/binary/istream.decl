#ifndef REDHORN_SOCKET_BINARY_ISTREAM_DECL
#define REDHORN_SOCKET_BINARY_ISTREAM_DECL

#include <cstddef>
#include <memory>
#include <mutex>
#include <stdexcept>
#include <chrono>

#include <redhorn/logging.hpp>
#include <redhorn/iovec.hpp>
#include <redhorn/builtin/binary.decl>

namespace redhorn
{
    namespace binary
    {
        class recv_error : public std::runtime_error
        {
        public:
            using std::runtime_error::runtime_error;
        };

        class invalid_msg : public std::runtime_error
        {
        public:
            using std::runtime_error::runtime_error;
        };


        template <typename _datatype>
        class istream
        {
        public:
            typedef _datatype datatype;
        public:
            static const size_t buffer_size = 1024 * 4;
        
            struct node 
            {
                datatype data;
                redhorn::iovec iovec;
                std::unique_ptr<node> next;

                node()
                {
                    iovec = redhorn::binary::readv(data);
                }

                node(const node&) = delete;
                node& operator=(const node&) = delete;

                node(node&&) = delete;
                node& operator=(node&&) = delete;

            };
        
            struct head
            {
                std::unique_ptr<node> base;

                head()
                    : base(std::make_unique<node>())
                {
                }

                head(const head&) = delete;
                head& operator=(const head&) = delete;

                head(head&&) = default;
                head& operator=(head&&) = default;

            };

            struct tail
            {
                node *base;

                tail(const head &h)
                    : base(h.base.get())
                {
                }

                tail(const tail&) = default;
                tail& operator=(const tail&) = default;
            };

        public:
            explicit istream(int fd)
                : _fd(fd)
                , _head()
                , _tail(_head)
                , _old_tail(_head)
                , _log("ostream." + std::to_string(fd))

            {
                _tail.base->next = std::make_unique<node>();
            };

            istream(const istream &) = delete;
            istream& operator=(const istream &) = delete;
            istream(istream &&) = delete;
            istream& operator=(istream &&) = delete;

        
        public:
            size_t fetch(bool block=true);
            datatype recv(bool block=true,
                          std::chrono::milliseconds timeout = std::chrono::milliseconds::zero(),
                          const std::function<bool(const datatype &)> &filter = [](const datatype &){return true;});
        
        protected:
            datatype try_recv(const std::function<bool(const datatype &)> &filter);

        protected:
            void set_old_tail(const tail &t)
            {
                std::lock_guard<std::mutex> guard(_old_tail_mutex);
                _old_tail = t;
            }

            tail get_old_tail()
            {
                std::lock_guard<std::mutex> guard(_old_tail_mutex);
                return _old_tail;
            }
        
        private:
            int _fd;
            head _head;
            tail _tail;
            tail _old_tail;
            redhorn::logger _log;
            mutable std::mutex _head_mutex;
            mutable std::mutex _tail_mutex;
            mutable std::mutex _old_tail_mutex;
        };
    }
}
#endif // REDHORN_SOCKET_BINARY_ISTREAM_DECL

