#ifndef REDHORN_SOCKET_SOCKET_DECL
#define REDHORN_SOCKET_SOCKET_DECL

#include <chrono>
#include <atomic>

#include <redhorn/logging.hpp>
#include <redhorn/errors.hpp>

#include <redhorn/callback.decl>
#include <redhorn/locked_data/locked_data.decl>

namespace redhorn
{

    template <typename _datatype, template<typename> class _metadata>
    class socket
    {
    public:
        typedef _datatype datatype;
        typedef _metadata<datatype> metadata;

        class callback
        {
        public:
            virtual void pollin(socket){};
            virtual void pollout(socket){};
            virtual void flush(socket){};
            virtual void fetch(socket){};
            virtual void recv(socket){};
            virtual void send(socket){};
            virtual void close(socket){};
            virtual void shutdown(socket){};

            virtual ~callback(){};
        };

    private:
        class socket_data
        {
        public:
            socket_data();
            explicit socket_data(int fd);
        public:
            std::atomic<int> fd;
            std::atomic<bool> valid;
            std::unique_ptr<typename metadata::istream> istream;
            std::unique_ptr<typename metadata::ostream> ostream;
            redhorn::logger log;
            
            std::mutex dtor_mutex;
            
            callback_registry<callback> callbacks;
        };
    public:
        socket();
        explicit socket(int fd);

    public:
        socket(const socket &) = default;
        socket& operator=(const socket &) = default;
        socket(socket && other);
        socket& operator=(socket &&other);
        ~socket();

    public:
        static socket connect(const std::string &host,
                              unsigned int port);

    public:
        operator bool();
        bool empty();
        void close();
        size_t flush(bool block=true,
                     std::chrono::milliseconds timeout = std::chrono::milliseconds::zero());
        size_t fetch(bool block=true);
        datatype recv(bool block=true, 
                      std::chrono::milliseconds timeout = std::chrono::milliseconds::zero(),
                      const std::function<bool(const datatype &)> &filter = [](const datatype &){return true;});
        void send(datatype &&msg);
        void send(redhorn::locked_data<datatype> msg);
        bool pollin(bool block = true, 
                    std::chrono::milliseconds timeout = std::chrono::milliseconds::zero());
        bool pollout(bool block = true, 
                     std::chrono::milliseconds timeout = std::chrono::milliseconds::zero());
 
        int handler() const;
        std::size_t id() const;

    public:
        void subscribe(callback *cb);
        void unsubscribe(callback *cb);

    private:
        void check() const;
        bool _poll(bool block,
                   std::chrono::milliseconds timeout,
                   short event) const;
        
    private:
        std::shared_ptr<socket_data> _data;
        class nothing {};
        std::shared_ptr<nothing> _shared_counter;
    };
}
#endif // REDHORN_SOCKET_SOCKET_DECL
