#ifndef REDHORN_UTILS_JSON_HPP
#define REDHORN_UTILS_JSON_HPP

#include <ostream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <memory>
#include <string>

#include "errors.hpp"

namespace redhorn
{
    namespace utils
    {
        inline std::istream& ignore_sequence(std::istream &is, std::string expected)
        {
            std::unique_ptr<char[]> buff = std::make_unique<char[]>(expected.size() + 1);
            is.get(buff.get(), expected.size() + 1);
            if (std::string(buff.get()) != expected)
                throw invalid_json("expected: '"+expected+"', got: '"+std::string(buff.get())+"'");
            return is;
        }

        inline std::istream& strip(std::istream &is)
        {
            char ch;
            ch = is.peek();
            while (std::isspace(ch))
            {
                is.get(ch);
                ch = is.peek();
            }
            return is;
        }

        inline std::istream& read_control(std::istream &is, std::string delimiter)
        {
            strip(is);
            ignore_sequence(is, delimiter);
            return is;
        }    

        inline std::istream& read_string(std::istream &is, std::string &res)
        {
            strip(is);
            char ch;
            is.get(ch);
            if (ch != '"')
                throw invalid_json("expected string, something else found");
            bool escaped = false;
            is.get(ch);
            res.erase();
    

            while (ch != '\"' || escaped)
            {
                if (escaped)
                {
                    switch (ch)
                    {
                    case 'b': res += '\b'; break;
                    case 'f': res += '\f'; break;
                    case 'n': res += '\n'; break;
                    case 'r': res += '\r'; break;
                    case 't': res += '\t'; break;
                    case '"':
                    case '\\':
                    case '/': res += ch; break;
                    case 'u':
                    {
                        char hex_code[4];
                        is.read(hex_code, 4);
                        unsigned int code = 0;
                        std::stringstream(hex_code) >> std::hex >> code;
                        if (code > 127) 
                            throw invalid_json("proper support of \\u is not implemented yet");
                        else
                            res += (char)code;
                        break;
                    }
                    default : throw invalid_json("invalid control character");

                    }
                    escaped = false;
                }
                else if (ch == '\\')
                {
                    escaped = true;
                }
                else if (ch == '"')
                {
                }
                else 
                {
                    res += ch;
                }
                is.get(ch);
            }
        
            return is;
        }    

        inline std::istream& read_bool(std::istream &is, bool &res)
        {
            strip(is);
            char ch = is.peek();
            if (ch == 'f')
            {
                ignore_sequence(is, "false");
                res = false;
            }
            else
            {
                ignore_sequence(is, "true");
                res = true;
            }
            return is;
        }    

        inline std::istream& _read_number(std::istream &is, std::stringstream &res)
        {
            strip(is);
            char ch;
            ch = is.peek();
            bool empty = true;
            while ((ch == '.') ||
                   (ch == '-') ||
                   ((ch >= '0') && (ch <= '9')))
            {
                is.get(ch);
                res << ch;
                ch = is.peek();
                empty = false;
            }
            if (empty)
                throw invalid_json("Expected number");
            return is;
        }

        template <typename _type>
        std::istream& read_number(std::istream &is, _type &res)
        {
            std::stringstream ss;
            _read_number(is, ss);
            ss >> res;
            return is;
        }

    };
};
#endif // REDHORN_UTILS_JSON_HPP
