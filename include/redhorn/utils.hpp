#ifndef REDHORN_UTILS_HPP
#define REDHORN_UTILS_HPP

#include <memory>
#include <cstring>
#include <string>
#include <vector>
#include <stdexcept>
#include <cstdarg>
#include <cerrno>

#include <sys/uio.h>
#include <unistd.h>

#include "iovec.hpp"

namespace redhorn
{
    namespace utils
    {
        
        inline std::string format_string(const char *format, ...)
        {
            static const int MAX_STR = 1024;
            va_list args;
            va_start (args, format);
            char buff[MAX_STR];
            vsnprintf (buff, MAX_STR, format, args);    
            va_end (args);
            return std::string(buff);
        }

        template <typename T>
        class free_delete
        {
            constexpr free_delete() noexcept = default;

            template <class U, class = typename std::enable_if<std::is_convertible<U*,T*>::value>::type>
            free_delete (const free_delete<U>& ) noexcept {}

            void operator()(T* ptr) const
            {
                free(ptr);
            }
        };

        class eof_error : public std::runtime_error
        {
        public:
            eof_error(const std::string &msg) : std::runtime_error(msg) {};
        };

        class io_error : public std::runtime_error
        {
        public:
            io_error(const std::string &msg) : std::runtime_error(msg) {};
        };

        inline void safe_read(int fd, void *buf, size_t count)
        {
            size_t i = 0;
            while (i < count)
            {
                ssize_t res = ::read(fd, (char*)buf + i, count - i);
                if (res > 0)
                    i += res;
                else if (res == 0)
                    throw eof_error("end of file reached while reading");
                else 
                {
                    int err = errno;
                    throw io_error(format_string("error occurred while reading, errno: %d, strerror: %s", err, strerror(err)));
                }
            }
        }

        inline void safe_write(int fd, void *buf, size_t count)
        {
            size_t i = 0;
            while (i < count)
            {
                ssize_t res = ::write(fd, (char*)buf + i, count - i);
                if (res > 0)
                    i += res;
                else 
                {
                    int err = errno;
                    throw io_error(format_string("error occurred while reading, errno: %d, strerror: %s", err, strerror(err)));
                }

            }
        }
        
        inline void safe_readv(int fd, redhorn::iovec &read_vec)
        {
            ssize_t recv = 0;
            while (!read_vec.empty())
            {
                redhorn::iovec::iov data = read_vec.top();
                recv = ::readv(fd, data.data, data.count);
                if (recv > 0)
                {
                    read_vec.pop(recv);
                }
                else if (recv == 0)
                    throw eof_error("end of file reached while reading");
                else
                {
                    int err = errno;
                    throw io_error(format_string("error occurred while reading, errno: %d, strerror: %s", err, strerror(err)));
                }
            }
        }

        inline void safe_writev(int fd, redhorn::iovec &write_vec)
        {
            ssize_t sent = 0;
            while (!write_vec.empty())
            {
                redhorn::iovec::iov data = write_vec.top();
                sent = ::writev(fd, data.data, data.count);
                if (sent > 0)
                {
                    write_vec.pop(sent);
                }
                else
                {
                    int err = errno;
                    throw io_error(format_string("error occurred while writing, errno: %d, strerror: %s", err, strerror(err)));
                }
            }
        }

    };
};
#endif // REDHORN_UTILS_HPP
