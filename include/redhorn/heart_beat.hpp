#ifndef REDHORN_HEART_BEAT_HPP
#define REDHORN_HEART_BEAT_HPP

#include <thread>
#include <unordered_map>
#include <mutex>
#include <memory>

#include <poll.h>

#include <redhorn/task_queue.hpp>
#include <redhorn/socket/socket.hpp>
#include <redhorn/listener.hpp>
#include <redhorn/logging.hpp>

namespace redhorn
{
    class heart_beat
    {
    private:
        class info_base
        {
        public:
            virtual void send_ping(){};
            virtual void check_timestamp(){};
            virtual void disable(){};
            virtual ~info_base(){};
        };

        template <typename _datatype, template<typename> class _metadata>
        class socket_info : public info_base
        {
        private:
            typedef socket<_datatype, _metadata> socket_t;
            class callback : public socket_t::callback
            {
            public:
                callback(heart_beat &_heart);
                virtual void fetch(socket_t socket) override;
            private:
                heart_beat &_heart;
            public:
                std::atomic<std::chrono::steady_clock::time_point> _timestamp;
            };

        public:
            socket_info(heart_beat &heart,
                        socket_t s,
                        std::chrono::milliseconds threshold,
                        redhorn::locked_data<_datatype> beat_data);                        

            socket_info(const socket_info &) = delete;
            socket_info& operator=(const socket_info &) = delete;

            socket_info(socket_info && other) = delete;
            socket_info& operator=(socket_info &&other) = delete;

            ~socket_info();

            virtual void send_ping() override;
            virtual void check_timestamp() override;
            virtual void disable() override;

        private:
            std::atomic<bool> _active;
            heart_beat &_heart;
            socket_t _socket;
            callback _callback;
            std::chrono::milliseconds _threshold;
            redhorn::locked_data<_datatype> _beat_data;
        };

    public:
        heart_beat(redhorn::logger log,
                   std::chrono::milliseconds ping_rate,
                   std::chrono::milliseconds pong_rate);
        void start();
        void stop();
        void wait();

    public:
        template <typename _datatype, template<typename> class _metadata>
        void bind(socket<_datatype, _metadata> socket, 
                  std::chrono::milliseconds beat_rate,
                  redhorn::locked_data<_datatype> beat_data);
        template <typename _datatype, template<typename> class _metadata>
        void unbind(socket<_datatype, _metadata> socket);

    public:
        std::chrono::milliseconds ping_rate() const
        {
            return _ping_rate;
        }

        std::chrono::milliseconds pong_rate() const
        {
            return _pong_rate;
        }
        

    private:
        std::unordered_map<int, std::shared_ptr<info_base>> get_infos();
        void add_info(int fd, std::shared_ptr<info_base> info);
        std::shared_ptr<info_base> remove_info(int fd);

    private:
        void ping_loop();
        void pong_loop();

    private:
        redhorn::logger _log;

        enum class state
        {
            pending,
            running,
            stopping,
            joined,
        } _state;

        std::unordered_map<int, std::shared_ptr<info_base>> _info_map;
        std::mutex _map_mutex;

        std::thread _ping_thread;
        std::thread _pong_thread;

        std::chrono::milliseconds _ping_rate;
        std::chrono::milliseconds _pong_rate;
        std::condition_variable _ping_cv;
        std::condition_variable _pong_cv;
        std::mutex _ping_mutex;
        std::mutex _pong_mutex;
    };


    template <typename _datatype, template<typename> class _metadata>
    heart_beat::socket_info<_datatype, _metadata>::callback::callback(heart_beat &heart)
        : _heart(heart)
        , _timestamp(std::chrono::steady_clock::now())
    {
    }

    template <typename _datatype, template<typename> class _metadata>
    void
    heart_beat::socket_info<_datatype, _metadata>::callback::fetch(socket_t)
    {
        _timestamp.store(std::chrono::steady_clock::now());        
    };

    template <typename _datatype, template<typename> class _metadata>
    heart_beat::socket_info<_datatype, _metadata>::socket_info(heart_beat &heart,
                                                               socket_t s,
                                                               std::chrono::milliseconds threshold,
                                                               redhorn::locked_data<_datatype> beat_data)
        : _active(true)
        , _heart(heart)
        , _socket(std::move(s))
        , _callback(heart)
        , _threshold(threshold)
        , _beat_data(beat_data)
    {
        _socket.subscribe(&_callback);
    }

    template <typename _datatype, template<typename> class _metadata>
    heart_beat::socket_info<_datatype, _metadata>::~socket_info()
    {
        _socket.unsubscribe(&_callback);
    }

    template <typename _datatype, template<typename> class _metadata>
    void
    heart_beat::socket_info<_datatype, _metadata>::send_ping() 
    {
        if (_active.load())
        {
            _socket.send(_beat_data);
        }
    }

    template <typename _datatype, template<typename> class _metadata>
    void
    heart_beat::socket_info<_datatype, _metadata>::check_timestamp() 
    {
        if (_active.load())
        {
            if (_threshold != std::chrono::milliseconds::zero())
            {
                if ((std::chrono::steady_clock::now() - _callback._timestamp.load()) > _threshold * 2)
                {
                    _socket.close();
                }
            }
        }
    }
    
    template <typename _datatype, template<typename> class _metadata>
    void
    heart_beat::socket_info<_datatype, _metadata>::disable() 
    {
        _active.store(false);
    }

    template <typename _datatype, template<typename> class _metadata>
    void heart_beat::bind(socket<_datatype, _metadata> socket, 
                          std::chrono::milliseconds beat_rate,
                          redhorn::locked_data<_datatype> beat_data)
    {
        std::shared_ptr<info_base> new_info = \
            std::make_shared<socket_info<_datatype, _metadata>>(*this, 
                                                                socket, 
                                                                std::move(beat_rate), 
                                                                std::move(beat_data));
        add_info(socket.handler(), new_info);
    }

    template <typename _datatype, template<typename> class _metadata>
    void heart_beat::unbind(redhorn::socket<_datatype, _metadata> socket)
    {
        if (socket.handler() > 0)
            remove_info(socket.handler());
    }

}
#endif // REDHORN_HEART_BEAT_HPP
