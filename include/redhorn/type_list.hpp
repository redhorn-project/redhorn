#ifndef REDHORN_TYPE_LIST_HPP
#define REDHORN_TYPE_LIST_HPP

#include <cstdlib>

namespace redhorn
{
    namespace type_list
    {
        template <typename... _list>
        struct list
        {
        };

        typedef list<> empty;

        template <typename... _list>
        struct concat
        {
            typedef list<_list...> type;
        };

        template <typename... _left, typename... _right>
        struct concat<list<_left...> , _right...>
        {
            typedef list<_left..., _right...> type;
        };

        template <typename _left, typename... _right>
        struct concat<_left, list<_right...> >
        {
            typedef list<_left, _right...> type;
        };

        template <typename... _left, typename... _right>
        struct concat<list<_left...> , list<_right...> >
        {
            typedef list<_left..., _right...> type;
        };
        
        template <typename _value, typename... _list>
        struct contains;

        template <typename _value, typename _head, typename... _tail>
        struct contains<_value, _head, _tail...>
        {
            static const bool value = contains<_value, _tail...>::value;
        };

        template <typename _value>
        struct contains<_value>
        {
            static const bool value = false;
        };

        template <typename _value, typename... _tail>
        struct contains<_value, _value, _tail...>
        {
            static const bool value = true;
        };

        template <typename... _list>
        struct as_list
        {
            typedef list<_list...> type;
        };

        template <typename... _list>
        struct as_list< list<_list...> >
        {
            typedef list<_list...> type;
        };



        template <typename... _list>
        struct length;

        template <typename... _list>
        struct length<list<_list...> >
        {
            static const size_t value = length<_list...>::value;
        };

        template <typename _head, typename... _tail>
        struct length<_head, _tail...>
        {
            static const size_t value = length<_tail...>::value + 1;
        };

        template <>
        struct length<>
        {
            static const size_t value = 0;
        };
    };
};
#endif // REDHORN_TYPE_LIST_HPP
