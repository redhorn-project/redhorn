#ifndef REDHORN_LOGGING_HPP
#define REDHORN_LOGGING_HPP

#include <cstdio>
#include <cstring>
#include <iostream>
#include <sstream>
#include <memory>
#include <mutex>
#include <map>
#include <chrono>
#include <iomanip>
#include <exception>

#include <syslog.h>

#include <redhorn/threading.hpp>

namespace redhorn
{
    class logger;

    namespace logging
    {
        enum class level : int
        {
            emergency = LOG_EMERG,
            alert = LOG_ALERT,
            critical = LOG_CRIT,
            error = LOG_ERR,
            warning = LOG_WARNING,
            notice = LOG_NOTICE,
            info = LOG_INFO,
            debug = LOG_DEBUG,
        };
        
        typedef std::unordered_map<std::string, redhorn::logging::level> config;
     
        void name_this_thread(std::string name);
        
        void init();
        void init(std::string name);
        void init(std::string name, redhorn::logging::config config);
    }

    namespace _logging
    {
        std::string& thread_name();
        logging::config& config();
        logging::level threshold(std::string name);
        void terminate();

        const char* level_to_str(const logging::level &enum_level);
        
        template <logging::level _level>
        class writer
        {
        public:
            writer(const std::string &name,
                   logging::level threshold)
                : _name(name)
                , active(_level <= threshold)
            {
            }

            writer(const writer&) = delete;
            writer& operator=(const writer&) = delete;
            writer (writer&&) = delete;
            writer& operator=(writer&&) = delete;

            operator bool() const
            {
                return active;
            }

            template <typename... Args>
            bool operator()(const std::string &format_str, Args... args) const
            {
                if (active)
                {
                    if (!_name.empty())
                        syslog(static_cast<int>(_level),
                               (std::string("%s[%s] - %s - ") + format_str).c_str(), 
                               _name.c_str(), 
                               redhorn::_logging::thread_name().c_str(), 
                               _logging::level_to_str(_level),
                               std::forward<Args>(args)...);
                    else
                        syslog(static_cast<int>(_level),
                               (std::string("[%s] - %s - ") + format_str).c_str(), 
                               redhorn::_logging::thread_name().c_str(), 
                               _logging::level_to_str(_level),
                               std::forward<Args>(args)...);
                }
                return true;
            }
            
        private:
            const std::string _name;
                
        public:
            const bool active;
        };

        class exception_writer : public writer<logging::level::error>
        {
        public:
            using writer<logging::level::error>::writer;

            template <typename... Args>
            bool operator()(const std::string &format_str, Args... args) const
            {
                if (active)
                {
                    auto eptr = std::current_exception();
                    std::string frmt = format_str;
                    if (eptr)
                    {

                        try
                        {
                            std::rethrow_exception(eptr);
                        } 
                        catch(const std::exception& e) 
                        {
                            frmt += "\nError type: ";
                            frmt += typeid(e).name();
                            frmt += "\nError msg: ";
                            frmt += e.what();
                        }
                        catch(...)
                        {
                        }
                    }
                    writer<logging::level::error>::operator()(frmt.c_str(), std::forward<Args>(args)...);
                }
                return true;
            }
        };
        
    }
    
    class logger
    {
    public:
        typedef redhorn::logging::level level;
    private:
        logger(std::string name, level threshold);

    public:    
        logger();
        explicit logger(std::string name);

    public:
        logger(const logger&);
        logger& operator=(const logger&);
        logger (logger&&);
        logger& operator=(logger&&);

    public:
        logger child(const std::string &child_name);



    private:
        std::string _name;
        level _threshold;
        
    public:
        const _logging::writer<level::debug> debug;
        const _logging::writer<level::info> info;
        const _logging::writer<level::notice> notice;
        const _logging::writer<level::warning> warning;
        const _logging::writer<level::error> error;
        const _logging::writer<level::critical> critical;
        const _logging::writer<level::alert> alert;
        const _logging::writer<level::emergency> emergency;

        const _logging::exception_writer exception;
    };

}


#endif // REDHORN_LOGGING_HPP

