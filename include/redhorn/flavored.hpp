#ifndef REDHORN_FLAVORED_HPP
#define REDHORN_FLAVORED_HPP

#include "flavored/flavored.hpp"
#include "flavored/dtype.hpp"
#include "flavored/describe.hpp"
#include "flavored/binary.hpp"
#include "flavored/json.hpp"
#include "flavored/sha3.hpp"

#endif // REDHORN_FLAVORED_HPP
