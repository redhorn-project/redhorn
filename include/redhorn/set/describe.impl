#ifndef REDHORN_SET_DESCRIBE_IMPL
#define REDHORN_SET_DESCRIBE_IMPL

#include <set>
#include <unordered_set>

#include "../builtin/describe.hpp"

#include "describe.decl"

namespace redhorn
{
    namespace _describe 
    {
        template <typename _set>
        struct set_describe
        {
        public:
            static std::string str(const _set &value)
            {
                std::string res;
                for (auto iter = value.begin(); iter != value.end();)
                {
                    res += describe<typename _set::value_type>::str(*iter);
                    ++iter;
                    if (iter != value.end())
                        res += ", ";
                }
                return utils::format_string("[%s]", res.c_str());
            };
        };

        template <typename _key, typename _compare,  typename _Alloc>
        struct describe< std::set<_key, _compare, _Alloc> >
        {
        private:
            typedef std::set<_key, _compare, _Alloc> _type;

        public:
            static std::string str(const _type &value)
            {
                return set_describe<_type>::str(value);
            };
        };
        
        template <typename _key, typename _compare,  typename _Alloc>
        struct describe< std::multiset<_key, _compare, _Alloc> >
        {
        private:
            typedef std::multiset<_key, _compare, _Alloc> _type;

        public:
            static std::string str(const _type &value)
            {
                return set_describe<_type>::str(value);
            };
        };
        
        template <typename _key, typename _hash, typename _key_equal,  typename _Alloc>
        struct describe< std::unordered_set<_key, _hash, _key_equal, _Alloc> >
        {
        private:
            typedef std::unordered_set<_key, _hash, _key_equal, _Alloc> _type;

        public:
            static std::string str(const _type &value)
            {
                return set_describe<_type>::str(value);
            };
        };
        
        template <typename _key, typename _hash, typename _key_equal,  typename _Alloc>
        struct describe< std::unordered_multiset<_key, _hash, _key_equal, _Alloc> >
        {
        private:
            typedef std::unordered_multiset<_key, _hash, _key_equal, _Alloc> _type;

        public:
            static std::string str(const _type &value)
            {
                return set_describe<_type>::str(value);
            };
        };

    };
};

#endif // REDHORN_SET_DESCRIBE_IMPL
