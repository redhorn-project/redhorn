#ifndef REDHORN_SIGNALS_HPP
#define REDHORN_SIGNALS_HPP

#include <thread>
#include <functional>

#include <unistd.h>
#include <signal.h>
#include <errno.h>

#include <redhorn/logging.hpp>
#include <redhorn/event.hpp>

namespace redhorn
{
    class signals
    {
    public:
        class handlers
        {
        public:
            static void set_event(int sig, redhorn::signals &signals);
            static void terminate(int sig);
        };
        friend handlers;
    public:
        signals(const signals &) = delete;
        signals& operator=(const signals &) = delete;
        signals(signals &&) = delete;
        signals& operator=(signals &&) = delete;

        signals();
        void set_handler(int sig, std::function<void(int)> handler);

        void set_handler(int sig, const std::function<void(int, signals&)> &handler);
        void set_handler(int sig, const std::function<void()> &handler);
        void start();
        void stop();
        void wait();

    public:
        static ::sigset_t block(const std::vector<int> &sigs);

    private:
        void sig_loop();
        
    public:
        redhorn::event sigint;

    private:
        std::thread _sig_thread;
        redhorn::logger _log;
        ::sigset_t _sig_set;
        std::unordered_map<int, std::function<void(int)>> _handlers;
        std::mutex _handlers_mutex;
    };
};
#endif // REDHORN_SIGNALS_HPP
