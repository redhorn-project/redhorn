#ifndef REDHORN_PACKET_DESCRIBE_IMPL
#define REDHORN_PACKET_DESCRIBE_IMPL

#include "packet.hpp"

#include "../builtin/describe.hpp"

#include "dtype.hpp"
#include "describe.decl"

namespace redhorn
{
    namespace _describe 
    {
        template <typename... _list>
        struct dfields;
        template <typename _head, typename... _tail>
        struct dfields<_head, _tail...>
        {
            template <typename _type>
            static std::string str(const _type &value)
            {
                return utils::format_string("%s, '%s':%s", 
                                            dfields<_tail...>::str(value).c_str(),
                                            _head::name, 
                                            describe<typename _head::type>::str(*(typename _head::type*)((char*)(&value) + _head::offset)).c_str());

            }
        };

        template <typename _head>
        struct dfields<_head>
        {
            template <typename _type>
            static std::string str(const _type &value)
            {
                return utils::format_string("'%s':%s", 
                                            _head::name, 
                                            describe<typename _head::type>::str(*(typename _head::type*)((char*)(&value) + _head::offset)).c_str());
            }
        };

        template <>
        struct dfields<>
        {
            template <typename _type>
            static std::string str(const _type &value)
            {
                (void) value;
                return "";
            }
        };

        template <typename... _list>
        struct dfields<type_list::list<_list...> >
        {
            template <typename _type>
            static std::string str(const _type &value)
            {
                return dfields<_list...>::str(value);
            }
        };

        template <typename _data, typename _meta>
        struct describe< packet<_data, _meta> >
        {
        private:
            typedef  packet<_data, _meta> _type;
        public:
            static std::string str(const _type &value)
            {
                
                return utils::format_string("{%s}", dfields<typename _type::fields>::str(value).c_str());
            };
        };

    };
};
#endif // REDHORN_PACKET_DESCRIBE_IMPL
