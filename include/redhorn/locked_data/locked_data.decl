#ifndef REDHORN_LOCKED_DATA_LOCKED_DATA_DECL
#define REDHORN_LOCKED_DATA_LOCKED_DATA_DECL

#include <memory>

namespace redhorn
{
    template <typename _datatype>
    class locked_data
    {
    public:
        typedef _datatype datatype;

    public:
        locked_data() = default;
        explicit locked_data(_datatype &&data)
            : _data(std::make_shared<_datatype>(std::move(data)))
        {
        }

        locked_data(locked_data &&other) = default;
        locked_data& operator=(locked_data &&other) = default;
        locked_data(const locked_data &other) = default;
        locked_data& operator=(const locked_data &other) = default;

        operator bool() const
        {
            return static_cast<bool>(_data);
        }

        std::shared_ptr<const _datatype> operator->() const
        {
            return _data;
        }

        const _datatype& operator*() const
        {
            return *_data;
        }

        void reset()
        {
            _data.reset();
        }

    private:
        std::shared_ptr<const datatype> _data;
    };

    template <typename _datatype, typename... _args>
    locked_data<_datatype> make_locked(_args&&... args)
    {
        return locked_data<_datatype>(_datatype(std::forward<_args>(args)...));
    }
                                          
}

#endif // REDHORN_LOCKED_DATA_LOCKED_DATA_DECL
