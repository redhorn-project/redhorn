#ifndef REDHORN_IOVEC_HPP
#define REDHORN_IOVEC_HPP

#include <vector>
#include <list>
#include <functional>
#include <memory>
#include <cstring>
#include <initializer_list>

#include <sys/uio.h>

namespace redhorn
{
    class iovec
    {
    private:

        typedef std::vector<::iovec> iovec_t;
        typedef std::function<void(iovec&)> deferred_t;

        class cleanup_t
        {
        public:
            template <typename _type>
            void* alloc(const _type &init)
            {
                void* ptr = std::malloc(sizeof(_type));
                std::memcpy(ptr, &init, sizeof(_type));
                _ptrs.push_back(ptr);
                return ptr;
            }

            ~cleanup_t()
            {
                for (auto &ptr : _ptrs)
                    std::free(ptr);
            }
        private:
            std::vector<void*> _ptrs;
        };

        struct chunk
        {
            iovec_t iovec;
            deferred_t deferred;
            cleanup_t cleanup;
        };
    
    public:
        struct iov
        {
        public:
            iov(::iovec *data,
                std::size_t count)
                : data(data)
                , count(count)
            {
            }
        public:
            ::iovec *data;
            std::size_t count;
        };
    public:
        iovec()
            : offset(0)
        {
        }
        iovec(iovec&) = delete;
        iovec(iovec &&) = default;
        iovec& operator=(iovec&) = delete;
        iovec& operator=(iovec&&) = default;
    public:
        template <typename _type>
        void push(_type *value, size_t count = 1)
        {
            static_assert(std::is_arithmetic<_type>::value, "Only arichmetic types is allowed");
            if (_chunk_list.empty() || _chunk_list.back().deferred)
            {
                _chunk_list.emplace_back();
            }
            _chunk_list.back().iovec.emplace_back(
                ::iovec({static_cast<void*>(const_cast<typename std::remove_const<_type>::type*>(value)), 
                            sizeof(_type) * count}));
        }

        template <typename _type>
        void push_temp(const _type &init)
        {
            static_assert(std::is_arithmetic<_type>::value, "Only arichmetic types is allowed");
            if (_chunk_list.empty() || _chunk_list.back().deferred)
            {
                _chunk_list.emplace_back();
            }
            void *ptr = _chunk_list.back().cleanup.alloc(init);
            _chunk_list.back().iovec.emplace_back(::iovec({ptr, sizeof(_type)}));
        }

        void push_deferred(deferred_t def);
        iov top();
        bool empty();
        void pop(std::size_t size);

    private:
        void eval_deferred();
    private:
        std::list<chunk>  _chunk_list;
        size_t offset;
    };

}

#endif // REDHORN_IOVEC_HPP
