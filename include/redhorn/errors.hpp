#ifndef REDHORN_ERRORS_HPP
#define REDHORN_ERRORS_HPP

#include <stdexcept>
	
namespace redhorn
{
    class timeout_error : public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };

    class invalid_json : public std::invalid_argument
    {
    public:
        using std::invalid_argument::invalid_argument;
    };

    class invalid_socket : public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };

    class socket_create_error : public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };

    class host_error : public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };

    class connect_error : public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };

    class deadlock_error : public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };

}
#endif // REDHORN_ERRORS_HPP
