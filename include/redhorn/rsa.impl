#ifndef REDHORN_RSA_IMPL
#define REDHORN_RSA_IMPL

#include <cryptopp/osrng.h>

#include "rsa.decl"

namespace redhorn
{
    namespace rsa
    {
        template <typename _type>
        std::string sign(const redhorn::rsa::key &key, const _type &data)
        {
            redhorn::sha3::digest digest = redhorn::sha3::hash(data);
            CryptoPP::Integer m(digest.bytes(), redhorn::sha3::digest::size);
            CryptoPP::AutoSeededRandomPool prng;
            CryptoPP::Integer s;
            {
                std::lock_guard<std::mutex> lock(key._mutex);
                s = key._priv_key.CalculateInverse(prng, m);
            }

            std::string res;
            CryptoPP::HexEncoder hex(new CryptoPP::StringSink(res), false);
            s.BEREncode(hex);
            return res;

        }

        template <typename _type>
        bool verify(const redhorn::rsa::certificate &certificate,
                    const std::string &signature,
                    const _type &data)
        {

            CryptoPP::HexDecoder decoder;
            decoder.Put(reinterpret_cast<const unsigned char*>(signature.data()), signature.size() );
            decoder.MessageEnd();

            CryptoPP::Integer s;
            try
            {
                s.BERDecode(decoder);
            }
            catch (...)
            {
                return false;
            }

            CryptoPP::Integer actual_hash;
            {
                std::lock_guard<std::mutex> lock(certificate._mutex);
                actual_hash = certificate._pub_key.ApplyFunction(s);
            }

            redhorn::sha3::digest digest = redhorn::sha3::hash(data);
            CryptoPP::Integer expected_hash(digest.bytes(), redhorn::sha3::digest::size);

            return actual_hash == expected_hash;
        }

    }
}

#endif // REDHORN_RSA_IMPL
