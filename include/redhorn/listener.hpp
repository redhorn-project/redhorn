#ifndef REDHORN_LISTENER_HPP
#define REDHORN_LISTENER_HPP

#include <chrono>
#include <atomic>

#include <redhorn/logging.hpp>
#include <redhorn/errors.hpp>
#include <redhorn/callback.hpp>

namespace redhorn
{

    class bind_error : public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };

    class setsockopt_error : public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };

    class accept_error : public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };

    class listener
    {
    public:
        class callback
        {
        public:
            virtual void poll(listener) {};
            virtual void accept(listener) {};
            virtual void shutdown(listener) {};
            virtual void close(listener) {};

            virtual ~callback(){};
        };
    private:
        class listener_data
        {
        public:
            listener_data();
            explicit listener_data(unsigned int port);
        public:
            std::atomic<int> fd;
            std::atomic<bool> valid;
            redhorn::logger log;
            
            std::mutex dtor_mutex;
            callback_registry<callback> callbacks;
        };

    public:
        listener();        
        explicit listener(unsigned int port);

        listener(const listener &) = default;
        listener& operator=(const listener &) = default;

        listener(listener && other);
        listener& operator=(listener &&other);
        ~listener();
        void close();

        int handler() const;
        std::size_t id() const;

    private:
        int _non_blocking_accept() const;
        int _accept(bool block,
                    std::chrono::milliseconds timeout) const;
        bool _poll(bool block,
                   std::chrono::milliseconds timeout) const;

    public:
        template <typename socket>
        socket accept(bool block = true, 
                      std::chrono::milliseconds timeout = std::chrono::milliseconds::zero()) 
        {
            int fd = _accept(block, timeout);
            if (fd < 0)
                return socket();
            else
            {
                socket res(fd);
                _data->callbacks.call([this](callback &cb){cb.accept(*this);});
                return res;
            }
        }

        bool poll(bool block = true, 
                  std::chrono::milliseconds timeout = std::chrono::milliseconds::zero()) 
        {
            bool res = _poll(block,
                             timeout);
            if (res)
                _data->callbacks.call([this](callback &cb){cb.poll(*this);});
            return res;
        }

    public:
        void subscribe(callback *cb)
        {
            _data->callbacks.subscribe(cb);
        }
        void unsubscribe(callback *cb)
        {
            _data->callbacks.unsubscribe(cb);
        }
        
    private:
        void check() const;
    
    private:
        std::shared_ptr<listener_data> _data;
        class nothing {};
        std::shared_ptr<nothing> _shared_counter;
    };
}

#endif // REDHORN_LISTENER_HPP

