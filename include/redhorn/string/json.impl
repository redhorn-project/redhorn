#ifndef REDHORN_STRING_JSON_IMPL
#define REDHORN_STRING_JSON_IMPL

#include <string>
#include <iomanip>

#include "../builtin/json.hpp"

#include "json.decl"

namespace redhorn
{
    namespace _json
    {
        template <>
        struct serialize< std::string >
        {
        private:
            typedef std::string _type;

        public:
            static void write(std::ostream &os, const _type *value)
            {
                os << "\"";
                for (const char &ch : *value)
                {
                    if ((ch < 32) || (ch == 127)) //Control characters (C0 + delete)
                    {
                        os << "\\u" << std::setfill ('0') << std::setw(4) 
                           << std::hex << (short)ch << std::dec;
                    }
                    else
                    {
                        switch (ch)
                        {
                            case '\b': os << "\\b"; break;
                            case '\f': os << "\\f"; break;
                            case '\n': os << "\\n"; break;
                            case '\r': os << "\\r"; break;
                            case '\t': os << "\\t"; break;
                            case '\v': os << "\\v"; break;
                            case '\'': os << "'"; break;
                            case '"': os << "\\\""; break;
                            case '\\': os << "\\\\"; break;
                            default: os << ch; break;
                        }
                    }
                }
                os << "\"";
            }

            static void read(std::istream &is, _type *value)
            {
                utils::read_string(is, *value);
            }
        };
    };
};

#endif // REDHORN_STRING_JSON_IMPL
