#ifndef REDHORN_THREADING_HPP
#define REDHORN_THREADING_HPP

#include <thread>
#include <atomic>
#include <mutex>
#include <unordered_map>
#include <iostream>
#include <vector>

namespace redhorn
{
    namespace threading
    {
        void check();
        void signal(const std::thread::id &id);

        class stop_thread : public std::exception
        {
        public:
            stop_thread() {}
            virtual const char* what() const noexcept
            {
                return "stop_thread";
            }
        };
    }
}

#endif // REDHORN_THREADING_HPP

