#ifndef REDHORN_BUILTIN_DESCRIBE_IMPL
#define REDHORN_BUILTIN_DESCRIBE_IMPL

#include <sstream>

#include "dtype.hpp"
#include "../utils.hpp"

#include "describe.decl"

namespace redhorn
{
    namespace _describe 
    {
        template <typename _type>
        struct describe
        {
            static std::string str(const _type &value)
            {
                static_assert(std::is_arithmetic<_type>::value, "Only arichmetic types is allowed");
                std::stringstream ss;
                ss << value;
                return ss.str();
            };
        };

        template <>
        struct describe<char>
        {
        private:
            typedef char _type;

        public:
            static std::string str(const _type &value)
            {
                return utils::format_string("'%c'", value);
            };
        };

        template <>
        struct describe<bool>
        {
        private:
            typedef bool _type;

        public:
            static std::string str(const _type &value)
            {
                return value?"True":"False";
            };
        };

    };

    template <typename _type>
    std::string str(const _type &value)
    {
        return _describe::describe<_type>::str(value);
    }

    template <typename _type>
    std::string repr(const _type &value)
    {
        return utils::format_string("%s, dtype=%s",
                                    _describe::describe<_type>::str(value).c_str(),
                                    dtype<_type>::str.c_str());
    }


};
#endif // REDHORN_BUILTIN_DESCRIBE_IMPL
