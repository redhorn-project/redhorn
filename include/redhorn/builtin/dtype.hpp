#ifndef REDHORN_BUILTIN_DTYPE_HPP
#define REDHORN_BUILTIN_DTYPE_HPP

#include "dtype.decl"
#include "dtype.impl"

#endif // REDHORN_BUILTIN_DTYPE_HPP
