#ifndef REDHORN_BUILTIN_TYPE_NAME_HPP
#define REDHORN_BUILTIN_TYPE_NAME_HPP

namespace redhorn
{
    namespace types
    {
        template <typename _type>
        struct type_name
        {
            constexpr static const char *name = "void";
        };

        template <>
        struct type_name<int>
        {
            constexpr static const char *name = "int";
        };

        template <>
        struct type_name<long>
        {
            constexpr static const char *name = "int";
        };

        template <>
        struct type_name<long long>
        {
            constexpr static const char *name = "int";
        };

        template <>
        struct type_name<unsigned int>
        {
            constexpr static const char *name = "uint";
        };

        template <>
        struct type_name<unsigned long>
        {
            constexpr static const char *name = "uint";
        };

        template <>
        struct type_name<unsigned long long>
        {
            constexpr static const char *name = "uint";
        };

        template <>
        struct type_name<float>
        {
            constexpr static const char *name = "float";
        };

        template <>
        struct type_name<double>
        {
            constexpr static const char *name = "float";
        };

        template <>
        struct type_name<char>
        {
            constexpr static const char *name = "char";
        };

        template <>
        struct type_name<bool>
        {
            constexpr static const char *name = "bool";
        };

    };

    using types::type_name;
};

#endif // REDHORN_BUILTIN_TYPE_NAME_HPP

