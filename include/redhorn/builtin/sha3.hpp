#ifndef REDHORN_BUILTIN_SHA3_HPP
#define REDHORN_BUILTIN_SHA3_HPP

#include "sha3.decl"
#include "sha3.impl"

#endif // REDHORN_BUILTIN_SHA3_HPP
