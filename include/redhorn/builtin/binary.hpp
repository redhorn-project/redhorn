#ifndef REDHORN_BUILTIN_BINARY_HPP
#define REDHORN_BUILTIN_BINARY_HPP

#include "binary.decl"
#include "binary.impl"

#endif // REDHORN_BUILTIN_BINARY_HPP
