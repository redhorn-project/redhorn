#ifndef REDHORN_CONFIG_PARSER_HPP
#define REDHORN_CONFIG_PARSER_HPP

#include <string>
#include <memory>
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>
#include <algorithm>

#include <argp.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/types.h>
#include <pwd.h>

namespace redhorn
{
    template <class T>
    class option_helper;
    
    class option_base
    {
        friend class config_parser;
        template <class T>
        friend class option_filler;

    private:
        virtual void set_value(char *value) = 0;
        virtual void fill_argp_option(::argp_option &option) = 0;
    public:
        option_base()
            : _set(false)
        {
        }
        virtual ~option_base()
        {
        }

        
        
    protected:
        std::string _long_option;
        char _short_option;
        bool _required;
        bool _set;
        std::string _doc_string;
        std::string _arg_string;
    };

    template <class T>
    class option_converter
    {
    public:
        static void convert(T& dst, char *src)
        {
            std::stringstream ss;
            ss << src;
            ss >> dst;
            if (!ss.eof())
                throw std::invalid_argument("convertion failed");
        }
    };

    template <class T>
    class option_filler
    {
    public:
        static void fill(::argp_option &option, option_base *opt)
        {
        option.name = opt->_long_option.c_str();
        option.key = opt->_short_option;
        if (opt->_arg_string.empty())
            option.arg = "ARG";
        else
            option.arg = opt->_arg_string.c_str();
        option.flags = 0;
        if (opt->_doc_string.empty())
            option.doc = "No description";
        else
            option.doc = opt->_doc_string.c_str();
        option.group = 0;
        }
    };

    template <>
    class option_converter<bool>
    {
    public:
        static void convert(bool& dst, char *src)
        {
            if (!src)
                dst = true;
            else
            {
                std::string value = src;
                if (value == "yes"
                    || value == "true"
                    || value == "1")
                    dst=true;
                else if (value == "no"
                         || value == "false"
                         || value == "0")
                    dst=false;
                else
                    throw std::invalid_argument("convertion failed");
            }
        }
    };

    template <>
    class option_filler<bool>
    {
    public:
        static void fill(::argp_option &option, option_base *opt)
        {
            option.name = opt->_long_option.c_str();
            option.key = opt->_short_option;
            option.arg = "yes|no|true|false|0|1";
            option.flags = OPTION_ARG_OPTIONAL;
            if (opt->_doc_string.empty())
                option.doc = "No description";
            else
                option.doc = opt->_doc_string.c_str();
            option.group = 0;
        }
    };


    template <>
    class option_converter<std::string>
    {
    public:
        static void convert(std::string& dst, char *src)
        {
            dst = src;
        }
    };

    
    template <class T>
    class option : public option_base
    {
    public:
        option(T &value)
            : option_base() 
            , _value(value)
        {
        }

    
    private:
        virtual void set_value(char *value) override
        {
            option_converter<T>::convert(_value, value);
            _set = true;
        }

        virtual void fill_argp_option(::argp_option &option) override
        {
            option_filler<T>::fill(option, this);
        }

    public:    
        option<T>& default_value(const T &value)
        {
            _value = value;
            _set = true;
            return *this;
        }

        option<T>& long_option(std::string name)
        {
            _long_option = std::move(name);
            return *this;
        }

        option<T>& short_option(char name)
        {
            _short_option = name;
            return *this;
        }


        option<T>& required(bool required = true)
        {
            _required = required;
            return *this;
        }

        option<T>& doc(std::string doc)
        {
            _doc_string = std::move(doc);
            return *this;
        }

        option<T>& arg(std::string arg)
        {
            _arg_string = std::move(arg);
            return *this;
        }

    private:
        T &_value;
    };


    class config_parser
    {
    public:
        config_parser() = default;

        template <class T>
        option<T>& add_option(T &value)
        {
            _options.emplace_back(std::make_unique<option<T>>(value));
            return *reinterpret_cast<option<T>*>((_options.back().get()));
        }

        void parse(int argc, char* argv[], const std::vector<std::string> &filenames);

    private:
        static error_t parse_opt (int key, char *arg, struct argp_state *state);
        void parse_options(int argc, char *argv[]);

        static std::string get_home_dir();
        static void parse_ini_line(std::vector<std::string> &tokens,
                                   const std::string &line,
                                   const char delimeter);
        void parse_ini(std::string fname);

    private:
        std::vector<std::unique_ptr<option_base>> _options;
    };


 
}
#endif // REDHORN_CONFIG_PARSER_HPP

