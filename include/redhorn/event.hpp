#ifndef REDHORN_EVENT_HPP
#define REDHORN_EVENT_HPP

#include <condition_variable>

#include <redhorn/threading.hpp>
#include <redhorn/errors.hpp>

namespace redhorn
{
    class event
    {
    public:
        event()
            : _flag(false)
        {
        }

        event(const event &) = delete;
        event& operator=(const event &) = delete;

        event(event &&) = delete;
        event& operator=(event &&) = delete;

        void wait()
        {
            std::unique_lock<std::mutex> lock(_mutex);
            _cv.wait(lock, [this]{
                    redhorn::threading::check(); 
                    return _flag;
                });
            _flag = false;
        }

        void wait(const std::chrono::milliseconds &timeout)
        {
            std::unique_lock<std::mutex> lock(_mutex);
            bool ret = _cv.wait_for(lock, 
                                    timeout,
                                    [this]{
                                        redhorn::threading::check(); 
                                        return _flag;
                                    });
            if (ret == false)
                throw timeout_error("blocking wait failed");
            _flag = false;
        }
        
        void notify()
        {
            {
                std::lock_guard<std::mutex> lock(_mutex);
                _flag = true;
            }
            _cv.notify_one();
        }

        void signal()
        {
            _cv.notify_all();
        }

        bool ready()
        {
            std::lock_guard<std::mutex> lock(_mutex);
            return _flag;
        }

        void reset()
        {
            std::lock_guard<std::mutex> lock(_mutex);
            _flag = false;
        }

    private:
        std::condition_variable _cv;
        std::mutex _mutex;
        bool _flag;
    };
}

#endif // REDHORN_EVENT_HPP
