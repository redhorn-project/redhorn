#ifndef REDHORN_MAP_JSON_IMPL
#define REDHORN_MAP_JSON_IMPL

#include <map>
#include <unordered_map>

#include "../builtin/json.hpp"

#include "json.decl"

namespace redhorn
{
    namespace _json
    {
        template <typename _map>
        struct map_serialize
        {
        public:
            static void write(std::ostream &os, const _map *value)
            {
                os << "[";
                auto iter = value->begin();
                while (iter != value->end())
                {
                    if (iter != value->begin())
                        os << ",";
                    os << "[";
                    serialize<typename _map::key_type>::write(os, &(iter->first));
                    os << ",";
                    serialize<typename _map::mapped_type>::write(os, &(iter->second));
                    os << "]";
                    ++iter;
                }
                os << "]";
            }

            static std::pair<typename _map::key_type, typename _map::mapped_type> read_item(std::istream &is)
            {
                std::pair<typename _map::key_type, typename _map::mapped_type> buff;
                utils::read_control(is, "[");
                serialize<typename _map::key_type>::read(is, &(buff.first));
                utils::read_control(is, ",");
                serialize<typename _map::mapped_type>::read(is, &(buff.second));
                utils::read_control(is, "]");
                return buff;
            }

            static void read(std::istream &is, _map *value)
            {
                utils::read_control(is, "[");
                utils::strip(is);
                auto inserter = std::inserter(*value, value->end());
                while (is.peek() != ']')
                {
                    inserter = read_item(is);
                    utils::strip(is);
                    if (is.peek() != ']')
                    {
                        utils::read_control(is, ",");
                        utils::strip(is);
                    }
                }
                utils::read_control(is, "]");
            }

        };
        

        template <typename _key, typename _compare,  typename _Alloc>
        struct serialize< std::map<_key, _compare, _Alloc> >
        {
        private:
            typedef std::map<_key, _compare, _Alloc> _type;

        public:
            static void write(std::ostream &os, const _type *value)
            {
                map_serialize<_type>::write(os, value);
            }
            static void read(std::istream &is, _type *value)
            {
                map_serialize<_type>::read(is, value);
            }

        };
        
        template <typename _key, typename _value, typename _compare,  typename _Alloc>
        struct serialize< std::multimap<_key, _value, _compare, _Alloc> >
        {
        private:
            typedef std::multimap<_key, _value, _compare, _Alloc> _type;

        public:
            static void write(std::ostream &os, const _type *value)
            {
                map_serialize<_type>::write(os, value);
            }
            static void read(std::istream &is, _type *value)
            {
                map_serialize<_type>::read(is, value);
            }

        };
        
        template <typename _key, typename _value, typename _hash, typename _key_equal,  typename _Alloc>
        struct serialize< std::unordered_map<_key, _value, _hash, _key_equal, _Alloc> >
        {
        private:
            typedef std::unordered_map<_key, _value, _hash, _key_equal, _Alloc> _type;

        public:
            static void write(std::ostream &os, const _type *value)
            {
                map_serialize<_type>::write(os, value);
            }
            static void read(std::istream &is, _type *value)
            {
                map_serialize<_type>::read(is, value);
            }

        };
        
        template <typename _key, typename _value, typename _hash, typename _key_equal,  typename _Alloc>
        struct serialize< std::unordered_multimap<_key, _value, _hash, _key_equal, _Alloc> >
        {
        private:
            typedef std::unordered_multimap<_key, _value, _hash, _key_equal, _Alloc> _type;

        public:
            static void write(std::ostream &os, const _type *value)
            {
                map_serialize<_type>::write(os, value);
            }
            static void read(std::istream &is, _type *value)
            {
                map_serialize<_type>::read(is, value);
            }

        };
    };
};

#endif // REDHORN_MAP_JSON_IMPL
