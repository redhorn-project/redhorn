#include <redhorn/heart_beat.hpp>

using namespace redhorn;

heart_beat::heart_beat(redhorn::logger log,
                       std::chrono::milliseconds ping_rate,
                       std::chrono::milliseconds pong_rate)
    : _log(std::move(log))
    , _state(state::pending)
    , _ping_rate(ping_rate)
    , _pong_rate(pong_rate)
{
}

void heart_beat::start()
{
    if (_state != state::pending )
        throw std::logic_error("iopool is not pending");
    
    _ping_thread = std::thread([this]
                               {
                                   redhorn::logging::name_this_thread("ping");
                                   this->ping_loop();
                               });
    _pong_thread = std::thread([this]
                               {
                                   redhorn::logging::name_this_thread("pong");
                                   this->pong_loop();
                               });

    _state = state::running;
}

void heart_beat::stop()
{
    if (_state != state::running )
        throw std::logic_error("heart_beat is not running");
    redhorn::threading::signal(_ping_thread.get_id());
    redhorn::threading::signal(_pong_thread.get_id()); 
    _ping_cv.notify_all();
    _pong_cv.notify_all();
    _state = state::stopping;
}

void heart_beat::wait()
{
    if (_state != state::stopping && _state != state::running )
        throw std::logic_error("heart_beat is not running");
    _ping_thread.join();
    _pong_thread.join();
    _state = state::joined;
}

std::unordered_map<int, std::shared_ptr<heart_beat::info_base>> heart_beat::get_infos()
{
    std::lock_guard<std::mutex> guard(_map_mutex);
    return _info_map;
}

void heart_beat::add_info(int fd, std::shared_ptr<heart_beat::info_base> info)
{
    std::lock_guard<std::mutex> guard(_map_mutex);
    _info_map.emplace(fd, std::move(info));
}

std::shared_ptr<heart_beat::info_base> heart_beat::remove_info(int fd)
{
    std::lock_guard<std::mutex> guard(_map_mutex);
    std::shared_ptr<info_base> info = std::move(_info_map[fd]);
    _info_map.erase(fd);
    return info;
}


void heart_beat::pong_loop()
{
    if (_pong_rate == std::chrono::milliseconds::zero())
    {
        _log.info("disabled");
        return;
    }
    
    try
    {
        _log.info("started");
        for (;;)
        {
            redhorn::threading::check();
            auto alarm = std::chrono::steady_clock::now() + _pong_rate;
            try
            {
                for (auto &it : get_infos())
                {
                    it.second->check_timestamp();
                }
            }
            catch (redhorn::threading::stop_thread&)
            {
                throw;
            }
            catch (...)
            {
                _log.exception("pong failed");
            }
            {
                std::unique_lock<std::mutex> guard(_pong_mutex);
                while (_pong_cv.wait_until(guard, alarm) == std::cv_status::no_timeout)
                    redhorn::threading::check();
            }
        }
    }
    catch (redhorn::threading::stop_thread&)
    {
        _log.info("caught stop signal");
    }
    _log.info("exited");
            
}

void heart_beat::ping_loop()
{
    if (_ping_rate == std::chrono::milliseconds::zero())
    {
        _log.info("disabled");
        return;
    }
    
    try
    {
        _log.info("started");
        for (;;)
        {
            redhorn::threading::check();
            auto alarm = std::chrono::steady_clock::now() + _ping_rate;
            try
            {
                for (auto &it : get_infos())
                {
                    it.second->send_ping();
                }
            }
            catch (redhorn::threading::stop_thread&)
            {
                throw;
            }
            catch (...)
            {
                _log.exception("ping failed");
            }
            {
                std::unique_lock<std::mutex> guard(_ping_mutex);
                while (_ping_cv.wait_until(guard, alarm) == std::cv_status::no_timeout)
                    redhorn::threading::check();
            }
        }
    }
    catch (redhorn::threading::stop_thread&)
    {
        _log.info("caught stop signal");
    }
    _log.info("exited");
            
}

