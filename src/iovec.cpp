#include <stdexcept>

#include <redhorn/iovec.hpp>

void redhorn::iovec::push_deferred(deferred_t def)
{
    if (_chunk_list.empty() || _chunk_list.back().deferred)
    {
        _chunk_list.emplace_back();
    }
    _chunk_list.back().deferred = def;
}
        
redhorn::iovec::iov redhorn::iovec::top()
{
    eval_deferred();
    if (!empty())
    {
        iovec_t& ch = _chunk_list.front().iovec;
        return iov(&(ch[offset]), ch.size() - offset);
    }
    else
        throw std::runtime_error("iovec is empty");
}


bool redhorn::iovec::empty()
{
    eval_deferred();
    return _chunk_list.empty();
}
            

void redhorn::iovec::pop(std::size_t size)
{
    eval_deferred();
    if (!empty())
    {
        for (auto &it : _chunk_list.front().iovec)
        {
            if (it.iov_len <= size)
            {
                size -= it.iov_len;
                ++offset;
            }
            else
            {
                it.iov_len -= size;
                it.iov_base = (char*)it.iov_base + size;
                size = 0;
            }
        }
    }
    if (size != 0)
        throw std::runtime_error("Argument for iovec::pop is too big");
    eval_deferred();
}

void redhorn::iovec::eval_deferred()
{
    if (_chunk_list.empty())
        return;

    chunk &front = _chunk_list.front();
    if (front.iovec.size() == offset)
    {        
        if (front.deferred)
        {
            iovec iov;
            front.deferred(iov);
            _chunk_list.pop_front();
            offset = 0;
            _chunk_list.splice(_chunk_list.begin(), iov._chunk_list);
        }
        else
        {
            _chunk_list.pop_front();
            offset = 0;
        }
        eval_deferred();
    }
}

