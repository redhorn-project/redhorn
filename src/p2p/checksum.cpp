#include <cryptopp/cryptlib.h>
#include <cryptopp/sha3.h>
#include <cryptopp/hex.h>

#include <redhorn/p2p/checksum.hpp>
#include <redhorn/logging.hpp>

static void update_hash(CryptoPP::SHA3_256 &hash, const redhorn::p2p::id::peer &pid, const redhorn::p2p::route &route)
{
    hash.Update(pid.bytes(), redhorn::p2p::id::peer::size);
    for (auto &rp : route)
    {
        hash.Update(rp.rid.bytes(), redhorn::p2p::id::router::size);
        for (auto &endpoint : rp.endpoints)
        {
            hash.Update(reinterpret_cast<const unsigned char*>(endpoint.hostname.c_str()), endpoint.hostname.size() * sizeof(char));
            hash.Update(reinterpret_cast<const unsigned char*>(&(endpoint.port)), sizeof(endpoint.port));
        }
    }
}

redhorn::p2p::checksum redhorn::p2p::router_checksum(const std::map<id::peer, redhorn::p2p::route> &routes,
                                                     const std::unordered_map<id::peer, redhorn::rsa::certificate> &certificates)
{
    CryptoPP::SHA3_256 hash;
    for (auto &s : routes)
    {
        const id::peer &pid = s.first;
        const redhorn::p2p::route &route = s.second;
        if (certificates.find(pid) != certificates.end())
        {
            update_hash(hash, pid, route);
        }
    }
    redhorn::p2p::checksum checksum;
    hash.Final(*checksum);
    return checksum;
}


redhorn::p2p::checksum redhorn::p2p::router_checksum(const std::map<id::peer, redhorn::p2p::route> &routes)
{
    CryptoPP::SHA3_256 hash;
    for (auto &s : routes)
    {
        update_hash(hash, s.first, s.second);
    }
    redhorn::p2p::checksum checksum;
    hash.Final(*checksum);
    return checksum;
}

