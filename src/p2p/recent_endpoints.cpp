#include <redhorn/p2p/recent_endpoints.hpp>


void redhorn::p2p::recent_endpoints::cache(endpoints endpoints)
{
    std::lock_guard<std::mutex> guard(_bootstrap_lock);
    _bootstrap.emplace_front(std::move(endpoints));
    if (_bootstrap.size() > 10)
        _bootstrap.pop_back();
}

void redhorn::p2p::recent_endpoints::cache(const endpoints &endpoints,
                                    const id::peer &anchor)
{
    cache(endpoints);
    {
        std::lock_guard<std::mutex> guard(_anchors_lock);
        _anchors[anchor] = endpoints;
    }                
}

void redhorn::p2p::recent_endpoints::remove(const id::peer &anchor)
{
    std::lock_guard<std::mutex> guard(_anchors_lock);
    _anchors.erase(anchor);
}        

redhorn::p2p::endpoints redhorn::p2p::recent_endpoints::find(const id::peer &anchor) const
{
    std::lock_guard<std::mutex> guard(_anchors_lock);
    auto iter = _anchors.find(anchor);
    if (iter == _anchors.end())
        return redhorn::p2p::endpoints();
    return iter->second;
}

std::list<redhorn::p2p::endpoints> redhorn::p2p::recent_endpoints::bootstrap() const
{
    std::lock_guard<std::mutex> guard(_bootstrap_lock);
    return _bootstrap;
}
        
