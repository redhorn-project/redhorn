#include <redhorn/logging.hpp>

#include <execinfo.h>

using namespace redhorn;

void logging::name_this_thread(std::string name)
{
    _logging::thread_name() = std::move(name);
}

void logging::init()
{
    logging::name_this_thread("main");
    std::set_terminate(_logging::terminate);
    openlog(NULL, LOG_NDELAY | LOG_PERROR | LOG_PID, LOG_USER);
}

void logging::init(std::string name)
{
    static std::string _name = std::move(name);
    logging::name_this_thread("main");
    std::set_terminate(_logging::terminate);
    openlog(_name.c_str(), LOG_NDELAY | LOG_PERROR | LOG_PID, LOG_USER);
}

void logging::init(std::string name, logging::config config)
{
    logging::init(std::move(name));
    _logging::config() = config;
}

logger::logger(std::string name, level threshold)
    : _name(std::move(name))
    , _threshold(threshold)
    , debug(_name, _threshold)
    , info(_name, _threshold)
    , notice(_name, _threshold)
    , warning(_name, _threshold)
    , error(_name, _threshold)
    , critical(_name, _threshold)
    , alert(_name, _threshold)
    , emergency(_name, _threshold)
    , exception(_name, _threshold)
{
}

logger::logger()
    : logger("", _logging::threshold(""))
{
}

logger::logger(std::string name)
    : logger(std::move(name), _logging::threshold(name))
{
}

logger::logger(const logger &other)
    : logger(other._name, other._threshold)
{
}

logger& logger::operator=(const logger &other)
{
    _name = other._name;
    _threshold = other._threshold;
    return *this;
}

logger::logger(logger &&other)
    : logger(std::move(other._name), other._threshold)
{
}

logger& logger::operator=(logger &&other)
{
    _name = std::move(other._name);
    _threshold = other._threshold;
    return *this;
}

logger logger::child(const std::string &child_name)
{
    if (!_name.empty())
        return logger(_name + "." + child_name);
    else
        return logger(child_name);
}


static std::string _thread_id_as_string(std::thread::id id)
{
    std::stringstream ss;
    ss << id;
    return ss.str();
}

std::string& _logging::thread_name()
{
    static thread_local std::string name = _thread_id_as_string(std::this_thread::get_id());
    return name;
}

logging::config& _logging::config()
{
    static logging::config config;
    return config;
}

logging::level _logging::threshold(std::string name)
{
    logging::config &config = _logging::config();
    if (config.empty())
        return logging::level::error;

    while (!name.empty())
    {
        auto iter = config.find(name);
        if (iter != config.end())
            return iter->second;

        std::string::size_type dot_pos = name.rfind(".");
        if (dot_pos != std::string::npos)
            name.resize(dot_pos);
        else
            name.clear();
    }

    auto iter = config.find("");
    if (iter != config.end())
        return iter->second;

    return logging::level::error;
}
 
void _logging::terminate()
{
    void *array[50];
    size_t size;

    size = backtrace(array, 50);
    char ** symbols = backtrace_symbols(array, size);
    logger log("");
    std::string backtrace_str;
    if (size > 0)
    {
        backtrace_str = symbols[0];
        for (size_t i = 1; i < size; ++i)
        {
            backtrace_str += '\n';
            backtrace_str += symbols[i];
        }
    }

    auto eptr = std::current_exception();
    if (eptr)
    {
        try
        {
            std::rethrow_exception(eptr);
        } 
        catch(const std::exception& e) 
        {
            log.critical("Unhandled exception of type %s" \
                         "\nError: %s" \
                         "\nBacktrace:\n%s", 
                         typeid(e).name(), e.what(), backtrace_str.c_str());

        }
        catch(...)
        {
            log.critical("Unhandled exception of unknown type" \
                         "\nBacktrace:\n%s", 
                         backtrace_str.c_str());
        }
    }
    else
    { 
        log.critical("std::terminate is called without active exception" \
                     "\nBacktrace:\n%s", backtrace_str.c_str());
    }
        
    abort();
}
    

const char* _logging::level_to_str(const logging::level &enum_level)
{
    static const char* level_str[] = {
        "Emergency",
        "Alert",
        "Critical",
        "Error",
        "Warning",
        "Notice",
        "Info",
        "Debug"
    };
    return level_str[int(enum_level)];
}
