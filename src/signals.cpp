
#include <redhorn/signals.hpp>
#include <redhorn/threading.hpp>

using namespace redhorn;

void signals::handlers::set_event(int sig, redhorn::signals &signals)
{
    if (sig == SIGINT)
    {
        signals.sigint.notify();
    }
    else
        signals._log.warning(std::string("caught unexpected signal, sig: ") + std::to_string(sig));
}

void signals::handlers::terminate(int)
{
    std::terminate();
}

signals::signals()
    : _log("signals")
{
    _sig_set = block({SIGINT, SIGPIPE});
    set_handler(SIGINT, handlers::set_event);
}


void signals::set_handler(int sig, std::function<void(int)> handler)
{
    std::lock_guard<std::mutex> lock(_handlers_mutex);
    _handlers[sig] = std::move(handler);
}

void signals::set_handler(int sig, const std::function<void(int, signals&)> &handler)
{
    set_handler(sig,
                [this, handler](int sig)
                {
                    handler(sig, *this);
                });
}


void signals::set_handler(int sig, const std::function<void()> &handler)
{
    set_handler(sig,
                [handler](int)
                {
                    handler();
                });
}
        
void signals::start()
{
    _sig_thread = std::thread([this]
                              {
                                  redhorn::logging::name_this_thread("signal");
                                  sig_loop();
                              });
}

void signals::stop()
{
    redhorn::threading::signal(_sig_thread.get_id());
    ::kill(getpid(), SIGINT);
}

void signals::wait()
{
    _sig_thread.join();
}

::sigset_t signals::block(const std::vector<int> &sigs)
{
    sigset_t sig_set;
    sigemptyset(&sig_set);
    for (auto &sig : sigs)
        sigaddset(&sig_set, sig);
    if (::pthread_sigmask(SIG_BLOCK, &sig_set, NULL) != 0)
        throw std::runtime_error(std::string("pthread_sigmask failed error: ") + std::strerror(errno));
    return sig_set;
}


void signals::sig_loop()
{
    _log.info("started");
    try
    {
        for (;;) 
        {
            int sig, err;
            if ((err = ::sigwait(&_sig_set, &sig)) != 0)
                throw std::logic_error(std::string("sigwait failed, error: ") + std::strerror(err));

            redhorn::threading::check();
            try
            {
                std::function<void(int)> handler;
                {
                    std::lock_guard<std::mutex> lock(_handlers_mutex);
                    handler = _handlers.at(sig);
                }
                handler(sig);
            }
            catch (redhorn::threading::stop_thread&)
            {
                throw;
            }
            catch (const std::exception &e)
            {
                _log.error("Handler failed, error: %s", e.what());
            }
            catch (...)
            {
                _log.error("Something really bad happend");
            }
                
        }
    }
    catch (redhorn::threading::stop_thread&)
    {
        _log.info("caught stop signal");
    }
    _log.info("exited");
            
}
