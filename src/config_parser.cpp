#include <redhorn/config_parser.hpp>

using namespace redhorn;

void config_parser::parse(int argc, char* argv[], const std::vector<std::string> &filenames)
{
    for (auto &filename : filenames)
        parse_ini(filename);
    parse_options(argc, argv);
    for (size_t i = 0; i < _options.size(); ++i)
        if (!_options[i]->_set && _options[i]->_required)
            throw std::invalid_argument("Required option '" + _options[i]->_long_option + "' not set");
    
}

error_t config_parser::parse_opt(int key, char *arg, struct argp_state *state)
{
    try
    {
        config_parser &parser = *(reinterpret_cast<redhorn::config_parser*>(state->input));
        if (key == ARGP_KEY_ARG)
            return ARGP_ERR_UNKNOWN;
        else if (key == ARGP_KEY_END)
        {
            if (state->arg_num != 0)
                return ARGP_ERR_UNKNOWN;
        }
        else
        {
            for (auto & opt : parser._options)
            {
                if (opt->_short_option == key)
                {
                    opt->set_value(arg);
                    return 0;
                }
            }
        }
    }
    catch (...)
    {
        return ARGP_ERR_UNKNOWN;
    }
    return ARGP_ERR_UNKNOWN;
}


void config_parser::parse_options(int argc, char *argv[])
{
    std::vector<argp_option> options(_options.size() + 1);
    for (size_t i = 0; i < _options.size(); ++i)
        _options[i]->fill_argp_option(options[i]);
    options[_options.size()].name = NULL;

    struct argp argp = { options.data(), parse_opt, NULL, NULL, NULL, NULL, NULL };

    argp_parse (&argp, argc, argv, 0, 0, this);
        
}


std::string config_parser::get_home_dir()
{
    if (std::getenv("HOME"))
        return std::getenv("HOME");
    struct passwd *pw = ::getpwuid(::getuid());
    return pw->pw_dir;    
}


void config_parser::parse_ini_line(std::vector<std::string> &tokens,
                                   const std::string &line,
                                   const char delimeter)
{
    tokens.clear();
    std::stringstream sline(line);
    std::string item;
    while(std::getline(sline, item, delimeter))
    {
        tokens.push_back(item);
    }
}
        
void config_parser::parse_ini(std::string fname)
{
    if (fname.find("~/") == 0)
        fname = get_home_dir() + fname.substr(1);

    std::ifstream file(fname);

    if (file.good()) 
    {
        std::string line;
        while(std::getline(file, line))
        {
            if ((line[0] == '#') || (line[0] == '['))
                continue;

            std::vector<std::string> tokens;
            parse_ini_line(tokens, line, '=');
            if (tokens.size() != 2)
            {
                continue;
            }
            std::string &key = tokens[0];
            key.erase(std::remove_if(key.begin(), key.end(), ::isspace), key.end());

            std::string &value = tokens[1];
            int i = -1;
            while(value[++i] == ' ');

            for (auto &opt : _options)
            {
                if (opt->_long_option == key)
                {
                    opt->set_value((char*)value.c_str());
                    break;
                }
            }
        }
    } 
}
