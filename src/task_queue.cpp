#include <functional>

#include <redhorn/queue.decl>
#include <redhorn/queue.impl>

template class redhorn::queue<std::function<void()>>;
