#include <redhorn/threading.hpp>

#include <sstream>

using namespace redhorn;

namespace _threading
{
    class thread_statuses
    {
    private:
        friend void threading::signal(const std::thread::id &id);

        void stop_thread(std::thread::id thread_id)
        {
            std::lock_guard<std::mutex> guard(_mutex);
            if (_flags.count(thread_id) == 0)
                _flags[thread_id] = std::make_unique<std::atomic<bool>>(true);
            _flags.at(thread_id)->store(true);
        }
        static thread_statuses& instance()
        {
            static thread_statuses ts;
            return ts;
        }

        friend class thread_guard;

        std::atomic<bool>* register_thread()
        {
            std::lock_guard<std::mutex> guard(_mutex);
            if (_flags.count(std::this_thread::get_id()) == 0)
                _flags[std::this_thread::get_id()] = std::make_unique<std::atomic<bool>>(false);
            return _flags.at(std::this_thread::get_id()).get();
        }

        void unregister_thread()
        {
            std::lock_guard<std::mutex> guard(_mutex);
            if (_flags.count(std::this_thread::get_id()) > 0)
                _flags.erase(std::this_thread::get_id());
        }

    private:
        std::unordered_map<std::thread::id, std::unique_ptr<std::atomic<bool>>> _flags;
        std::mutex _mutex;
    };

    class thread_guard
    {
    private:
        friend void threading::check();
        friend void threading::signal(const std::thread::id &id);
        bool check()
        {
            return _flag->load();
        }

        thread_guard()
        {
            _flag = thread_statuses::instance().register_thread();
        }

        ~thread_guard()
        {
            thread_statuses::instance().unregister_thread();
        }

    private:
        std::atomic<bool> *_flag;
    };
}


void threading::check()
{
    thread_local _threading::thread_guard _guard;
    if (_guard.check())
    {
        throw stop_thread();
    }
}

void threading::signal(const std::thread::id &id)
{
    _threading::thread_statuses::instance().stop_thread(id);
}

