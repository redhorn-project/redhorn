#include <cstring>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <poll.h>
#include <fcntl.h>

#include <redhorn/errors.hpp>
#include <redhorn/listener.hpp>

using namespace redhorn;



listener::listener_data::listener_data()
    : fd(0)
    , valid(false)
    , log("listener.null")
    , callbacks(log.child("callbacks"))
{
}

listener::listener_data::listener_data(unsigned int port)
    : fd(0)
    , valid(false)
    , log(std::string("listener.")+std::to_string(port))
    , callbacks(log.child("callbacks"))
{
    log.info("creating");
    struct sockaddr_in serv_addr;

    fd.store(::socket(AF_INET, SOCK_STREAM, 0));
    if (fd.load() < 0) 
        throw redhorn::socket_create_error(std::strerror(errno));

    int option = 1;
    if (::setsockopt(fd.load(), SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option)) < 0)
        throw setsockopt_error(std::strerror(errno));

    std::memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(port);
 
    if (::bind(fd.load(), (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        throw bind_error(std::strerror(errno));

    ::listen(fd.load(), 20);
    ::fcntl(fd.load(), F_SETFL, O_NONBLOCK);
    valid = true;
}



listener::listener()
    : _data(std::make_shared<listener_data>())
    , _shared_counter(nullptr)
{
};

listener::listener(unsigned int port)
    : _data(std::make_shared<listener_data>(port))
    , _shared_counter(std::make_shared<nothing>())
{
};

listener::listener(listener && other)
    : _data(std::move(other._data))
    , _shared_counter(std::move(other._shared_counter))
{
    other._data = std::make_shared<listener_data>();
    other._shared_counter.reset();
}
        
listener& listener::operator=(listener &&other)
{
    _data = std::move(other._data);
    _shared_counter = std::move(other._shared_counter);
    other._data = std::make_shared<listener_data>();
    other._shared_counter.reset();
    return *this;
}


listener::~listener()
{
    if (_data->valid.load())
    {
        bool last_one = false;
        {
            std::lock_guard<std::mutex> guard(_data->dtor_mutex);
            last_one = _shared_counter.unique();
            _shared_counter.reset();
        }
        if (last_one)
            close();
    }
}

void listener::close()
{
    if (_data->valid.exchange(false))
    {
        int fd = _data->fd.load();
        ::shutdown(fd, SHUT_RDWR);
        _data->callbacks.call([this](callback& cb){cb.shutdown(*this);});

        ::close(fd);
        _data->callbacks.call([this](callback& cb){cb.close(*this);});
    }
}

int listener::handler() const
{
    return _data->fd.load();
}

std::size_t listener::id() const
{
    return reinterpret_cast<std::size_t>(_data.get());
}

int listener::_non_blocking_accept() const
{
    int sock_fd = ::accept(_data->fd.load(), NULL, NULL);
    if (sock_fd < 0)
    {
        if ((errno == EAGAIN) || (errno == EWOULDBLOCK))
            return -1;
        else
            throw accept_error(std::strerror(errno));
    }
    return sock_fd;
}

bool listener::_poll(bool block, 
                     std::chrono::milliseconds timeout) const
{
    check();

    pollfd fds[1] = {{_data->fd.load(), POLLIN, 0}};
    int err;
    if (block && timeout == std::chrono::milliseconds::zero())
        err = ::poll(fds, 1, -1);
    else if (block)
        err = ::poll(fds, 1, timeout.count());
    else
        err = ::poll(fds, 1, 0);

    if (err == 0)
        return false;
    else if (err < 0)
        throw std::runtime_error(strerror(errno));
    else
    {
        if (fds[0].revents & POLLIN)
            return true;
        else
            throw std::runtime_error("accept socket poll: unexpected revents");
    }
}

int listener::_accept(bool block, 
                     std::chrono::milliseconds timeout) const
{
    check();
    if (block)
    {
        int fd = _non_blocking_accept();
        while (fd < 0)
        {
            if (_poll(block, timeout))
                fd = _non_blocking_accept();
            else
                throw timeout_error("blocking accept failed");
        }
        return fd;
    }
    else
    {
        return _non_blocking_accept();
    }
}

void listener::check() const
{
    if (!_data->valid.load())
        throw invalid_socket("invalid listener");
}
