#include <redhorn/rsa.hpp>

#include <redhorn/string/sha3.hpp>


redhorn::rsa::certificate::certificate(const redhorn::rsa::key &key)
{
    std::lock_guard<std::mutex> lock(key._mutex);
    _pub_key = CryptoPP::RSA::PublicKey(key._priv_key);
}

redhorn::rsa::certificate::certificate(const std::string &cert)
{
    CryptoPP::HexDecoder decoder;
    decoder.Put(reinterpret_cast<const unsigned char*>(cert.c_str()), cert.size() );
    decoder.MessageEnd();

    _pub_key.BERDecode(decoder);
    
    CryptoPP::AutoSeededRandomPool prng;    
    if (!_pub_key.Validate(prng, 3))
        throw std::invalid_argument("Can't verify certificate");
}

redhorn::rsa::certificate::certificate(const certificate &other)
{
    std::lock_guard<std::mutex> lock(other._mutex);
    _pub_key = other._pub_key;
}

redhorn::rsa::certificate& redhorn::rsa::certificate::operator=(const certificate &other)
{
    std::lock_guard<std::mutex> lock(other._mutex);
    _pub_key = other._pub_key;
    return *this;
}


redhorn::rsa::certificate::operator bool() const
{
    std::lock_guard<std::mutex> lock(_mutex);
    CryptoPP::AutoSeededRandomPool prng;    
    return _pub_key.Validate(prng, 3);
}

std::string redhorn::rsa::certificate::str() const
{
    std::lock_guard<std::mutex> lock(_mutex);
    std::string res;
    CryptoPP::HexEncoder sink(new CryptoPP::StringSink(res), false);
    _pub_key.DEREncode(sink);
    return res;
}

redhorn::sha3::digest redhorn::rsa::certificate::fingerprint() const
{
    return redhorn::sha3::hash(this->str());
}


redhorn::rsa::key::key(const std::string &key)
{
    CryptoPP::HexDecoder decoder;
    decoder.Put(reinterpret_cast<const unsigned char*>(key.c_str()), key.size() );
    decoder.MessageEnd();

    _priv_key.BERDecode(decoder);
    CryptoPP::AutoSeededRandomPool prng;    
    if (!_priv_key.Validate(prng, 3))
        throw std::invalid_argument("Can't verify key");
}

redhorn::rsa::key::key(const key &other)
{
    std::lock_guard<std::mutex> lock(other._mutex);
    _priv_key = other._priv_key;
}

redhorn::rsa::key& redhorn::rsa::key::operator=(const key &other)
{
    std::lock_guard<std::mutex> lock(other._mutex);
    _priv_key = other._priv_key;
    return *this;
}

redhorn::rsa::key::operator bool() const
{
    std::lock_guard<std::mutex> lock(_mutex);
    CryptoPP::AutoSeededRandomPool prng;    
    return _priv_key.Validate(prng, 3);
}

std::string redhorn::rsa::key::str() const
{
    std::lock_guard<std::mutex> lock(_mutex);
    std::string res;
    CryptoPP::HexEncoder sink(new CryptoPP::StringSink(res), false);
    _priv_key.DEREncode(sink);
    return res;
}

redhorn::rsa::certificate redhorn::rsa::key::certificate() const
{
    return redhorn::rsa::certificate(*this);
}

redhorn::sha3::digest redhorn::rsa::key::fingerprint() const
{
    return redhorn::sha3::hash(this->certificate().str());
}

redhorn::rsa::key redhorn::rsa::key::generate(size_t size)
{
    redhorn::rsa::key res;
    CryptoPP::AutoSeededRandomPool prng;
    CryptoPP::RSA::PrivateKey privKey;
    res._priv_key.GenerateRandomWithKeySize(prng, size);
    return res;
}

