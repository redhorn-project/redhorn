#include <fcntl.h>

#include <redhorn/iopool.hpp>
#include <redhorn/utils.hpp>

using namespace redhorn;

iopool::info_base::info_base(int fd, 
                             int events,
                             std::function<void()> signal)
    : fd(fd)
    , events(std::make_shared<std::atomic<short>>(events))
    , signal(signal)
{
}

iopool::listener_info::listener_info(redhorn::listener listener,
                                     std::function<void()> signal)
    : info_base(listener.handler(), POLLIN, signal)
{
    pollin = [listener, 
              events = this->events,
              signal = this->signal]() mutable
             {
                 listener.poll(false);
                 *events |= POLLIN;
                 signal();
             };
}

iopool::iopool(redhorn::logger log)
    : _log(std::move(log))
    , _state(state::pending)
{
    if (::pipe(_signal_fd) == -1) 
        throw std::runtime_error(::strerror(errno));

    if (::fcntl(_signal_fd[0], F_SETFL, O_NONBLOCK) == -1)
        throw std::runtime_error(::strerror(errno));
    if (::fcntl(_signal_fd[1], F_SETFL, O_NONBLOCK) == -1)
        throw std::runtime_error(::strerror(errno));

    _signal = [fd = _signal_fd[1]]()
              {
                  static const char msg[] = "wakeup";
                  ::write(fd, msg, sizeof(msg));
              };
}

void iopool::start(size_t pool_size)
{
    if (_state != state::pending )
        throw std::logic_error("iopool is not pending");
    for (size_t i = 0; i < pool_size ; ++i)
        _iothreads.push_back(std::thread([this, i]
                                         {
                                             redhorn::logging::name_this_thread("io."+std::to_string(i));
                                             this->io_loop();
                                         }));
 
    _poll_thread = std::thread([this]
                               {
                                   redhorn::logging::name_this_thread("poll");
                                   this->poll_loop();
                               });
    _state = state::running;
}

void iopool::stop()
{
    if (_state != state::running )
        throw std::logic_error("iopool is not running");
    redhorn::threading::signal(_poll_thread.get_id());
    for (auto &io : _iothreads)
        redhorn::threading::signal(io.get_id());
    _task_queue.signal();
    _signal();
    _state = state::stopping;
}

void iopool::wait()
{
    if (_state != state::stopping && _state != state::running )
        throw std::logic_error("iopool is not running");
    _poll_thread.join();
    ::close(_signal_fd[0]);
    ::close(_signal_fd[1]);
    for (auto &io : _iothreads)
        io.join();
    _state = state::joined;
}


void iopool::flush()
{
    if (_state != state::joined)
        throw std::logic_error("iopool is not stopped");
    _log.info("flush");
    redhorn::task_queue::value_type task = 
        []()
        {
            throw redhorn::threading::stop_thread();
        };
    _task_queue.push(std::move(task));
    
    try
    {
        for (;;)
        {
            auto task = _task_queue.pop();
            task();
        }
    }
    catch (redhorn::threading::stop_thread&)
    {
        _log.info("flush: caught stop signal");
    }
    _log.info("flushed");
}

void iopool::bind(redhorn::listener listener)
{
    {
        std::lock_guard<std::mutex> guard(_map_mutex);
        _info_map.emplace(listener.id(), new listener_info(listener, _signal));
    }
    _signal();
}

void iopool::unbind(redhorn::listener listener)
{
    std::lock_guard<std::mutex> guard(_map_mutex);
    _info_map.erase(listener.id());
}

void iopool::poll()
{
    std::vector<std::size_t> ids;
    std::vector<::pollfd> pollfds;
    ids.reserve(_info_map.size() + 1);
    pollfds.reserve(_info_map.size() + 1);

    ids.push_back(0);
    pollfds.emplace_back(::pollfd{_signal_fd[0], POLLIN, 0});
    {
        std::lock_guard<std::mutex> guard(_map_mutex);
        for (auto &pair : _info_map)
        {
            short events = pair.second->events->load();
            if (events)
            {
                ids.push_back(pair.first);
                pollfds.emplace_back(::pollfd{pair.second->fd, events, 0});
            }
        }
    }
        
    int ret = ::poll(pollfds.data(), pollfds.size(), -1);

    if ((ret == -1) && (errno != EINTR))
    {
        throw std::runtime_error(strerror(errno));
    }

    if (pollfds[0].revents & POLLIN)
    {
        static char buff[128];
        for (;;)
        {
            int ret = read(pollfds[0].fd, &buff, sizeof(buff));
            if ( ret <= 0)
            {
                if (errno == EAGAIN)
                    break;
                else
                    throw std::runtime_error(strerror(errno));
            }
        }
    }

    for (size_t i = 1; i < ids.size(); ++i)
    {
        std::size_t id = ids[i];
        ::pollfd &pfd = pollfds[i];
        
        std::shared_ptr<info_base> info;
        {
            std::lock_guard<std::mutex> guard(_map_mutex);
            auto iter = _info_map.find(id);
            if (iter != _info_map.end())
                info = iter->second;
        }
        if (!info)
        {
            _log.warning("socket/listener info was not found for fd: %d", pfd.fd);
        }
        else
        {
            if (pfd.revents & POLLIN)
            {
                *(info->events) ^= POLLIN;
                _task_queue.push(info->pollin);
            }
            if (pfd.revents & POLLOUT)
            {
                *(info->events) ^= POLLOUT;
                _task_queue.push(info->pollout);
            }
            if (pfd.revents & POLLERR)
            {
                _log.warning("POLLERR signalled for fd: %d", pfd.fd);
                *(info->events) = 0;
            }
            if (pfd.revents & POLLHUP)
            {
                _log.warning("POLLHUP signalled for fd: %d", pfd.fd);
                *(info->events) ^= POLLIN;
            }
        }
    }
}

void iopool::poll_loop()
{
    try
    {
        _log.info("started");
        for (;;)
        {
            redhorn::threading::check();
            try
            {
                poll();
            }
            catch (redhorn::threading::stop_thread&)
            {
                throw;
            }
            catch (...)
            {
                _log.exception("poll failed");
            }
        }
    }
    catch (redhorn::threading::stop_thread&)
    {
        _log.info("caught stop signal");
    }
    _log.info("exited");
            
}

void iopool::io_loop()
{
    try
    {
        _log.info("started");
        for (;;)
        {
            redhorn::threading::check();
            auto task = _task_queue.pop();
            try
            {
                task();
            }
            catch (redhorn::threading::stop_thread&)
            {
                throw;
            }
            catch (...)
            {
                _log.exception("Task failed");
            }
        }
    }
    catch (redhorn::threading::stop_thread&)
    {
        _log.info("caught stop signal");
    }
    _log.info("exited");
                                                         
}
