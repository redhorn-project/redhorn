PLATFORM ?= $(shell uname)

gtest_filter=$(filter)
ifeq ($(strip $(filter)),)
	gtest_filter=
else
	gtest_filter=--gtest_filter=$(filter)
endif

log_verbose=$(verbose)
ifeq ($(verbose),yes)
	log_verbose=-v
else
	log_verbose=
endif

build: 
	$(MAKE) -C src/
	$(MAKE) -C tests/

install:
	install -d $(DESTDIR)/usr/lib/
	cp -P lib/libredhorn.so* $(DESTDIR)/usr/lib/
	install -d $(DESTDIR)/usr/include/
	cp -r include/redhorn $(DESTDIR)/usr/include/

test: build
ifeq ($(PLATFORM), Darwin)
	DYLD_LIBRARY_PATH=./lib ./bin/tests $(log_verbose) $(gtest_filter)
else ifeq ($(PLATFORM), Linux)
	LD_LIBRARY_PATH=./lib ./bin/tests $(log_verbose) $(gtest_filter)
endif

clean:
	$(MAKE) -C src/ clean
	$(MAKE) -C tests/ clean

.PHONY: build test clean install
